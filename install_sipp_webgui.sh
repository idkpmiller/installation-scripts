#!/bin/bash
##############################
# Version 0.1 - 
#   Initial release 22/11/2014
#   last updated 22/11/2014
#
# by Paul Miller
##############################
# DESCRIPTION:
# installation script for 
# LAMP (Apache, MySQL, PHP).
#
# Optionally PHP GUI
# then adds the SIPp web Frontend
# It does not check for a local install of SIPp.
# Script is focused on Raspberry Pi
# Wheezy installations. But may
# function on other Debian/Ubuntu
# Installs. 
##############################

IPv4_ADDRESS=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
FQDN=$(hostname -f)
DIALOG="$(which whiptail dialog)"

# Locate any existing apps
APACHE="$(which apache2)"
MYSQL="$(which mysqld)"
PGS=""

# establish text format options
bold=`tput bold`
normal=`tput sgr0`

# initialise variables
WEBSVR="NONE"
DBSVR="NONE"
PHPSVR=0
PHPMYADMIN=0

# Define web app packages
APACHEPKG="apache2"

# Define php app packages
PHPPKG="php5 zip unzip"
PHPMYSQLPKG="php5-mysql"
PHPAPACHEPKG="php5-cli php-db php5-cgi libapache2-mod-php5"

# Define database app packages
MYSQLPKG="mysql-server mysql-client"

# Define application specific parameters

#Paths are without trailing forward slash
SIPPHTTPPATH="/var/www/html"
SIPP_PATH="/usr/bin"
SIPP_VERSION="3.4.1"
MYSQL_ROOT_PW="ptcsim"
PHPADMIN_PW="ptcsim"


#---------------------
# Functions
#---------------------
function pTitle () {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as arguament to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot() {
# Supports: text, whiptail, dialog
# Requires:
# DIALOG="$(which whiptail dialog)" # variable
##############################################

logger "$0 Entered OS ENVIRONMENT CHECKS" 

if [[ $DIALOG ]]; then
	(( `id -u` )) && $DIALOG --msgbox --title "Checking for root Priviledges:" "Must be ran as root, try prefixing with sudo." 8 44 && exit 1
else
	pTitle "Checking for root Priviledges:"
	(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
fi
#func_chkRoot
}

function func_chkInternet () {
pTitle "Checking Internet Connectivity."
/usr/bin/wget -q --tries=10 --timeout=5 "http://www.google.com" -O /tmp/index.google &> /dev/null
if [[ ! -s /tmp/index.google ]];then
	echo "No Internet connection. Exiting."
	/bin/rm /tmp/index.google
	exit 1
else
	echo "Internet connection working"
	/bin/rm /tmp/index.google
fi
#func_chkInternet
}

function func_updSystem() {
pTitle 'Updating system, please Wait.'
apt-get -y -qq update
  
if [[ $DIALOG ]]; then
	#Check if user want to upgrade now.
	whiptail --yesno --title "Upgrade" "Would you like to upgrade the system now?" 10 50
	if [[ $? -ne 0 ]]; then
		echo "NO, selected so continuing..."
	else
		apt-get -y upgrade;
		apt-get -y autoremove;
	fi
else  
	while true; do
	read -p "Would you like to upgrade the system now? (Y/N)" answer
		case $answer in
			[Yy] ) 
				echo "Upgrading please wait..";
				apt-get -y upgrade;
				apt-get -y autoremove;
				break;;
			[Nn] ) echo "NO, selected so continuing"; break;;
			* ) echo "Please answer Y or N.";;
		esac
	done
fi
#End func_updSystem    
}

function _rmdir () {
if [ -d $1 ];then
  /bin/rm -r $1
  RESULT=$?
  if [[ $RESULT != 0 ]]; then echo "Remove Directory $1 Failed."; exit 1; fi
fi

}

function func_InstPackages(){
#Usage:
# call function with list of packages to be installed as the argument"
# e.g. 
# MusicPKG="mp3 sox"
# func_InstPackages MusicPKG
##########################
PACKAGES="${!1}"

for pkg in $PACKAGES; do
	if apt-get -y -qq install $pkg; then
		echo "${bold}Successfully installed $pkg ${normal}"
	else
		echo "${bold}Error installing $pkg ${normal}"
		exit 1
	fi
done

pTitle "Package $1 installation complete."
#End func_InstPackages
}

########### WEB #############
  
function func_InstApache() {
logger "$0 Entered func_InstApache"
pTitle "Installing Apache:"
WEBSVR="apache2"
func_InstPackages APACHEPKG

#setup the servername
echo 'ServerName       $FQDN' > /etc/apache2/conf.d/servername.conf

chown www-data:www-data /var/www
chmod 775 /var/www
usermod -a -G www-data spark

a2enmod ssl 
a2ensite default-ssl

/etc/init.d/apache2 restart
# End func_InstApache
}

######### DATABASE ##########

function func_InstMySQL() {
logger "$0 Entered func_InstMySQL"
pTitle "Installing MySQL:"
echo "mysql-server mysql-server/root_password password ptcsim" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password ptcsim" | debconf-set-selections

func_InstPackages MYSQLPKG
#Set MySQL to start automatically
update-rc.d mysql remove
update-rc.d mysql defaults
DBSVR="mysqld"
# End func_InstMySQL
}

########### PHP #############
function func_InstPHP () {
# install PHP Web
	echo "Adding apache module"
    func_InstPackages PHPAPACHEPKG
    pTitle "Installing PhP:"
	func_InstPackages PHPPKG
	  
	# create a basic php test page
    echo '<?php phpinfo(); ?>' > /var/www/phpinfo.php
	PHPSVR=1
	
# install PHP admin for mysql
	func_InstPackages PHPMYSQLPKG
    func_InstPHPMyAdmin
# END func_InstPHP
}

function func_InstPHPMyAdmin () {
# unattended version of the function
# actions optionally to perform when a specific database is installed and PHP
echo "Entering func_InstPHPMyAdmin, PHPSVR=$PHPSVR, DBSVR=$DBSVR"
if [[ $PHPSVR -eq "1" ]] && [[ $DBSVR == "mysqld" ]]
then
  #if there is also a dialog based service then provide GUI feedback to the user
  if [[ $DIALOG ]]
  then
    $DIALOG --yesno "Would you like phpmyadmin installed, a web GUI to adminstate MySQL?" 10 70
    answer=$?
    if [[ $answer == 0 ]]; then
      echo "You have selected to install the web GUI app"
	  echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | debconf-set-selections
	  echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
	  echo "phpmyadmin phpmyadmin/mysql/admin-user string root" | debconf-set-selections
	  echo "phpmyadmin phpmyadmin/mysql/admin-pass password $MYSQL_ROOT_PW" | debconf-set-selections
	  echo "phpmyadmin phpmyadmin/mysql/app-pass password $PHPADMIN_PW" |debconf-set-selections
	  echo "phpmyadmin phpmyadmin/app-password-confirm password $PHPADMIN_PW" | debconf-set-selections
	  apt-get -y -qq install phpmyadmin
	  PHPMYADMIN=1
    else
      echo "You have selected to not install the web GUI app"
    fi
  else
    #if there is no dialog based service then provide text feedback to the user
    while true; do
      read -p "Would you like phpmyadmin installed, a web GUI to adminstate MySQL? (Y/N)" answer
        case $answer in
          [Yy] ) 
	      echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | debconf-set-selections
	      echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
	      echo "phpmyadmin phpmyadmin/mysql/admin-user string root" | debconf-set-selections
	      echo "phpmyadmin phpmyadmin/mysql/admin-pass password $MYSQL_ROOT_PW" | debconf-set-selections
	      echo "phpmyadmin phpmyadmin/mysql/app-pass password $PHPADMIN_PW"" |debconf-set-selections
    	  echo "phpmyadmin phpmyadmin/app-password-confirm password $PHPADMIN_PW"" | debconf-set-selections
		  apt-get -y -qq install phpmyadmin
			PHPMYADMIN=1
			echo "|================================================================================|"
			echo "| The next package to be installed is PHPMyAdmin as part of the installation     |"
			echo "| a selection of the Web Server is Required.                                     |"
			echo "| You will see the following questions:                                          |"
			echo "|                                                                                |"
			case $WEBSVR in
				apache2 ) 
				  echo 'Include /etc/phpmyadmin/apache.conf' >> /etc/apache2/apache2.conf
				  echo "| Web server to reconfigure automatically: ${bold}<-- Apache2${normal}           |"
				  echo "| Configure database for phpmyadmin with dbconfig-common? ${bold}<-- No${normal} |"
				  ;;
				* ) echo "An unexpected error has occurred.";;
			esac
			echo "|================================================================================|"
			break
			;;
          [Nn] ) echo "You have selected to not install the web GUI app"; break;;
          * ) echo "Please select a valid option.";;
        esac
    done
  fi
fi

# END func_InstPHPMyAdmin
}

########### OTHER #############
function func_InstSIPp-Web () {
cd /usr/src

# Download
wget 'http://softlayer-dal.dl.sourceforge.net/project/sipp/sipp/3.1/sipp_webfrontend_v1.2.tgz' -O 'sipp_webfrontend_v1.2.tgz' -c
 

# Copy & Extract
if [ -d $SIPPHTTPPATH ];then
  mkdir $SIPPHTTPPATH
fi

cp sipp_webfrontend_v1.2.tgz $SIPPHTTPPATH/
cd $SIPPHTTPPATH/
tar zxvf sipp_webfrontend_v1.2.tgz 
mv $SIPPHTTPPATH/src/* $SIPPHTTPPATH/

# Create DATABASE
db="CREATE DATABASE SIPpDB;"
mysql -u root -p$MYSQL_ROOT_PW -e "$db"
if [ $? != "0" ]; then
 echo "[Error]: Database creation failed"
 exit 1
else
 pTitle " Database has been created successfully "
fi 

mysql -u root -p$MYSQL_ROOT_PW SIPpDB < $SIPPHTTPPATH/tables.sql
if [ $? != "0" ]; then
 echo "[Error]: Database table population failed"
 exit 1
else
 pTitle " Database has been populated with tables successfully "
fi 

# Edit config.ini.php

cat <<EOF > $SIPPHTTPPATH/config.ini.php
;<?/* with this line a browser is not able to view this file, so please leave it.
;
;  SIPp Webfrontend	- Web tool to create, manage and run SIPp test cases
;  Copyright (c) 2008 Mario Smeritschnig
;  Idea, support, planning, guidance Michael Hirschbichler
; 
; * * * BEGIN LICENCE * * *
; 
;  This file is part of SIPp Webfrontend.
;  
;  SIPp Webfrontend is free software: you can redistribute it and/or modify
;  it under the terms of the GNU General Public License as published by
;  the Free Software Foundation, either version 3 of the License, or
;  (at your option) any later version.
;  
;  SIPp Webfrontend is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;  
;  You should have received a copy of the GNU General Public License
;  along with SIPp Webfrontend.  If not, see <http://www.gnu.org/licenses/>
;
; 
; * * * END LICENCE * * *
; 
;
; This configuration file is divided in three sections respectively tagged with [EXECUTABLES], [AVP], and [CONFIG].

; In the [EXECUTABLES]-section SIPp executables and their versions can be specified. At least one executable is mandatory.

; In the [AVP]-section global command line parameters for SIPp can be specified in form of attribute-value pairs (attribute = value).
; I.e. most parameters of SIPp (without the leading -) will be accepted (see sipp -h for more details).
; These parameters can be overwritten in each individual call by using the 'Extended parameters' textarea in the web tool.
; Take care that you only use parameters that work with every version of SIPp you are using with this tool.
; If a attribute has no value, but is a switch, use TRUE as a value (e.g. aa = TRUE)
; If a value contains non-alphanumeric characters it needs to be enclosed in double-quotes (").
; Parameters not allowed are:
;   i, m, nd, nr, t, p, r, timeout, pause_msg_ign, v, bind_local, inf, sd, sf, sn, stf, trace_msg,
;   trace_shortmsg, trace_screen, trace_err, trace_timeout, trace_stat, trace_rtt, trace_logs
;
; In the [CONFIG]-section you will find program specific parameters, like admin password or mysql-database connection information...



; Executables section starts here
[EXECUTABLES]

; Specify the absolute paths to the SIPp executables using the syntax: version_number = path
; The first occurrence of such a line, defines the default version. At least one version is mandatory.
; e.g.
; 2.0 = "/usr/bin/sipp2/sipp"
; 1.0 = "/usr/bin/sipp1/sipp"
$SIPP_VERSION = "$SIPP_PATH/sipp"

; Attribute-value-pairs section starts here
[AVP]

; Set the local control port number. Default is 8888.
cp = 8888
; Set the statistics report frequency on screen. Default is 1 and default unit is seconds. Mandatory.
f = 1

; Set the maximum number of simultaneous calls. Once this
; limit is reached, traffic is decreased until the number
: of open calls goes down. Default:
; (3 * call_duration (s) * rate).
l = 10000

; Set the local port number.  By default, the system tries to find a free port, starting at 5060. Mandatory.
p = 5060

; The response times are dumped in the 
; log file defined by -trace_rtt each time 'freq' measures are performed.
; Default value is 200.
; rtt_freq = 200

; Set the timer resolution. Default unit is milliseconds. 
; This option has an impact on timers precision. Small
; values allow more precise scheduling but impacts CPU
; usage. If the compression is on, the value is set to
; 50ms. The default value is 10ms.
; timer_resol = 10



; Config section starts here
[CONFIG]

; Specify the host, user, password, and the database name to access the mysql-database.
db_host = "localhost"
db_user = "root"
db_pwd = "ptcsim"
db_name = "SIPpDB"

; With csv_separator you can set the separator for downloadable csv files (log files, injection files ...).
; Default separator is ;
csv_separator = ";"

; Specify the password for the admin section. Use admin_pwd = "" to disable admin-authentication.
admin_pwd = "ptcsim"

; Here you can specify the minimum time, sipp processes and tempfiles may reside after creation. The unit is seconds, the default value 3600.
remove_garbage_after = 3600

; with this line a browser is not able to view this file, so please leave it. */?>
EOF


# change php.ini for sipp web fronend to function
sed -i "s/short_open_tag = Off/short_open_tag = On/g" /etc/php5/apache2/php.ini
# restart to make sure php.ini change takes effect
/etc/init.d/apache2 restart  

# END func_InstSIPp-Web
}

function func_Complete(){
#func_Complete
logger "$0 Entered func_Complete"
MESSAGE="A $WEBSVR webserver can be found by "
MESSAGE=$MESSAGE"pointing your web browser at:\n"
MESSAGE=$MESSAGE"\n"

for addr in $IPv4_ADDRESS; do
  MESSAGE=$MESSAGE"HTTP:          http://$addr\n"
  
  if [[ $PHPSVR ]]; then
    MESSAGE=$MESSAGE"PhP Test:      http://$addr/phpinfo.php\n"
  fi
  
  if [[ $PHPMYADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPMyAdmin     http://$addr/phpmyadmin\n"
  fi
  
  if [[ $PHPLITEADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPLiteAdmin   http://$addr/phpliteadmin.php (passwd = admin)\n"
  fi
  
  if [[ $PHPPGADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPPGAdmin   http://$addr/phppgadmin (login postgres, blank passwd)\n"
  fi
done

if [[ $DIALOG ]]
then
  $DIALOG --msgbox --title "Script is Complete!" "$MESSAGE" 18 78
else
  pTitle " The script is complete. "
  echo -e "$MESSAGE"
fi
# END func_Complete
}

##---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
func_chkInternet
func_updSystem

#---------------------
# INSTALL Applications
#---------------------

# Install LAMP
func_InstMySQL
func_InstApache
func_InstPHP

# install SIPp Web UI
func_InstSIPp-Web
  	
func_Complete
pTitle "SIPp Web GUI   http://$addr/index.php (login $PHPADMIN_PW)"

#---------------------
# FINISHING OFF
#---------------------	
if [[ $WEBSVR != "NONE" ]]; then
  /etc/init.d/$WEBSVR restart
  func_Complete
else
  echo "Web server not installed."
fi
echo "Done!"
echo "================================================================================"

#!/bin/bash
/usr/bin/logger 'NTP Local source Recovered port check'

echo '
############### Inserted by Monit Service hostntp.conf ###############
driftfile /var/lib/ntp/ntp.drift

server ntp.sip2serve.com iburst
server 0.debian.pool.ntp.org iburst
server 1.debian.pool.ntp.org iburst
server  127.127.1.0
fudge   127.127.1.0 stratum 10

# By default, exchange time with everybody, but don't allow configuration.
restrict -4 default kod notrap nomodify nopeer noquery
restrict -6 default kod notrap nomodify nopeer noquery

# Local users may interrogate the ntp server more closely.
restrict 127.0.0.1
restrict ::1

# If you want to provide time to your local subnet, change the next line.
# (Again, the address is an example only.)
# broadcast 192.168.0.255

' > /etc/ntp.conf

######################################################################

/etc/init.d/ntp restart
/usr/bin/logger 'NTP returned to not broadcasting time services'

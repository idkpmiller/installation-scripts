#!/bin/bash
DIR=$(cd $(dirname $0) && pwd)
cd $DIR
DNSIP="192.168.0.6"

/usr/bin/logger 'DNS Local Primary Passed port Check'

# set DNS resolvers
echo "Exec: $DIR/local_DNS.sh"
$DIR/local_DNS.sh

ifconfig eth0:10 del $DNSIP
arp -d $DNSIP
echo "DNS Local Primary IP address released"
/usr/bin/logger 'DNS Local Primary IP address released'
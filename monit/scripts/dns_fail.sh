#!/bin/bash
DNSIP="192.168.0.6"
MASK="255.255.255.0"

# set DNS resolvers
remote_DNS.sh

#check if ping works
if ping -c 1 $DNSIP > /dev/null
then
  /usr/bin/logger 'DNS Primary passed ping Check'
else
  /usr/bin/logger 'DNS Primary failed ping Check' -t PROFILE
  #ifconfig eth0:10 $DNSIP netmask $MASK
  #sleep 5
  #arping -c 4 -A -I eth0:10 $DNSIP
fi




#!/bin/bash
/usr/bin/logger 'Remote DNS resolvers activated'

#will become /etc/resolv.conf
cat <<EOF > /etc/resolv.conf
############### Inserted by Monit Service remote_DNS.sh ###############
domain sip2serve.com
search sip2serve.com
nameserver 192.168.0.2
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF


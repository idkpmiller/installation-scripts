#!/bin/bash
/usr/bin/logger 'DHCP source recovered port Check'


cat <<EOF > /etc/dnsmasq.d/run.dhcp
#
# PLEASE: read the Monit dhcp.conffile as this just enable/disables the service!
#
#dhcp-range=eth0,192.168.0.220,192.168.0.240,48h
EOF

/etc/init.d/dnsmasq restart
/usr/bin/logger 'DHCP service on Tiny is disabled'
#!/bin/bash
/sbin/ip route replace default via 192.168.0.1 dev eth0 
/usr/bin/logger 'Default GW failed ICMP Check'

# set DNS resolvers
local_DNS.sh

/usr/bin/logger 'Primary Internet DSL GW failed, DNS resolvers revert to default'

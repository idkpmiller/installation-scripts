#!/bin/bash
SIPIP="192.168.0.23"

/usr/bin/logger 'SIP Primary Server failed UDP port Check'

#check if ping works
if ping -c 1 $SIPIP > /dev/null
then
  /usr/bin/logger 'SIP Primary Server passed ping Check'
else
  /usr/bin/logger 'SIP Server Primary failed ping Check' -t PROFILE
  #ifconfig eth0:10 $SIPIP netmask $MASK
  #sleep 5
  #arping -c 4 -A -I eth0:10 $SIPIP
fi

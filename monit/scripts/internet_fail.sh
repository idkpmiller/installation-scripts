#!/bin/bash
/sbin/ip route replace default via 192.168.0.1 dev eth0 
/usr/bin/logger 'Primary Internet failed ICMP Check'

#will become /etc/resolv.conf
cat <<EOF > /etc/resolv.conf
############### Inserted by Monit Service ###############
domain sip2serve.com
search sip2serve.com
nameserver 192.168.0.6
nameserver 192.168.254.3
EOF

/usr/bin/logger 'Primary Internet using DSL failed, DNS resolvers revert to default'
 


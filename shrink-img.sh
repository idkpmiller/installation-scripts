#!/bin/bash -e

#************************************************#
#                   Shrink Image                 #
#           written by Paul Miller               #
#                  Jan, 2014                     #
#                                                #
#    Takes a rootfs RPi image and makes it       #
#      as small as possible                      #
#************************************************#

if [ $DEBUG ]; then
	set -x
fi
PROG=(pwd -p)
SOURCE="$(find /opt/images/drink-me/ -name **.img | awk 'NR==1{print;quit}')"
PREFIX="shrunk-"
SOURCE_PATH="/opt/images/drink-me/"
SOURCE_FILE="$(basename $SOURCE)"
DEST_FILE=$PREFIX$SOURCE_FILE
MAPPER="$(kpartx -l $SOURCE | awk NR==2'{print $1}')"

if [ -z $SOURCE_FILE ]
then
   echo "No img file to shrink found"
   exit 1
fi

echo "The source is called " $SOURCE 
echo "The source file is called " $SOURCE_FILE
echo "The destn file is called " $DEST_FILE
echo "The mapper is " $MAPPER

#Check if root and advise if not
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 7 

# log a message to /var/log/messages that we have entered the script.
logger Entered ${SOURCE_FILE}.sh script -t SHRINK

cd ${SOURCE_PATH} || { echo "command failed"; exit 8; } 

echo "current directory"
pwd

echo "Set-Up: partition mapping defined"
kpartx -av $SOURCE_FILE || { echo "command failed"; exit 9; } 

e2fsck -f -y -v /dev/mapper/$MAPPER

resize2fs /dev/mapper/$MAPPER

read -p "Enter the # Blocks to continue" RBLOCKS

#echo "Set-Up: Creating temporary mount point"
mkdir /mnt/tmp || { echo "command failed"; exit 10; } 

#echo "Set-Up: mounted partition $MAPPER as image"
mount /dev/mapper/$MAPPER /mnt/tmp || { echo "command failed"; exit 11; } 

###################################################################
DISK_SIZE="$(($(sudo blockdev --getsz /dev/mapper/$MAPPER)/2048/925))"
PART_START="$(sudo parted /dev/mapper/$MAPPER -ms unit s p |grep "^2" |cut -f2 -d:)"
[ "$PART_START" ] || exit 1
#PART_END="$(((DISK_SIZE*925*2048-1)-1536))"
PART_END=$(awk  'BEGIN { rounded = sprintf("%.0f", ${RBLOCKS}*4000*1.03); print rounded }')
###################################################################
# Display Info...
###################################################################
echo $PROGRAM - $VERSION
echo "======================================================"
echo "Current Disk Info"
fdisk -l /dev/mapper/$MAPPER
echo ""
echo "======================================================"
echo ""
echo "Calculated Info:"
echo " Disk Size  = $DISK_SIZE gb"
echo " Part Start = $PART_START"
echo " Part End   = $PART_END"
echo ""
echo "======================================================"
read -p "Press enter to continue" TEMP

#echo "Clean-Up: unmounted image"
umount /mnt/tmp  || { echo "command failed"; exit 13; }

#echo "Clean-Up: Removed temporary mount point"
rm -rf /mnt/tmp  || { echo "command failed"; exit 14; }

echo "Clean-Up: deleted partition mapping"
kpartx -d $SOURCE_FILE  || { echo "command failed"; exit 15; }

#############################################

echo "Making changes using fdisk..."
printf "d\n2\nn\np\n2\n$PART_START\n$PART_END\np\nw\n" | fdisk /dev/mmcblk0

exit 0

echo
echo Setting up init.d resize2fs_once script

cat <<\EOF > /etc/init.d/resize2fs_once &&
#!/bin/sh
### BEGIN INIT INFO
# Provides: resize2fs_once
# Required-Start:
# Required-Stop:
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Run resize2fs_once on boot
# Description:
### END INIT INFO

. /lib/lsb/init-functions

case "$1" in
  start)
    log_daemon_msg "Starting resize2fs_once, THIS WILL TAKE A FEW MINUTES " && 
    
    # Do our stuff....   
    resize2fs /dev/mmcblk0p2 &&
    
    # Okay, not lets remove this script
    rm /etc/init.d/resize2fs_once &&
    update-rc.d resize2fs_once remove &&
    log_end_msg $?
    ;;
  *)
    echo "Usage: $0 start" >&2
    exit 3
    ;;
esac
EOF
  chmod +x /etc/init.d/resize2fs_once &&
  update-rc.d resize2fs_once defaults &&

echo
echo #####################################################################
echo "System is now ready to resize your system n the next boot of the image."

echo "Script Complete..."



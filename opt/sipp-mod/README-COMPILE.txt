Should be as simple as:

 

  make pcapplay_ossl

 

Requires ncurses, libopenssl 0.9.8, libpcap, libcrypto0.9.8 

 

 

mbriggs@forestero ~/sipp/matt $ ldd ./sipp

        cygcrypto-0.9.8.dll => /usr/bin/cygcrypto-0.9.8.dll (0x6e7b0000)

        cyggcc_s-1.dll => /usr/bin/cyggcc_s-1.dll (0x6e320000)

        cygncurses-10.dll => /usr/bin/cygncurses-10.dll (0x6c780000)

        cygssl-0.9.8.dll => /usr/bin/cygssl-0.9.8.dll (0x6b980000)

        wpcap.dll => /cygdrive/d/WINDOWS/system32/wpcap.dll (0x10000000)


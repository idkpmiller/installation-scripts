#!/bin/bash
##############################
#   Initial release 20/01/2014
#
# by Paul Miller
##############################
# DESCRIPTION:
# Script to run SIPp scenarios
##############################

IPv4_ADDRESS=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
FQDN=$(hostname -f)
SCRIPTPATH=`pwd -P`
PROG=$(basename $0)
SCRIPTPREF=${PROG%.*}
SIPP="$(which sipp)"
VERSION='0.1.8'
CONF_FILE=$SCRIPTPATH/$SCRIPTPREF.conf
DIALOG="$(which whiptail dialog)"

bold=`tput bold`
normal=`tput sgr0`

#---------------------
# Functions
#---------------------
function pTitle {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as arguament to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot {
# Supports: text, whiptail, dialog
# Requires:
# DIALOG="$(which whiptail dialog)" # variable
##############################################

if [ $DIALOG ]
then
  (( `id -u` )) && $DIALOG --msgbox --title "Checking for root Priviledges:" "Must be ran as root, try prefixing with sudo." 8 44 && exit 1
else
pTitle "Checking for root Priviledges:"
  (( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
fi
# END func_chkRoot
}

function func_usage {
echo "Usage: $0"
echo "${bold}OPTIONS:${normal}"
#echo "  -c     Enter the configuration mode, this is used to change base settings."
echo "  -h     This help file"
echo "  -s     Display the curent base settings and their values"
echo "  -v     Display the script and SIPp Version"
echo "  -d     Set the target dircectory number for the test"
echo "  -f     Set the call XML file to be used (as quoted text)."
exit 0
# END func_usage
}

function show_ver {
echo " Script Version = $VERSION"
echo " ================================================================================"
${SIPP_PATH}sipp -v
# END show_ver_
}

function show-conf {
pTitle "Current Configuration"
echo "SIPP_PATH=${SIPPH}"
echo "SIPP=${SIPP}"
echo "SBC_IP=${SBC_IP}"
echo "SBC_Port=${SBC_Port}"
echo "VALUE_PATH=${VALUE_PATH}"
echo "VALUE_CSV=${VALUE_CSV}"
echo "REG_XML=${REG_XML}"
echo "CALL_XML=${CALL_XML}"
echo "TARGET_DN=${TARGET_DN}"
# END show-conf
}

function get-conf {
if [ -e ${CONF_FILE} ]
then
  source ${CONF_FILE}
else
echo "Creating configuration file, ${CONF_FILE}" 
  echo '
# The parent directory path where the scenario and files can be located
# end the path with the forward slash /
SIPP_PATH='/usr/bin/'

# The version of SIPp executable that will be run is located.
SIPP='/usr/bin/sipp'

# Add the SBC IP address that you would like to use for the test; e.g.
###PROD 4250###
#122.56.253.104	MDR-SBC-0506-AGVCp
#122.56.253.105	MDR-SBC-0506-AGVCs
#122.56.254.199	HN-SBC-0102-AGVCp
#122.56.254.200	HN-SBC-0102-AGVCs
#122.56.254.104	WN-SBC-0304-AGVCp
#122.56.254.105	WN-SBC-0304-AGVCs
#122.56.255.104	CH-SBC-0304-AGVCp
#122.56.255.105	CH-SBC-0304-AGVCs
###PROD 4500###
#122.56.253.231	PAK-SBC-0102-AGVCp
#122.56.253.232	PAK-SBC-0102-AGVCs
#122.56.254.167	PRO-SBC-0102-AGVCp
#122.56.254.168	PRO-SBC-0102-AGVCs
#122.56.255.167	RIC-SBC-0102-AGVCp
#122.56.255.168	RIC-SBC-0102-AGVCs
###NIL 4250###
#10.111.111.244	GFO-SD-A
#10.111.111.245	GVC2-SD-Ap
#10.111.111.246	GVC2-SD-As
###NIL 4500###
#10.207.44.87		NIL-SBC-0506-AGVCp
#10.207.44.88		NIL-SBC-0506-AGVCs
#10.207.44.89		NIL-SBC-0506-AGFO
SBC_IP=122.56.255.168

# SBC port typically this is 5060
SBC_Port=5060

# This is the path to the value file a .csv file where all the
# fields to be used in the call scenario files are held.
VALUE_PATH='./values/'

# The actual value file name the recommended name format is
# <ENV>-<ID> The id can be anything that provides the tester
# with a sufficient unambigous recognisable name
VALUE_CSV='prod-vc.csv'

# The name of the Registration XML scenario file this is often
# the same except for when specific registration scenarios are to be tested.
REG_XML='./registration/UAC-REG.xml'

# Target directory number for the call XML file this element
# can be overiden on the command line for ease of switching.
TARGET_DN='042291832'

' > ${CONF_FILE}

fi
}


function func_sipp {
# Supports: text, whiptail, dialog
# Requires:
# DIALOG="$(which whiptail dialog)" # variable
##############################################

logger "$0 Entered OS ENVIRONMENT CHECKS" 

if [ $DIALOG ]
then
  (( `id -u` )) && $DIALOG --msgbox --title "SIPp:" "Currently hardcoded." 8 44
  echo Launching test "${CALL_XML}" >> results.txt
  ${SIPP} ${SBC_IP}:${SBC_PORT} -sf "${REG_XML}" -inf "${VALUE_CSV}" -l 2 -m 1
  if test $? -ne 0
    then
        echo Test $i failed >> results.txt
    else
        echo Test $i succeeded >> results.txt
    fi
else
pTitle "SIPp:"
  (( `id -u` )) && echo "Currently hardcoded."
  ${SIPP} ${SBC_IP}:${SBC_PORT} -sf "${REG_XML}" -inf "${VALUE_CSV}" -l 2 -m 1
  if test $? -ne 0
    then
        echo Test $i failed >> results.txt
    else
        echo Test $i succeeded >> results.txt
    fi
fi

# END func_chkRoot
}

function func_filter {
NUM=0;
ls | egrep 'somefilter' | while read i ; do
    echo $'\t'$NUM$'\t'$i; NUM=$(($NUM+1)); done;
}

#LINES=$(filter)
#
#IFS=$'\n\t'
#
#dialog --backtitle "Linux Shell Script" --title "Main\
#Menu" --menu "Move using [UP] [DOWN],[Enter] to\
#Select" 15 50 3 $LINES 2>/tmp/menuitem.$$
#
#unset IFS
#Edit2: Use this to retrieve the chosen filename:
#
#ITEM=$(cat /tmp/menuitem.$$)
#FILE=$(echo "$LINES" | sed -n 's/\t'$ITEM'\t//p')
############################

##---------------------
# OS ENVIRONMENT CHECKS
#---------------------
func_chkRoot
get-conf

 
OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "hvsf:d:" opt; do
    case "$opt" in
        h)  func_usage
            exit 0
            ;;
        v)  show_ver
            exit 0
            ;;
        s)  show-conf
            exit 0
            ;;
        f)  CALL_XML=$OPTARG
            ;;
        d)  TARGET_DN=$OPTARG
            ;;
        '?')
            func_usage >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Shift off the options and optional --.

func_sipp

#---------------------
# FINISHING OFF
#---------------------	

echo "Done!"
echo "================================================================================"

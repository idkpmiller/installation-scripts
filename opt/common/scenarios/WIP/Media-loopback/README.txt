from command line, this scenario will set up a media loopback call to a specific DN 
and then send PCMA around the loop

# sipp -s 5990 -ap test -sf register.xml -r 1 -rp 5000 192.168.0.23:5060 -trace_msg -trace_err -aa -inf users.csv -i 192.168.0.239

Must have parametes
===================

-s 5998			Set the username part of the resquest URI. Default is
                      	'service'

-sf UAC_with_audio.xml	Loads an alternate xml scenario file.

192.168.0.10:5060 	Set the remote device and port

-aa			Enable automatic 200 OK answer for INFO, UPDATE and
                      	NOTIFY messages.

-i 192.168.0.239	Set the local IP address for 'Contact:','Via:', and
                      	'From:' headers. Default is primary host IP address.

---------------------------------------------------------
Optional parameters
===================

-rp 5000 		Specify the rate period for the call rate.  Default is 1
                      	second and default unit is milliseconds.  This allows
                      	you to have n calls every m milliseconds (by using -r n
                      	-rp m).
                      	Example: -r 7 -rp 2000 ==> 7 calls every 2 seconds.
                                 -r 10 -rp 5s => 10 calls every 5 seconds.

-r 1			Set the call rate (in calls per seconds).  This value can
                      	bechanged during test by pressing '+','_','*' or '/'.
                      	Default is 10.
                      	pressing '+' key to increase call rate by 1 *
                      	rate_scale,
                      	pressing '-' key to decrease call rate by 1 *
                      	rate_scale,
                      	pressing '*' key to increase call rate by 10 *
                      	rate_scale,
                      	pressing '/' key to decrease call rate by 10 *
                      	rate_scale.
                      	If the -rp option is used, the call rate is calculated
                      	with the period in ms given by the user.

-t t1               	Set the transport mode:
                      	- u1: UDP with one socket (default),
                      	- un: UDP with one socket per call,
                      	- ui: UDP with one socket per IP address The IP
                        	addresses must be defined in the injection file.
                      	- t1: TCP with one socket,
                      	- tn: TCP with one socket per call,
                      	- l1: TLS with one socket,
                      	- ln: TLS with one socket per call,

-trace_msg
-trace_err



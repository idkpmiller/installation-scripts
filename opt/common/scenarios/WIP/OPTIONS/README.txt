OPTIONS

Send OPTIONS message 5 times to 30@192.168.1.211.

    sipp 192.168.1.211 -sf OPTIONS.xml -m 5 -s 30
    
Send OPTIONS message 30 times to 30@192.168.1.211 waiting 200 ms for 200/OK reply each time.

SIPp OPTIONS + 200/OK scenario
    sipp 192.168.1.211 -sf OPTIONS_recv_200.xml -m 30 -s 30
    
	
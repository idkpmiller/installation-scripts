#!/bin/bash
##############################
# by Paul Miller
##############################
# DESCRIPTION:
# Script to provide overall 
# menu control to other 
# control scripts
##############################


##############################
# VARABLES:
# section sets defaults & 
# script variables needed
##############################


DIR=$(cd $(dirname $0) && pwd)

##############################
# END OF VARIABLES
##############################


##############################
# FUNCTIONS:
# Only functions should exist
# in this area 
##############################

do_finish () {
  echo "Normal Close - User exited."
  rm -f /tmp/*$$
  exit
}

do_PortScan () {
  cd $DIR/scripts
  ./ctrl-chkhost.sh
}

do_SIPp () {
  cd $DIR/scripts
 ./ctrl-sipp.sh
}


##############################
# END OF FUNCTIONS
##############################


##############################
# CODE:
# This area starts the script
# and joins all functions 
# together to run the script.
##############################

cd $DIR

#tempfile3=/tmp/dialog_3_$$
##trap "rm -f $tempfile3" 0 1 2 5 15

# set traps so process kills will clean up
trap do_finish 0 1 2 5 15

while true; do
  FUN=$(whiptail --menu "The SIP PIT - (SIP Perfomance & Interoprability Tester)" 20 80 12 --cancel-button Quit --ok-button Select \
    "PortScan" "Check for open ports on hosts" \
    "SIPp" "A wrapper for the open source SIP and RTP test tool" \
    3>&1 1>&2 2>&3)
  RET=$?
  if [ $RET -eq 1 ]; then
    temp=
    do_finish
  elif [ $RET -eq 0 ]; then
    "do_$FUN" || whiptail --msgbox "There was an error running do_$FUN" 20 60 1
  else
    exit 1
  fi
done


#!/bin/bash
##############################
#   Initial release 20/01/2014
#
# by Paul Miller
##############################
# DESCRIPTION:
# Script to run SIPp scenarios
# use -h arguament to see list
# of commands available on the
# command line.
##############################


##############################
# VARABLES:
# section sets defaults & 
# script variables needed
##############################
IPv4_ADDRESS=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
DEFAULT_IF=$(route -n | grep 'UG[ \t]' | awk '{print $8}')
IP_ADDR=$(ip route show | grep "${DEFAULT_IF}  proto kernel  scope link  src" | awk '{print $9}')
FQDN=$(hostname -f)
SCRIPTPATH=$(pwd -P)
PROG=$(basename $0)
SCRIPTPREF=${PROG%.*}
SIPP="$(which sipp)"
OPTIONS=""
SBC_PORT=5060
SIPP_DISPLAY=rx
SCN_INUSE=both
SCN1=true
SCN2=true

TODAY=$(date +%F)

VERSION='0.2.4'
CONF_FILE=$SCRIPTPATH/$SCRIPTPREF.conf

bold=`tput bold`
normal=`tput sgr0`

##############################
# END OF VARIABLES
##############################


##############################
# FUNCTIONS:
# Only functions should exist
# in this area 
##############################

function pTitle {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as arguament to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot {
  (( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
# END func_chkRoot
}

function timestamp {
  date --rfc-3339=seconds
}

function debug {
  /bin/echo `date` $* | tee -a $SCRIPTPREF.log
}

function info {
 /bin/echo "`date \"+%x %X\"`  $*" | tee -a /${OUT_DIR}/INFO.txt 
}

function display_call {
echo "cset display $SIPP_DISPLAY" >/dev/udp/127.0.0.1/8888
}

function func_usage {
echo "Usage: $0"
echo "Description: This script is optimised to run a modified sipp instance that enables"
echo "two separate scenarios to be run one after another, a good example of this is when a" 
echo "REGISTER and a UAS dialog to be run in a single instance of SIPp"
echo "${bold}OPTIONS:${normal}"
#echo "  -c     Enter the configuration mode, this is used to change base settings."
echo "  -h     This help file"
echo "  -s     Display the curent base settings and their values"
echo "  -v     Display the script and SIPp Version"
echo "  -d     Set the target dircectory number for the test"
echo "  -f     Set the scn2 XML file to be used (as quoted text)."
echo "  -p     Set the SBC IP address for this call it overides XML conf file"
# END func_usage
}

function show_ver {
echo " Script Version = $VERSION"
${SIPP} -v | head -n 2
echo " ================================================================================"
# END show_ver_
}

function show-conf {
 pTitle "Current Configuration"
 echo "SIPP_PATH=${SIPP_PATH}"
 echo "VALUE_PATH=${VALUE_PATH}"
 echo "SCN_PATH=${SCN_PATH}"
 echo "OUTPUT_PATH=${OUTPUT_PATH}"
 echo "SIPP=${SIPP}"
 echo ""
 echo "SBC_IP=${SBC_IP}"
 echo "SBC_PORT=${SBC_PORT}"
 echo ""
 echo "VALUE_CSV=${VALUE_CSV}"
 echo "SCN1_XML=${SCN1_XML}"
 echo "SCN2_XML=${SCN2_XML}"
 echo "TARGET_DN=${TARGET_DN}"
 echo "OPTIONS=${OPTIONS}"
 echo "SIPP_DISPLAY=${SIPP_DISPLAY}"
# END show-conf
}

function chk_dir {
 # check if directory exists with todays date
 # if not found throw Error and exit
 if [ ! -d ${SIPP_PATH} ]; then debug "ERROR: Directory not found ${SIPP_PATH}"; exit 1; fi
 if [ ! -d ${SIPP_PATH}${VALUE_PATH} ]; then debug "ERROR: Directory not found ${SIPP_PATH}${VALUE_PATH}"; exit 1; fi
 if [ ! -d ${SIPP_PATH}${SCN_PATH} ]; then debug "ERROR: Directory not found ${SIPP_PATH}${SCN_PATH}"; exit 1; fi

 # if not create one
 if [ ! -d ${SIPP_PATH}${OUTPUT_PATH} ]; then mkdir ${SIPP_PATH}${OUTPUT_PATH}; chmod 664 ${SIPP_PATH}${OUTPUT_PATH}; fi
 if [ ! -d ${SIPP_PATH}${OUTPUT_PATH}${TODAY} ]; then mkdir ${SIPP_PATH}${OUTPUT_PATH}${TODAY}; chmod 664 ${SIPP_PATH}${OUTPUT_PATH}${TODAY}; fi

# END chk_dir
}

function get-conf {
if [ -e ${CONF_FILE} ]
then
  source ${CONF_FILE}
  # check directorys and files exist, where possible and not able
  # to be overridden on the command line.
  case ${SCN_INUSE} in
    1) SCN1=true
       SCN2=false
       ;;
    2) SCN1=false
       SCN2=true
       ;;
    both) SCN1=true
          SCN2=true
          ;;
  esac
else
echo "Creating initial configuration file, ${CONF_FILE}" 
  echo '
######## PATH SETTINGS ###############
# The parent directory path where the sipp input and ourput files can be located
# end the path with the forward slash /
SIPP_PATH='/opt/sipp/'

# This is the path to the value file a .csv file where all the
# fields to be used in the call scenario files are held.
# end the path with the forward slash /
# The full absolute path is %SIPP_PATH%%VALUE_PATH%
VALUE_PATH='values/'

# This is the path to the call scenario files are held.
# end the path with the forward slash /
# The full absolute path is %SIPP_PATH%%SCN_PATH%
SCN_PATH='scenarios/'

# This is the relative path where the output files from the call scenario are held.
# A directory named after the date of the test run is created and then
# under that directory is the results.log for all the tsts for that day
# A set of directorys are created each with the name of the test and a timestamp
# all files collected as a result of that test run are stored in this directory
# end the path with the forward slash /
# The full absolute path is %SIPP_PATH%%OUTPUT_PATH%
OUTPUT_PATH='results/'

######## END PATH SETTINGS ###############


#This is the absolute path to the version of SIPp that is to be used
# e.g. /opt/sipp-mod/sipp
SIPP='/opt/sipp-mod/sipp'


################FILE SETTINGS #####################
# The actual value file name the recommended name format is
# <ENV>-<ID> The id can be anything that provides the tester
# with a sufficiently unambigous recognisable name
VALUE_CSV=dev-mm2-child.csv

# The actual scenario instances that are in use
# the valid options are 1, 2 or both.
# The default is both 
SCN_INUSE=both

# The name of what is typically the Registration XML scenario file is SCN1 or 'scenario one' this is often
# left as the same standard registration scenario except for when specific registration scenarios are to be tested.
SCN1_XML=Registration/UAC-REG.xml

# The name of the second call XML scenario file, SCN2 or 'scenario two'
# this can be overridden from the command line if the script is being directly ran from a shell
SCN2_XML=UAC_FEOAOT_PAI.xml

######################################################################
# Add the SBC IP address that you would like to use for the test; e.g.
###PROD 4250###
#122.56.253.104	MDR-SBC-0506-AGVCp
#122.56.253.105	MDR-SBC-0506-AGVCs
#122.56.254.199	HN-SBC-0102-AGVCp
#122.56.254.200	HN-SBC-0102-AGVCs
#122.56.254.104	WN-SBC-0304-AGVCp
#122.56.254.105	WN-SBC-0304-AGVCs
#122.56.255.104	CH-SBC-0304-AGVCp
#122.56.255.105	CH-SBC-0304-AGVCs
###PROD 4500###
#122.56.253.231	PAK-SBC-0102-AGVCp
#122.56.253.232	PAK-SBC-0102-AGVCs
#122.56.254.167	PRO-SBC-0102-AGVCp
#122.56.254.168	PRO-SBC-0102-AGVCs
#122.56.255.167	RIC-SBC-0102-AGVCp
#122.56.255.168	RIC-SBC-0102-AGVCs
###NIL 4250###
#10.111.111.244	GFO-SD-A
#10.111.111.245	GVC2-SD-Ap
#10.111.111.246	GVC2-SD-As
###NIL 4500###
#10.207.44.87		NIL-SBC-0506-AGVCp
#10.207.44.88		NIL-SBC-0506-AGVCs
#10.207.44.89		NIL-SBC-0506-AGFO
SBC_IP=10.207.44.87

# SBC port by default this is 5060
SBC_PORT=5060


############# OTHER SETTINGS ####################
# Target directory number for the call XML file this element
# can be overiden on the command line for ease of switching.
TARGET_DN=123

# Options that should be tacked on the end just before the Target DN
OPTIONS="-l 2 -m 2 -trace_msg -trace_err -trace_screen -trace_stat"

# Set the display that sipp will use when SCN_INUSE equals both,
# valid options are rx, or main. If only one scenario is being used
# then this parameter is ignored.
SIPP_DISPLAY=rx

' > ${CONF_FILE}

fi
}

function chk-sbc {
    echo "Checking IPv4 Addresses in order" ${IP_ADDR}
for ADDR in $IP_ADDR
 
do
    echo "Checking IPv4 Address" ${ADDR}
	nc -z -s ${ADDR} -w 3 ${SBC_IP} ${SBC_PORT}
	if test $? -eq 0
	then
		IPv4_ADDRESS=$ADDR
              DEFAULT_IF=$(ip route show | grep ${IPv4_ADDRESS} | awk '{print $3}')
		info "Local Source IP address ${IPv4_ADDRESS}"
		info "Source Interface ${DEFAULT_IF}" 
		break
	else
		info "${bold}ERROR!${normal} SBC ${SBC_IP} port ${SBC_PORT} is not reachable using local IP ${ADDR}, check configuration and run again."
	fi
done
# END chk-sbc
}

function start_pcap {
#  screen -dm -t Trace_PC bash -c "/opt/sipp/start_pcap root@192.168.0.3" 
#  read -p "press return $!" TEMP
 :
}

function stop_pcap {
 # screen -r $(ps aux | grep 'Trace_PC' | awk '{print $2}')
 :
}

function pre_sipp {
 # display the titleto the console
 pTitle "SIPp:"

 # check the files we need exist.
 # this file is common whether we have 1 or 2 scenarios
 # check value file
 if [ -f "${SIPP_PATH}${VALUE_PATH}${VALUE_CSV}" ]; then
    VALUE="${SIPP_PATH}${VALUE_PATH}${VALUE_CSV}"
 else
    info "ERROR! Unable to locate ${VALUE_CSV} file at ${SIPP_PATH}${VALUE_PATH}"
    info "       check ${PROG}.conf file and try again."
    exit 2
 fi

 # check scn1 xml file if SCN_INUSE =1 or both
 if [ "$SCN_INUSE" = "1" ] || [ "$SCN_INUSE" = "both" ]; then
    if [ -f "${SIPP_PATH}${SCN_PATH}${SCN1_XML}" ]; then
      CALL1="${SIPP_PATH}${SCN_PATH}${SCN1_XML}"
      echo $(timestamp) : Launching test "${SCN1_XML}" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
    else
      info "ERROR! Unable to locate file ${SIPP_PATH}${SCN_PATH}${SCN1_XML}"
      info "       check ${PROG}.conf file and try again."
      exit 2
    fi
 fi

 # check if scn2 xml file exists if SCN_INUSE =2 or both
 if [ "$SCN_INUSE" = "2" ] || [ "$SCN_INUSE" = "both" ]; then
    if [ -f "${SIPP_PATH}${SCN_PATH}${SCN2_XML}" ]; then
      CALL2="${SIPP_PATH}${SCN_PATH}${SCN2_XML}"
      echo $(timestamp) : Launching test "${SCN2_XML}" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
    else
      info "ERROR! Unable to locate file ${SIPP_PATH}${SCN_PATH}${SCN2_XML}"
      info "       check ${PROG}.conf file and try again."
      exit 2
    fi
 fi
 # Check that the sip server is open for business.
 chk-sbc
 	if test $? -ne 0; then exit 1; fi

 # Check the number of sceanrios required
 # and creat the scenario file expectation based upon this
 case ${SCN_INUSE} in
    1) tSCENARIOS='-inf "${VALUE}" -sf "${CALL1}"' ;;
    2) tSCENARIOS='-sf "${CALL2}" -inf "${VALUE}"' ;;
    both) tSCENARIOS='-inf "${VALUE}" -sf "${CALL1}" -rxsf "${CALL2}" -rxinf "${VALUE}"' ;;
  esac
# SCENARIOS=$tSCENARIOS
 SCENARIOS=$(eval echo $tSCENARIOS)

 #SIPP_Display is only valid if both is selected for SCN_INUSE, otherwise main is the only valid option
 if [ $SCN_INUSE != "both" ]; then
   SIPP_DISPLAY="main"
 fi


 # check if scenario has call script running overide instructions
 match=$(awk -F\-\- '/CALLEG/ {gsub (/(^ *CALLEG: *| *$)/,"",$2); print $2}' "${SIPP_PATH}${SCN_PATH}${SCN2_XML}")
 if [ -z ${match} ]; then
   info "No Scenario Overide found"
   result="${SIPP} ${SBC_IP}:${SBC_PORT} ${SCENARIOS} -i ${IPv4_ADDRESS} ${OPTIONS} -s ${TARGET_DN}"  
 else
   info "Scenario Overide found"
   result=$(eval echo $match)
 fi
}

function func_sipp {

 # trap any user end requests and perform clean-up
 trap clean_up SIGHUP SIGINT SIGTERM
 start_pcap

 # Run new shell process that requests in the future to change the SIPp display screen.
 screen -dm -t Display_Call bash -c "sleep 3 ; echo \"cset display ${SIPP_DISPLAY}\" >/dev/udp/127.0.0.1/8888 "
 
 # create a varable holding datetime 
 NOW=(`date +%R`)
 
 # work out which scn 1 or 2 is the primary for outputing results.
 if [ "$SCN_INUSE" = "2" ] || [ "$SCN_INUSE" = "both" ]; then
   OP=${SCN2_XML%.*}
 else
   OP=${SCN1_XML%.*}
 fi

 #create output directory if it does not exist
 # OP=${SCN2_XML%.*}
 OUT_DIR="${SIPP_PATH}${OUTPUT_PATH}${TODAY}/${OP##*/}/${NOW}"
 if [ ! -d "${SIPP_PATH}${OUTPUT_PATH}${TODAY}/${OP##*/}" ]; then mkdir "${SIPP_PATH}${OUTPUT_PATH}${TODAY}/${OP##*/}"; fi
 if [ ! -d ${OUT_DIR} ]; then mkdir ${OUT_DIR}; fi 
 
 # create test case INFO file 
 touch /${OUT_DIR}/start

 # log the start time into the results information pack
 info "Test Run Start"

 # log the full command line to be executed into the results information pack
 info $result
 $result
 case "$?" in 
        0) 
            info "All calls were successful" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            info $(timestamp)" : Reg '${CALL1}' Success" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            ;;
        1)
            info "At least one call failed" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            info "$(timestamp) : Reg '${CALL1}' Failed" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            ;;
        97)
            info "exit on internal command. Calls may have been processed" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            info "$(timestamp) : Reg '${CALL1}' Unknown" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            ;;
        99)
            info "Normal exit without calls processed" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            info "$(timestamp) : Reg '${CALL1}' Not Run" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            ;;
        -1)
            info "Fatal Error" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            info "$(timestamp) : Reg '${CALL1}' Error" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            ;;
        -2)
            info "Fatal Error - Check IP binding" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            info "$(timestamp) : Reg '${CALL1}' Error" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            ;;
        *)
            info "$(timestamp) : Reg '${CALL1}' failed" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
            info ${SIPP} ${SBC_IP}:${SBC_PORT} -sf "${CALL1}" -inf "${VALUE}" -rxsf "${CALL2}" -rxinf "${VALUE}" ${OPTIONS} -s ${TARGET_DN}
            ;;
 esac
# END func_sipp 
}

function post_sipp {
 # gathers up files associated to SCN2 call
 # and moves them to the correct results directory
 if [ -d ${SIPP_PATH}${SCN_PATH} ]; then 
   cd ${SIPP_PATH}${SCN_PATH}
 else
   info "Error! ${SIPP_PATH}${SCN_PATH} does not exist." 
 fi 
 find . -cnewer ${OUT_DIR}/start -type f -exec mv {} "${OUT_DIR}" \;

 # copy the value file used
 cp "${VALUE}" "${OUT_DIR}"

echo "Call1 = ${CALL1}"
echo "outdir = ${OUT_DIR}"
 # copy the reg xml file
 if [ $SCN1  = "true" ] ; then
   cp "${CALL1}" "${OUT_DIR}"
 fi

echo "Call2 = ${CALL2}"
echo "outdir = ${OUT_DIR}"
 #copy the call xml file
 if [ "$SCN2" = "true" ] ; then
   cp "${CALL2}" "${OUT_DIR}"
 fi

 
 ################Voipmonitor
 # director is voipmonitor_base/date/ under this structure hours and mins folders are constructed
 # current approch is get all files below the date that changed since the start file was created.
 if [ -d "/var/spool/voipmonitor/" ]; then 
   cd "/var/spool/voipmonitor/"`date +%Y-%m-%d`
   find . -cnewer ${OUT_DIR}/start -type f -exec cp {} "${OUT_DIR}" \;
 else
   info "Warning! "/var/spool/voipmonitor/" does not exist, so not local packet capture results." 
 fi

# cd "/var/spool/voipmonitor/"`date +%Y-%m-%d`
# find . -cnewer ${OUT_DIR}/start -type f -exec cp {} "${OUT_DIR}" \;

# END post_sipp
}

function clean_up {
 # Perform program exit housekeeping
 echo "Cleaning up...."
 screen -r -t Trace_PC bash -c stop_pcap 
 info "Test Run End" 
 post_sipp
 if [ -f /${OUT_DIR}/start ]; then rm -f /${OUT_DIR}/start; fi
 echo " ================================================================================" >> ${SIPP_PATH}${OUTPUT_PATH}${TODAY}/results.txt
 exit
}


##############################
# END OF FUNCTIONS
##############################


##############################
# CODE:
# This area starts the script
# and joins all functions 
# together to run the script.
##############################


##---------------------
# OS ENVIRONMENT CHECKS
#---------------------
func_chkRoot
get-conf
chk_dir

 
# Reset is necessary if getopts was used previously in the script.
# TODO: It is a good idea to make this local in a function.
OPTIND=1 

# loop through to deal with command line provided options
while getopts "hvsf:d:p:" opt; do
    case "$opt" in
        h | -help)  func_usage
            exit 0
            ;;
        v)  show_ver
            exit 0
            ;;
        s)  show-conf
            exit 0
            ;;
        f)  SCN2_XML=$OPTARG
            ;;
        d)  TARGET_DN=$OPTARG
            ;;
        p)  SBC_IP=$OPTARG
            ;;
        '?')
            func_usage >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Shift off the options and optional --.

#echo "Target=$TARGET_DN, Scenario='$SCN2_XML', Leftovers: $@"
#echo "Target DN=$TARGET_DN, Scenario='$SCN2_XML'"
OPTIONS=${OPTIONS}"$@"
#echo "Target=$TARGET_DN, Scenario='$SCN2_XML', Leftovers='$OPTIONS'"
# End of command line parsing

# Call following functions to continue
pre_sipp
func_sipp
clean_up


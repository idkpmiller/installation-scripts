#!/bin/bash
##############################
# by Paul Miller
##############################
# DESCRIPTION:
# Script to provide menu
# control to sipp cli script
##############################


##############################
# VARABLES:
# section sets defaults & 
# script variables needed
##############################


PROG=$(basename $0)
CONF_FILE='sipp-UAS.conf'
_temp="/tmp/answer.$$"

trap "rm -f $_temp" 0 1 2 5 15

do_Back() {
 ../sippit.sh
}

do_finish() {
 echo "Normal Close - User exited $PROG."
 rm -f /tmp/*$$
 exit 
}

error_val() {
 RET2=$1
 whiptail --title "Error!" --msgbox "The value entered is not accetable.\nPlease try again\n $RET2\n" 9 52
 $RET2
}

#get_ipaddr() {
# IPADDR=""
# IPADDR=$(whiptail --inputbox "IP Address or Hostname to check?" 8 80 $IPADDR --title "Enter Address" 3>&1 1>&2 2>&3)
# retv=$?
# if [ $retv = 0 ]; then
#    # check that entry is not blank
#    if [ ! -z "$IPADDR" ]; then
#      echo "IP Addr field is not blank"
#    else
#      error_val "get_ipaddr"
#    fi
# fi
#}

#get_port() {
# PORT=
# PORT=$(whiptail --inputbox "Port No. to check [1-65535]?" 8 80 $PORT --title "Enter Port" 3>&1 1>&2 2>&3)
# retv=$?
# if [ $retv = 0 ]; then
#    # check if the input falls in the valid range
#    if [[ "$PORT" =~ ^[0-9]+$ ]] && [ "$PORT" -ge 1 -a "$PORT" -le 65535 ]; then
#      echo "Port is valid"
#    else
#      echo "Port is invalid"
#      error_val "get_port"
#    fi
# fi
#}

set_TARGET_DN() {
 VAR=$(whiptail --inputbox "Enter the destination number" 8 80 --cancel-button Cancel --ok-button Select $TARGET_DN --title "Destination Number" 3>&1 1>&2 2>&3)
 retv=$?
 if [ $retv = 0 ]; then
    # check that entry is not blank
    if [ ! -z "$VAR" ]; then
      sed -i "s/^\(TARGET_DN\s*=\s*\).*\$/\1$VAR/" $CONF_FILE
    else
      error_val "set_TARGET_DN"
    fi
 else
   # assume cancel
   do_EditConfig
 fi
}


Fb_SCN1_XML() {
 filter=".xml"
 # check dependacies before collecting new path data
 if [ ! -d "$SIPP_PATH" ]; then
   whiptail --title "Error!" --msgbox "Before setting this file the SIPP_PATH MUST be set to a valid directory.\n" 9 52
   do_EditConfig
 fi

 if [ ! -d "$SIPP_PATH$SCN_PATH" ]; then
   whiptail --title "Error!" --msgbox "Before setting this file the SCN_PATH MUST be set to a valid directory.\n" 9 52
   do_EditConfig
 fi
 
 # Directory dependacie checks passed so set check variables for later
 # substring manipulation
 prePath=$SIPP_PATH$SCN_PATH
 strLen=${#prePath}
 
 # Check is provided arguamnet was empty
 if [ -z $1 ]; then
   imgpath=$(ls -lhp / | awk -F ' ' ' { print $9 " " $5 } ')
 else
   imgpath=$(ls -lhp "/$1" | awk -F ' ' ' { print $9 " " $5 } ')
 fi

 # should always have a argument presented so really just belt and braces
 if [ -z $1 ]; then
   pathselect=$(whiptail --menu "Select $filter File" 20 80 12 --cancel-button Cancel --ok-button Select $imgpath 3>&1 1>&2 2>&3)
 else
   pathselect=$(whiptail --menu "Select $filter File" 20 80 12 --cancel-button Cancel --ok-button Select ../ BACK $imgpath 3>&1 1>&2 2>&3)
 fi
 RET=$?
 if [ $RET -eq 1 ]; then
   # control what happens when the user hits Cancel
    do_EditConfig
 elif [ $RET -eq 0 ]; then
   if [[ -d "/$1$pathselect" ]]; then
     Fb_SCN1_XML "/$1$pathselect"
   elif [[ -f "/$1$pathselect" ]]; then
     filename=`readlink -m $1$pathselect`
     SCN1_XML=${filename:strLen}

     # Final check to make sure the file selected is under the correct directory structure.
     if [ -f "$SIPP_PATH$SCN_PATH$SCN1_XML" ]; then
       sed -i "s|^\(SCN1_XML\s*=\s*\).*\$|\1'${SCN1_XML}'|" $CONF_FILE
     else
       whiptail --title "Error!" --msgbox "Change NOT accepted. selected file must exist under the $SIPP_PATH$SCN_PATH Directory\n" 9 52
       do_EditConfig
     fi
   fi
 fi
}


Fb_SCN2_XML() {
 filter=".xml"
 # check dependacies before collecting new path data
 if [ ! -d "$SIPP_PATH" ]; then
   whiptail --title "Error!" --msgbox "Before setting this file the SIPP_PATH MUST be set to a valid directory.\n" 9 52
   do_EditConfig
 fi

 if [ ! -d "$SIPP_PATH$SCN_PATH" ]; then
   whiptail --title "Error!" --msgbox "Before setting this file the SCN_PATH MUST be set to a valid directory.\n" 9 52
   do_EditConfig
 fi
 
 # Directory dependacie checks passed so set check variables for later
 # substring manipulation
 prePath=$SIPP_PATH$SCN_PATH
 strLen=${#prePath}
 
 # Check is provided arguamnet was empty
 if [ -z $1 ]; then
   imgpath=$(ls -lhp / | awk -F ' ' ' { print $9 " " $5 } ')
 else
   imgpath=$(ls -lhp "/$1" | awk -F ' ' ' { print $9 " " $5 } ')
 fi

 # should always have a argument presented so really just belt and braces
 if [ -z $1 ]; then
   pathselect=$(whiptail --menu "Select $filter File" 20 80 12 --cancel-button Cancel --ok-button Select $imgpath 3>&1 1>&2 2>&3)
 else
   pathselect=$(whiptail --menu "Select $filter File" 20 80 12 --cancel-button Cancel --ok-button Select ../ BACK $imgpath 3>&1 1>&2 2>&3)
 fi
 RET=$?
 if [ $RET -eq 1 ]; then
   # control what happens when the user hits Cancel
    do_EditConfig
 elif [ $RET -eq 0 ]; then
   if [[ -d "/$1$pathselect" ]]; then
     Fb_SCN2_XML "/$1$pathselect"
   elif [[ -f "/$1$pathselect" ]]; then
     filename=`readlink -m $1$pathselect`
     SCN2_XML=${filename:strLen}

     # Final check to make sure the file selected is under the correct directory structure.
     if [ -f "$SIPP_PATH$SCN_PATH$SCN2_XML" ]; then
       sed -i "s|^\(SCN2_XML\s*=\s*\).*\$|\1'${SCN2_XML}'|" $CONF_FILE
     else
       whiptail --title "Error!" --msgbox "Change NOT accepted. selected file must exist under the $SIPP_PATH$SCN_PATH Directory\n" 9 52
       do_EditConfig
     fi
   fi
 fi
}

Fb_VALUE_CSV() {
 filter=".csv"
 # check dependacies before collecting new path data
 if [ ! -d "$SIPP_PATH" ]; then
   whiptail --title "Error!" --msgbox "Before setting this file the SIPP_PATH MUST be set to a valid directory.\n" 9 52
   do_EditConfig
 fi

 if [ ! -d "$SIPP_PATH$VALUE_PATH" ]; then
   whiptail --title "Error!" --msgbox "Before setting this file the VALUE_PATH MUST be set to a valid directory.\n" 9 52
   do_EditConfig
 fi
 
 # Directory dependacie checks passed so set check variables for later
 # substring manipulation
 prePath=$SIPP_PATH$VALUE_PATH
 strLen=${#prePath}
 

 # Check is provided arguamnet was empty
 if [ -z $1 ]; then
   imgpath=$(ls -lhp / | awk -F ' ' ' { print $9 " " $5 } ')
 else
   imgpath=$(ls -lhp "/$1" | awk -F ' ' ' { print $9 " " $5 } ')
 fi

 # should always have a argument presented so really just belt and braces
 if [ -z $1 ]; then
   pathselect=$(whiptail --menu "Select $filter File" 20 80 12 --cancel-button Cancel --ok-button Select $imgpath 3>&1 1>&2 2>&3)
 else
   pathselect=$(whiptail --menu "Select $filter File" 20 80 12 --cancel-button Cancel --ok-button Select ../ BACK $imgpath 3>&1 1>&2 2>&3)
 fi
 RET=$?
 if [ $RET -eq 1 ]; then
   # control what happens when the user hits Cancel
    do_EditConfig
 elif [ $RET -eq 0 ]; then
   if [[ -d "/$1$pathselect" ]]; then
     Fb_VALUE_CSV "/$1$pathselect"
   elif [[ -f "/$1$pathselect" ]]; then
     filename=`readlink -m $1$pathselect`
     VALUE_CSV=${filename:strLen}

     # Final check to make sure the file selected is under the correct directory structure.
     if [ -f "$SIPP_PATH$VALUE_PATH$VALUE_CSV" ]; then
       sed -i "s|^\(VALUE_CSV\s*=\s*\).*\$|\1'${VALUE_CSV}'|" $CONF_FILE
     else
       whiptail --title "Error!" --msgbox "Change NOT accepted. selected file must exist under the $SIPP_PATH$VALUE_PATH Directory\n" 9 52
       do_EditConfig
     fi
   fi
 fi
}


set_SCN1_XML() {
Fb_SCN1_XML $SIPP_PATH$SCN_PATH
}

set_SCN2_XML() {
Fb_SCN2_XML $SIPP_PATH$SCN_PATH
}

set_VALUE_CSV() {
Fb_VALUE_CSV $SIPP_PATH$VALUE_PATH
}


set_SCN_INUSE() {
 # gets the user input of which call scenario are in play
 # options are 1, 2, or both.
 SCN_INUSE=$(whiptail --title "Which Scenario to use?" --radiolist "Use Scenario:" 10 40 4 \
  1 "" off \
  2 "" off \
  both "" on \
  3>&1 1>&2 2>&3)
 retv=$?
 if [ $retv = 0 ]; then
   sed -i "s|^\(SCN_INUSE\s*=\s*\).*\$|\1'${SCN_INUSE}'|" $CONF_FILE
 else
   # assume cancel
   do_EditConfig
 fi
}

set_SBC_IP() {
 VAR=$(whiptail --inputbox "SBC IPv4 address?" 8 80 $SBC_IP --title "Enter IP Address" 3>&1 1>&2 2>&3)
 retv=$?
 if [ $retv = 0 ] && [  -z "VAR" ]; then
   if [ -z $VAR ]; then whiptail --title "Error!" --msgbox "Entry cannot be blank." 10 30; return 1; fi
   sed -i "s/^\(SBC_IP\s*=\s*\).*\$/\1$VAR/" $CONF_FILE
 else
   # assume cancel
   do_EditConfig
 fi
}

set_SBC_PORT() {
 VAR=$(whiptail --inputbox "SBC Port No. [5060]?" 8 80 $SBC_PORT --title "Enter Port No." 3>&1 1>&2 2>&3)
 retv=$?
 if [ $retv = 0 ] && [  -z "VAR" ]; then
   if [ -z $VAR ]; then whiptail --title "Error!" --msgbox "Entry cannot be blank." 10 30; return 1; fi
   sed -i "s/^\(SBC_PORT\s*=\s*\).*\$/\1$VAR/" $CONF_FILE
 else
   # assume cancel
   do_EditConfig
 fi
}

set_SIPP_DISPLAY() {
 # gets the user input of which call to display when both scenario are in play
 # options are main or rx.
 SIPP_DISPLAY=$(whiptail --title "Which Scenario to display when both are being used?" --radiolist "Display Scenario:" 10 70 4 \
  rx "Changes to second callscenario after a few seconds." on \
  main "Stays with the first call scenario." off \
  3>&1 1>&2 2>&3)
 retv=$?
 if [ $retv = 0 ]; then
   sed -i "s|^\(SIPP_DISPLAY\s*=\s*\).*\$|\1'${SIPP_DISPLAY}'|" $CONF_FILE
 else
   # assume cancel
   do_EditConfig
 fi
}

set_OPTIONS() {
 VAR=$(whiptail --inputbox "Enter the string of options that you want to use" 8 80 -- "$OPTIONS" --title "Customise Options" 3>&1 1>&2 2>&3)
 retv=$?
 if [ $retv = 0 ]; then
    sed -i "s|^\(OPTIONS\s*=\s*\).*\$|\1'${VAR}'|" $CONF_FILE
 else
   # assume cancel
   do_EditConfig
 fi
}

set_SIPP_PATH() {
 VAR=$(whiptail --inputbox "Enter the path for SIPp" 8 80 $SIPP_PATH --title "SIPp Path" 3>&1 1>&2 2>&3)
 retv=$?
 if [ $retv = 0 ]; then
   if [ -z $VAR ]; then whiptail --title "Error!" --msgbox "Entry cannot be blank." 10 30; return 1; fi
   sed -i "s|^\(SIPP_PATH\s*=\s*\).*\$|\1'${VAR}'|" $CONF_FILE
 fi
}

set_VALUE_PATH() {
  curdir=$(pwd)
  if [ -d "${SIPP_PATH}" ]; then
    fileroot=${SIPP_PATH}
    cd ${fileroot}
    IFS_BAK=$IFS
    IFS=$'\n'
    menuitems=
    array=
    array=( $(ls -d */) )
    n=0
    for item in ${array[@]}
    do
      menuitems="$menuitems $n ${item// /_}" # subst. Blanks with "_"  
      let n+=1
    done

    IFS=$IFS_BAK
    dialog --backtitle "Path Selection" \
         --title "Select a directory" --menu \
         "Choose one of the directories that are \nlocated in ${fileroot} to hold the value files (.csv)" 16 40 8 $menuitems 2> $_temp
    if [ $? -eq 0 ]; then
      item=`cat $_temp`
      selection=${array[$(cat $_temp)]}
      cd $curdir
      sed -i "s|^\(VALUE_PATH\s*=\s*\).*\$|\1'${selection}'|" $CONF_FILE
    fi
  else
    whiptail --title "Error!" --msgbox "SIPp_Path must be set to a valid directory before setting this path." 10 30
    cd $curdir
    return
  fi
cd $curdir
}

set_SCN_PATH() {
  curdir=$(pwd)
  if [ -d "${SIPP_PATH}" ]; then
    fileroot=${SIPP_PATH}
    cd ${fileroot}
    IFS_BAK=$IFS
    IFS=$'\n'
    menuitems=
    array=
    array=( $(ls -d */) )
    n=0
    for item in ${array[@]}
    do
      menuitems="$menuitems $n ${item// /_}" # subst. Blanks with "_"  
      let n+=1
    done

    IFS=$IFS_BAK
    dialog --backtitle "Path Selection" \
         --title "Select a directory" --menu \
         "Choose one of the directories that are \nlocated in ${fileroot} to hold the scenarios files (.xml)" 16 40 8 $menuitems 2> $_temp
    if [ $? -eq 0 ]; then
      item=`cat $_temp`
      selection=${array[$(cat $_temp)]}
      cd $curdir
      sed -i "s|^\(SCN_PATH\s*=\s*\).*\$|\1'${selection}'|" $CONF_FILE
    fi
  else
    whiptail --title "Error!" --msgbox "SIPp_Path must be set to a valid directory before setting this path." 10 30
    cd $curdir
    return
  fi
cd $curdir
}


set_OUTPUT_PATH() {
  curdir=$(pwd)
  if [ -d "${SIPP_PATH}" ]; then
    fileroot=${SIPP_PATH}
    cd ${fileroot}
    IFS_BAK=$IFS
    IFS=$'\n'
    menuitems=
    array=
    array=( $(ls -d */) )
    n=0
    for item in ${array[@]}
    do
      menuitems="$menuitems $n ${item// /_}" # subst. Blanks with "_"  
      let n+=1
    done

    IFS=$IFS_BAK
    dialog --backtitle "Path Selection" \
         --title "Select a directory" --menu \
         "Choose one of the directories that are \nlocated in ${fileroot} to hold the resulting output files for the test cases." 16 40 8 $menuitems 2> $_temp
    if [ $? -eq 0 ]; then
      item=`cat $_temp`
      selection=${array[$(cat $_temp)]}
      cd $curdir
      sed -i "s|^\(OUTPUT_PATH\s*=\s*\).*\$|\1'${selection}'|" $CONF_FILE
    fi
  else
    whiptail --title "Error!" --msgbox "SIPp_Path must be set to a valid directory before setting this path." 10 30
    cd $curdir
    return
  fi
cd $curdir
}


set_SIPP() {
 VAR=$(whiptail --inputbox "Path and filename to run SIPp process" 8 80 $SIPP --title "SIPp binary" 3>&1 1>&2 2>&3)
 retv=$?
 if [ $retv = 0 ] && [  -z "VAR" ]; then
   sed -i "s|^\(SIPP\s*=\s*\).*\$|\1'${VAR}'|" $CONF_FILE
 fi
}

do_ShowConfig() {
 # Displays the current configuration to the user
  cat ./${CONF_FILE} | grep -Ev '(#.*$)|(^$)' 1> /tmp/temp
  MESSAGE=$(cat /tmp/temp)
  whiptail --title "Show Configuration" --msgbox "${MESSAGE}" 20 80 
  rm -f /tmp/temp
}

do_EditConfig() {
tempfile=/tmp/dialog_$$
trap "rm -f $tempfile" 0 1 2 5 15

source ./${CONF_FILE}

dialog --title "SIPp 1 Editor" \
           --menu "Select and Modify:" 20 80 12 \
                   "TARGET_DN" "${TARGET_DN}" \
                   "SCN2_XML" "${SCN2_XML}" \
                   "SCN1_XML" "${SCN1_XML}" \
                   "SCN_INUSE" "${SCN_INUSE}" \
                   "VALUE_CSV" "${VALUE_CSV}" \
                   "SBC_IP" "${SBC_IP}" \
                   "SBC_PORT" "${SBC_PORT}" \
                   "SIPP_DISPLAY" "${SIPP_DISPLAY}" \
                   "OPTIONS" "${OPTIONS}" \
                   "SIPP_PATH" "${SIPP_PATH}" \
                   "VALUE_PATH" "${VALUE_PATH}" \
                   "SCN_PATH" "${SCN_PATH}" \
                   "OUTPUT_PATH" "${OUTPUT_PATH}" \
                   "SIPP" "${SIPP}" \
                   "Back" "parent" \
                   "Q" "Quit to shell" 2> $tempfile
   retv=$?
   choice=$(cat $tempfile)
   [ $retv -eq 1 -o $retv -eq 255 ] && do_finish
   case $choice in
     TARGET_DN) set_TARGET_DN ;;
     SCN2_XML) set_SCN2_XML ;;
     SCN1_XML) set_SCN1_XML ;;
     SCN_INUSE) set_SCN_INUSE ;;
     VALUE_CSV) set_VALUE_CSV ;;
     SBC_IP) set_SBC_IP ;;
     SBC_PORT) set_SBC_PORT ;;
     SIPP_DISPLAY) set_SIPP_DISPLAY ;;
     OPTIONS) set_OPTIONS ;;
     SIPP_PATH) set_SIPP_PATH ;;
     VALUE_PATH) set_VALUE_PATH ;;
     SCN_PATH) set_SCN_PATH ;;
     OUTPUT_PATH) set_OUTPUT_PATH ;;
     SIPP) set_SIPP ;;
     Q) clear; exit 0 ;;
   esac
clear
}

do_SIPpVersion() {
  ./sipp-UAS.sh -v
  echo ""
  read -p "Press a key to continue" TEMP
  echo ""
}

do_RunSIPp() {
  ./sipp-UAS.sh
  echo ""
  read -p "Press a key to continue" TEMP
  echo ""
}

get_CONF () {
  source ./${CONF_FILE}
}


##############################
# END OF FUNCTIONS
##############################


##############################
# CODE:
# This area starts the script
# and joins all functions 
# together to run the script.
##############################


get_CONF

while true; do
  FUN=$(whiptail --menu "SIPp Menu" 20 80 12 --cancel-button Finish --ok-button Select \
    "ShowConfig" "Display the current configuration" \
    "EditConfig" "Change the current configuration" \
    "SIPpVersion" "Show the SIPp version information" \
    "RunSIPp" "Scan a list of hosts and port number in Production" \
    3>&1 1>&2 2>&3)
  RET=$?
  if [ $RET -eq 1 ]; then
    do_finish
  elif [ $RET -eq 0 ]; then
    "do_$FUN" || whiptail --msgbox "There was an error running do_$FUN" 20 60 1
  else
    exit 1
  fi
done


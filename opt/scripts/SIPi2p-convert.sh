#!/bin/bash

#####################################################
# Used to assist in converting SIP Inspector Pro scripts
# to SIPp xml scenario files
# 
# --------------------------------------------------
# 
# script developed by Paul Miller 2014
# Revision History
# ----------------
# 0.1 Paul Miller 09/03/2014 Initial release
# 
#################################################### 

##############################
# VARABLES:
# section sets defaults & 
# script variables needed
##############################

LASTINSTRUCT="NULL"
SCRIPTPATH=$(pwd -P)
PROG=$(basename $0)
SCRIPTPREF=${PROG%.*}
INFILE=$1
#MAPFILE=$2
OUTFILEBase=${INFILE%.*}
OUTFILE="${OUTFILEBase}".xml
#OUTFILE="${OUTFILEBase##*/}".xml

mode=
rxREQ="  <recv request="
rxRESP="  <recv response="
rxSTD=">"
rxAUTH=" auth=\"true\">"
rxOPT=" optional=\"true\">"

##############################
# END OF VARIABLES
##############################

##############################
# FUNCTIONS:
# Only functions should exist
# in this area 
##############################

function pTitle {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as arguament to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function chkRoot {
#pTitle "Checking for root Priviledges:"
  (( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
}

function usage {
	echo
	echo "USAGE: $0 SIP inspector Scenario Txt file]"
	exit
}

function chkInputs {
if [ -z $INFILE ]; then
	echo "ERROR: You must specify an input file."
	usage
fi
}

function prepSRC {
# Copy the INFILE to the a temporary file, we never work on the original!
tr -d '\r' < $INFILE > /tmp/temp$$.0

# Strip any whitespace or tabs from the start of each line in the OUTFILE
sed -i "s/^[ \\t]*//" /tmp/temp$$.0

# strip away Character 13 0x0Dh (CR)
sed 's/�//' /tmp/temp$$.0 > /tmp/temp$$
rm -f /tmp/temp$$.0

TMPOUTFILE=$(tr -d '\r' < /tmp/temp$$| sed "s/^[ \\t]*//" | sed "s/[ \\t]*$//")
rm -f /tmp/temp$$
#echo $TMPOUTFILE

}

function do_newinstruct {
  if [ $LASTINSTRUCT == "RECVMSG" ]; then
    echo "  </recv>" >> $OUTFILE
    echo "" >> $OUTFILE
  fi      

  if [ $LASTINSTRUCT == "SENDMSG" ]; then
    echo "" >> $OUTFILE
    echo "    ]]>" >> $OUTFILE
    echo "  </send>" >> $OUTFILE
    echo "" >> $OUTFILE
  fi 
}

function do_start_output {
# NUMOFLINES=$lineNo
 # Make sure no existing output file is present
 if [ -f $OUTFILE ]; then
	rm -rf $OUTFILE
 fi

 # this loop through should look at:
 # 1. Create new file
 # 2. Adding the xml header
 # 3. Working out if the call is a UAC or UAS scenario

 #Check scenario mode first.
 IFS=$'\n'
# lineNo=0
 for line in $TMPOUTFILE; do
#  let lineNo++
  case "$line" in 
    \-\-\-\-\-\-\-\-\-\-\> )
      mode="UAC"
      break
      ;;
    \<\-\-\-\-\-\-\-\-\-\-"*" )
      mode="UAS" 
      break
      ;;
  esac
 done

echo '<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE scenario SYSTEM "sipp.dtd">

<scenario name ="$mode scenario">

<!--  Call this script in conjunction with UAC-REG.xml if you need the endpoint to be registered -->
<!-- E.g. sipp 10.111.111.245:5060 -l 1 -m 1 -sf UAC-REG.xml -inf value.csv -rxsf scn2.xml -rxinf value.csv -trace_err -trace_msg  -->

<!-- CSV Fields: -->
<!--   0: SIP Domain    -->
<!--   1: From Username -->
<!--   2: AuthUsername -->
<!--   3: AuthPassword  -->

' > $OUTFILE
}

function do_COMMENT {
       do_newinstruct
       mline=${line#"//"}
       echo "<!-- " $mline " -->" >> $OUTFILE
       line=$mline
}


function do_SENDMSG {
       do_newinstruct
       echo "" >> $OUTFILE
       echo "  <send>" >> $OUTFILE
       echo "    <![CDATA[" >> $OUTFILE
       echo "" >> $OUTFILE
       LASTINSTRUCT="SENDMSG"
}

function do_SENDMSGopt {
       do_newinstruct
       echo "" >> $OUTFILE
       echo "<!-- send optional message -->" >> $OUTFILE
       echo "" >> $OUTFILE
       echo "  <send>" >> $OUTFILE
       echo "    <![CDATA[" >> $OUTFILE
       echo "" >> $OUTFILE
       LASTINSTRUCT="SENDMSG"
}

function do_RECVMSG {
       do_newinstruct
       echo "" >> $OUTFILE
       echo "<!-- awaiting to receive message -->" >> $OUTFILE
       LASTINSTRUCT="RECVMSG"
}

function do_RECVMSGopt {
       do_newinstruct
       echo "" >> $OUTFILE
       echo "<!-- may receive optional message -->" >> $OUTFILE
       LASTINSTRUCT="RECVMSGopt"
}

function do_CMD {
       do_newinstruct
       echo "" >> $OUTFILE
       echo " $line" >> $OUTFILE
       LASTINSTRUCT="CMD"
}

function do_OTHER {

  case "$LASTINSTRUCT" in
    RECVMSGopt )
      if [ $mode == "UAC" ]; then
        if [ ${line:0:3} == 401 ]; then 
          echo "${rxRESP}""\"""${line:0:3}""\"""${rxAUTH}" >> $OUTFILE
        else
          echo "${rxRESP}""\"""${line:0:3}""\"""${rxOPT}" >> $OUTFILE
        fi     
      else
        echo "${rxREQ}""\"""${line}""\"""${rxOPT}" >> $OUTFILE
      fi
      echo "  </recv>" >> $OUTFILE
      echo "" >> $OUTFILE
      LASTINSTRUCT="NULL"
      ;;
    RECVMSG )
      if [ $mode == "UAC" ]; then
        if [ ${line:0:3} == 401 ]; then 
          echo "${rxRESP}""\"""${line:0:3}""\"""${rxAUTH}" >> $OUTFILE
        else
          echo "${rxRESP}""\"""${line:0:3}""\"""${rxSTD}" >> $OUTFILE
        fi
      else
        echo "${rxREQ}""\"""${line}""\"""${rxSTD}" >> $OUTFILE
      fi
      echo "  </recv>" >> $OUTFILE
      echo "" >> $OUTFILE
      LASTINSTRUCT="NULL"
      ;;
    COMMENT )
      LASTINSTRUCT="NULL"
      ;;
    CMD )
      case "$line" in
        label=* )
            # not processing labels just remove them
            head -n -1 $OUTFILE > /tmp/temp.txt ; mv /tmp/temp.txt $OUTFILE
            ;;
        cmd_PlayPcap=* )
            # process audio files only at this time
            echo "<nop>" >> $OUTFILE             
            echo "  <action>" >> $OUTFILE
            echo "    <exec play_pcap_audio="${line:14}"/>" >> $OUTFILE
            echo "  </action>" >> $OUTFILE
            echo "</nop>" >> $OUTFILE
            ;;
        cmd_Pause=* )
            # simple pause support
            echo "<pause milliseconds="${line:10}"/>" >> $OUTFILE
            ;;
      esac
      ;;
    * ) 
      case "$line" in
       *----*if*==* )
            do_COMMENT
            ;;
       Authorization:* )
            echo "     Authorization: [authentication username=[field2] password=[field3]]" >> $OUTFILE
            ;;
       * )
            echo "     $line" >> $OUTFILE
            ;;
      esac
      ;;
  esac
}


function do_Mapping {
sed -i -e 's/toUsername/service/g' $OUTFILE
sed -i -e 's/fromUsername/field1/g' $OUTFILE
sed -i -e 's/authUsername/field2/g' $OUTFILE
sed -i -e 's/authPassword/field3/g' $OUTFILE
#sed -i -e 's/[authentication username=[field2]; password=[field3];]/[authentication username=[field2] password=[field3]]/g' $OUTFILE
sed -i -e 's/ver/sipp_version/g' $OUTFILE
}



function do_finish_output {

echo '
  <!-- Label for 'end of thread' -->
  <label id="99"/>

  <!-- definition of the response time repartition table (unit is ms)   -->
  <ResponseTimeRepartition value="10, 20, 30, 40, 50, 100, 150, 200"/>

  <!-- definition of the call length repartition table (unit is ms)     -->
  <CallLengthRepartition value="10, 50, 100, 500, 1000, 5000, 10000"/>

</scenario>

' >> $OUTFILE
}

function do_closeout {

# Convert the file to UTF-8
#iconv -f US-ASCII -t UTF-8 -o $OUTFILE /tmp/$OUTFILE

# Remove Temp File
if [ -f /tmp/$OUTFILE ]; then
       echo "w00t1"
	rm -rf /tmp/$OUTFILE
fi

echo "$INFILE has been converted and saved to $OUTFILE"

do_finish_output
}

##############################
# END OF FUNCTIONS
##############################


##############################
# CODE:
# This area starts the script
# and joins all functions 
# together to run the script.
##############################


#---------------------
# OS ENVIRONMENT CHECKS
#---------------------

chkRoot
chkInputs
pTitle "Processing $INFILE"
prepSRC

#create a new converted file
do_start_output

# Loop through each line of the scenario and check for condition match
IFS=$'\n'
#lineNo=0

for line in $TMPOUTFILE; do
#let lineNo++
case "$line" in 
  //* )
      do_COMMENT 
      ;;
  \-\-\-\-\-\-\-\-\-\-\> )
      do_SENDMSG
      ;;
  \-\-\-\-\-\-\-\-\-\-\>"*" )
      do_SENDMSGopt
      ;;
  "<----------" )
      do_RECVMSG
      ;;
  \<\-\-\-\-\-\-\-\-\-\-"*" )
      do_RECVMSGopt
      ;;
  "----------" )
      do_CMD
      ;;
  * ) 
      do_OTHER
      ;;
esac
done
 
do_closeout
do_Mapping

echo "Script complete."

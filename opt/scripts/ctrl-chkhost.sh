#!/bin/bash

##############################
# VARABLES:
# section sets defaults & 
# script variables needed
##############################

SCRIPTPATH=$(pwd -P)
PROG=$(basename $0)
_temp="/tmp/answer1.$$"

##############################
# END OF VARIABLES
##############################


##############################
# FUNCTIONS:
# Only functions should exist
# in this area 
##############################

do_Back() {
 ../sippit.sh
}

do_finish() {
  echo "Close - User exited $PROG."
  rm -f /tmp/temp
  rm -f /tmp/answer*.$$
  exit
}

error_val() {
RET2=$1
  dialog --backtitle "Error!" \
    --msgbox "The value entered is not accetable.\nPlease try again\n $RET2\n" 9 52
$RET2
}

get_ipaddr() {
IPADDR=
IPADDR=$(whiptail --inputbox "IP Address or Hostname to check?" 8 80 $IPADDR --title "Enter Address" 3>&1 1>&2 2>&3)
exitstatus=$?
if [ $exitstatus = 0 ]; then
    # check that entry is not blank
    if [ ! -z "$IPADDR" ]; then
      echo "IP Addr field is not blank"
    else
      error_val "get_ipaddr"
    fi
else
    echo "User selected Cancel."
    do_finish
fi
}

get_port() {
PORT=
PORT=$(whiptail --inputbox "Port No. to check [1-65535]?" 8 80 $PORT --title "Enter Port" 3>&1 1>&2 2>&3)
exitstatus=$?
if [ $exitstatus = 0 ]; then
    # check if the input falls in the valid range
    if [[ "$PORT" =~ ^[0-9]+$ ]] && [ "$PORT" -ge 1 -a "$PORT" -le 65535 ]; then
      echo "Port is valid"
    else
      echo "Port is invalid"
      error_val "get_port"
    fi
else
    echo "User selected Cancel."
    do_finish
fi
}

do_PortScanCustom() {
  get_ipaddr
  get_port
  ./chk_hosts.sh ${IPADDR} ${PORT} 1> /tmp/temp
  MESSAGE=$(cat /tmp/temp)
  dialog --title "Output" --clear --msgbox "${MESSAGE}" 20 80 
  rm -f /tmp/temp
}


### File or Directory selection menu with dialog
do_PortScanList() {
    fileroot=$SCRIPTPATH
    IFS_BAK=$IFS
    IFS=$'\n'
    menuitems=
    array=
    array=( $(ls $fileroot | grep .list) )
    n=0
    for item in ${array[@]}
    do
        menuitems="$menuitems $n ${item// /_}" # subst. Blanks with "_"  
        let n+=1
        menuitems="$menuitems ${item// /_}" # subst. Blanks with "_" 
    done
    IFS=$IFS_BAK
    dialog --backtitle "Scan List Selection" \
           --title "Select a preconfigured scanlist" --menu \
           "Choose one of the preconfigured scanlist that are \nlocated in ${SCRIPTPATH}" 16 40 8 $menuitems 2> $_temp
    if [ $? -eq 0 ]; then
        item=`cat $_temp`
        selection=${array[$(cat $_temp)]}
       ./chk_hosts.sh -f $selection
       echo ""
       read -p "Press a key to continue" TEMP
       echo ""
    fi
}


##############################
# END OF FUNCTIONS
##############################


##############################
# CODE:
# This area starts the script
# and joins all functions 
# together to run the script.
##############################

# set trap 
trap do_finish INT QUIT TERM EXIT

#
# Interactive use loop
#
while true; do
  FUN=$(whiptail --menu "Port Scan" 20 80 12 --cancel-button Finish --ok-button Select \
    "PortScanCustom" "Check if a specific port on a host is open" \
    "PortScanList" "Select a List of Hosts and Ports to Scan (*.list)" \
    3>&1 1>&2 2>&3)
  RET=$?
  if [ $RET -eq 1 ]; then
    temp=
    do_finish
  elif [ $RET -eq 0 ]; then
    "do_$FUN" || whiptail --msgbox "There was an error running do_$FUN" 20 60 1
  else
    exit 1
  fi
done

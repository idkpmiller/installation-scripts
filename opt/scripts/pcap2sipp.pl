#! /usr/bin/perl -w

use strict;
use Data::Dumper;

sub	error ($$)
{
	my ($pkt, $str) = @_;

	print STDERR Dumper ($pkt);
	print STDERR "ignoring packet $pkt->{n} - $str\n";
	print "packet $pkt->{n} - ignoring - $str\n";
	die "\n";
}

my ($fin, $fout);

sub	read_pcap
{
	my ($buf);

	read $fin, $buf, 24 or die;
	my @gh = unpack 'LSSlLLL', $buf;

	$gh[0] == 0xa1b2c3d4 or die "bad magic $gh[0] not a pcap?";
	$gh[6] == 1 or die "bad link type $gh[6]";

	my $num;
	while (1) {
		$num++;
		read $fin, $buf, 16 or last;
		my @rh = unpack 'LLLL', $buf;
		$rh[2] == $rh[3] or die "packet $num truncated\n";
		read ($fin, $buf, $rh[2]) == $rh[2] or die "packet $num read: $!";
		eval {
			parse_eth ({ n => $num, ts_sec => $rh[0],
				ts_usec => $rh[1], len => $rh[2] }, $buf);
		};
		die $@ if $@;
	}
}

sub	parse_eth
{
	my ($pkt, $data) = @_;
	$pkt->{eth} = \my %eth;

	@eth{qw/ dst src type data /} = unpack 'H12H12na*', $data;

	if ($eth{type} == 0x0800) {
		parse_ip ($pkt, $eth{data});
	} else {
		error $pkt, "eth unknown type $eth{type}";
	}
}

sub	parse_ip
{
	my ($pkt, $data) = @_;

	$pkt->{ip} = \my %ip;

	$ip{len} = unpack 'x2n', $data;
	($data, $ip{pad}) = unpack "a$ip{len} a*", $data;
	@ip{qw/ hlen tos len id foffset ttl proto cksum src
		dst data /} = unpack 'CCnnnCCna4a4a*', $data;

	error $pkt, "bad ip" if $ip{hlen} != 0x45;
	
	$ip{flags} = $ip{foffset} >> 13;
	$ip{offset} = $ip{foffset} & 0x1fff;
	error $pkt, "ip fragmented" if ($ip{flags} & 1) || $ip{offset};

	$ip{src_addr} = join '.', unpack 'C4', $ip{src};
	$ip{dst_addr} = join '.', unpack 'C4', $ip{dst};

	if ($ip{proto} == 0x11) {
		parse_udp ($pkt, $ip{data});
	} else {
		die "not udp";
	}
}

sub	parse_udp
{
	my ($pkt, $data) = @_;

	$pkt->{udp} = \my %udp;

	@udp{qw/ src_port dst_port len chksum data /} =
		unpack "nnnna*", $data;

	do_sip ($pkt);
}

my $rrs;

sub	do_sip
{
	my ($pkt) = @_;

	my $sip = $pkt->{udp}{data};

	$sip =~ m!^(\S+)()\s+\S+\s+SIP/2.0\s! ||
	$sip =~ m!^SIP/2.0\s+()(\S+)\s!
		or die "not a sip";

	if ($pkt->{ip}{src_addr} eq $ARGV[1]) {
		$sip =~ s/\015\012/\n/g;
		$sip =~ s/^(Content-Length):\s\d+/$1: [len]/im;
		if (!$rrs) {
			$sip =~ s/^(Call-ID):.*/$1: [call_id]/im;
		}
		if ($rrs) {
			for (qw(
				Via From To Call-ID
			)) {
				$sip =~ s/^$_:.*/[last_$_:]/im;
			}
			$sip =~ s/^([A-Z]+)\s+\S+/$1 [next_url]/;
		}
		for (@ARGV[2..$#ARGV]) {
			eval "\$sip =~ $_";
			die $@ if $@;
		}
		print <<XML;
  <pause milliseconds = "100"/>
  <send>
    <![CDATA[
$sip
    ]]>
  </send>

XML
	} elsif ($pkt->{ip}{dst_addr} eq $ARGV[1]) {
		if ($1) {
			print <<XML;
  <recv request="$1" rrs="true"></recv>

XML
		} else {
			print <<XML;
  <recv response="$2" rrs="true"></recv>

XML
		}
		$rrs++;
	} else {
		die "ip addr is not to or from sipp";
	}
}

die "usage: $0 file.pcap sipp_ip_addr\n" if @ARGV < 2;

open $fin, '<', $ARGV[0] or die "open $ARGV[0]: $!";
binmode $fin;

print <<XML;
<?xml version="1.0" encoding="ISO-8859-1" ?>
<!DOCTYPE scenario SYSTEM "sipp.dtd">

<scenario name="$0 @ARGV">

XML

read_pcap ();

print <<XML;
</scenario>
XML
# you should have one UDP SIP call in your pcap.
# fragmented ip is not supported yet.
# usage:
# perl pcap2sipp.pl cfna_in.pcap 1.2.3.4 s/fhost:5060/[local_ip]:[local_port]/g s/fhost/[local_ip]/g >cfna_in.xml


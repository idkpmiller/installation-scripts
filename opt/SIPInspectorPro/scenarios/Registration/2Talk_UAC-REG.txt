////////////////////////////////////////////////////////////////////////////////
// REGISTER Alice scenario
// REGISTERing Alice: [field2]
//
// VAL file:
// SIP Domain : [field0] cpg.co.nz
//
// Summary:
// Registers and check for response. If 401 provide authentication,
// if  200 OK then complete the test.
//
// 
//  SIP Messages
//  ---> REGISTER
//  <--- 401*
//  ---> REGISTER*
//  <--- 200
//  End:
// 
// 
////////////////////////////////////////////////////////////////////////////////


---------->
REGISTER sip:[field0] SIP/2.0
Via: SIP/2.0/UDP 203.97.97.115:[local_port];branch=[branch]
Max-Forwards: 70
From: <sip:[authUsername]@[field0]>;tag=as134e0f58-[call_number]
To: <sip:[authUsername]@[field0]>
Call-ID: 0cf407b60a126144577195b1570cc89c-[call_number]
CSeq: 101 REGISTER
User-Agent: SIP Test
Expires: 600
Contact: <sip:[authUsername]@203.97.97.115:[local_port]>
Content-Length: 0

<----------*
100 Trying


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 482) goto End


---- if (Resp == 503) goto End


<----------
401 Unauthorized


----------
set_Variable=ext_ip "Via:" "received=*;"


----------
set_Variable=ext_port "Via:" "rport=*"


---------->
REGISTER sip:[field0] SIP/2.0
[last_Via:]�
[last_From:]�
[last_To:]�
[last_Call-ID:]�
CSeq: 10[cseq+1] REGISTER
Max-Forwards: 70
User-Agent: SIP Test
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Expires: 600
Contact: <sip:[authUsername]@[$ext_ip]:[$ext_port]>
Content-Length: 0


<----------*
100 Trying


---- if (Resp == 401) goto End


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 503) goto End


<----------
200 OK


----------
cmd_NewDialog


<----------*
NOTIFY


---------->*
SIP/2.0 200 OK
[last_Via:]
[last_From:]
[last_To:];tag=1d503533
[last_Call-ID:]
[last_CSeq:]
User-Agent: SIP Test
Contact: <sip:[local_ip]:[local_port]>
Content-Length: 0


----------
label=End


////////////////////////////////////////////////////////////////////////////////
// DeREGISTER TG Pilot 
// 
////////////////////////////////////////////////////////////////////////////////
// 10.2.2 Removing Bindings
//
// Registrations are soft state and expire unless refreshed, but can
// also be explicitly removed.  A client can attempt to influence the
// expiration interval selected by the registrar as described in Section
// 10.2.1.  A UA requests the immediate removal of a binding by
// specifying an expiration interval of "0" for that contact address in
// a REGISTER request.  UAs SHOULD support this mechanism so that
// bindings can be removed before their expiration interval has passed.
//
// The REGISTER-specific Contact header field value of "*" applies to
// all registrations, but it MUST NOT be used unless the Expires header
// field is present with a value of "0".
//
//   Use of the "*" Contact header field value allows a registering UA
//   to remove all bindings associated with an address-of-record
//   without knowing their precise values.
//
////////////////////////////////////////////////////////////////////////////////
// REG & CANCEL REG TG Pilot scenario
// REGISTERing pilot: [authUsername]
// Registers and check for response. If 401 provide authentication,
// once  200 OK then wait and finally cancel the registration to 
// complete the test.
//
// 
//  SIP Messages
//  ============
//
//  ---> REGISTER
//  <--- 100*
//  ---- if (Resp == 403) goto End
//  ---- if (Resp == 404) goto End
//  ---- if (Resp == 503) goto End
//  <--- 401*
//  ---> REGISTER*
//  <--- 100*
//  ---- if (Resp == 401) goto End
//  ---- if (Resp == 403) goto End
//  ---- if (Resp == 404) goto End
//  ---- if (Resp == 503) goto End
//  <--- 200
//  End:
//
//
//
// values field mapping
// [field0] - SIP Domain  
// 
////////////////////////////////////////////////////////////////////////////////


// remove ALL Trunk Group / AOR bindings for the specific site.

---------->
REGISTER sip:[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[authUsername]@[field0]>;tag=24de4a5dbe4ef
To: <sip:[authUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq] REGISTER
Max-Forwards: 70
Expires: 0
Contact: *
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Length: 0


<----------*
100 Trying


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 503) goto End


<----------*
401 Unauthorized


---------->*
REGISTER sip:[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[authUsername]@[field0]>;tag=24de4a5dbe4ef
To: <sip:[authUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] REGISTER
Max-Forwards: 70
Expires: 0
Contact: *
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Length: 0


<----------*
100 Trying


---- if (Resp == 401) goto End


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 503) goto End


<----------
200 OK


----------
label=End








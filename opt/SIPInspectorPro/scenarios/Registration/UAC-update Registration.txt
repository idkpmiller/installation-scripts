////////////////////////////////////////////////////////////////////////////////
// REGISTER TG Pilot wait a period of time and 
// then DeREGISTER
// 
////////////////////////////////////////////////////////////////////////////////
// 
//  SIP Messages
//  ============
//
//  ---> REGISTER
//  <--- 100*
//  ---- if (Resp == 403) goto End
//  ---- if (Resp == 404) goto End
//  ---- if (Resp == 503) goto End
//  <--- 401*
//  ---> REGISTER*
//  <--- 100*
//  ---- if (Resp == 401) goto End
//  ---- if (Resp == 403) goto End
//  ---- if (Resp == 404) goto End
//  ---- if (Resp == 503) goto End
//  <--- 200
//
//  Pause 10s
//
//  ---> REGISTER
//  <--- 100*
//  ---- if (Resp == 401) goto End
//  ---- if (Resp == 403) goto End
//  ---- if (Resp == 404) goto End
//  ---- if (Resp == 503) goto End
//  <--- 200
//  End:
//
//
//
// values field mapping
// [field0] - SIP Domain  
// 
////////////////////////////////////////////////////////////////////////////////


---------->
REGISTER sip:[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[authUsername]@[field0]>;tag=24de4a5dbe4ef
To: <sip:[authUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq] REGISTER
Max-Forwards: 70
Contact: <sip:[authUsername]@[local_ip]:[local_port];transport=[transport]>
Content-Length: 0


<----------*
100 Trying


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 503) goto End


<----------*
401 Unauthorized


---------->*
REGISTER sip:[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[authUsername]@[field0]>;tag=24de4a5dbe4ef
To: <sip:[authUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] REGISTER
Max-Forwards: 70
Contact: <sip:[authUsername]@[local_ip]:[local_port];transport=[transport]>
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Length: 0


<----------*
100 Trying


---- if (Resp == 401) goto End


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 503) goto End


<----------
200 OK


----------
cmd_Pause=10000

// update list of bindings by adding an email address


---------->
REGISTER sip:[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[authUsername]@[field0]>;tag=24de4a5dbe4ef
To: <sip:[authUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] REGISTER
Max-Forwards: 70
Contact: <mailto:bob@telecom.co.nz>
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Length: 0


<----------*
100 Trying


---- if (Resp == 401) goto End


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 503) goto End


<----------
200 OK


----------
label=End








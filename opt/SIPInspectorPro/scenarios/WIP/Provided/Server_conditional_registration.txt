
<----------
REGISTER

---------->
SIP/2.0 200 OK
[last_Via:]
[last_From:]
[last_To:];tag=[call_number]
[last_Call-ID:]
[last_CSeq:]
Expires: 3600
User-Agent: SIPInspector_v_[ver]
Content-Length: 0


----------
cmd_NewDialog

<----------
INVITE

---------->
SIP/2.0 100 Trying
[last_Via:]
[last_From:]
[last_To:]
[last_Call-ID:]
[last_CSeq:]
User-Agent: SIPInspector_v_[ver]
Content-Length: 0


---------->
SIP/2.0 180 Ringing
[last_Via:]
[last_From:]
[last_To:];tag=[call_number]
[last_Call-ID:]
[last_CSeq:]
Contact: <sip:[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
Content-Length: 0


---------->
SIP/2.0 200 OK
[last_Via:]
[last_From:]
[last_To:];tag=[call_number]
[last_Call-ID:]
[last_CSeq:]
Contact: <sip:[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
Content-Type: application/sdp
Content-Length: [len]

v=0
o=111 843670094 843670094 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 0 101
a=rtpmap:0 PCMU/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20

<----------
ACK

----------
cmd_PlayPcap=test.pcap

<----------
BYE

---------->
SIP/2.0 200 OK
[last_Via:]
[last_From:]
[last_To:]
[last_Call-ID:]
[last_CSeq:]
Contact: <sip:[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
Content-Length: 0

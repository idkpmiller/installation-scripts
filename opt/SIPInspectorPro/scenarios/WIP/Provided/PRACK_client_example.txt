
---------->
INVITE sip:[toUsername]@[remote_ip]:[remote_port] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[fromUsername]@[local_ip]:[local_port]>;tag=[call_number]
To: <sip:[toUsername]@[remote_ip]:[remote_port]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq] INVITE
Max-Forwards: 70
Require: 100rel
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
Content-Type: application/sdp
Content-Length: [len]

v=0
o=111 843670094 843670094 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 0 101
a=rtpmap:0 PCMU/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20

<----------*
100 Trying

<----------
180 Ringing

---------->
PRACK sip:[fromUsername]@[remote_ip]:[remote_port];transport=udp SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[toUsername]@[local_ip]:[local_port]>;tag=[call_number]
To: <sip:[fromUsername]@[remote_ip]:[remote_port]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] PRACK
RAck: [val_last_RSeq:] [val_last_CSeq:]
Max-Forwards: 70
Contact: <sip:[toUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
Content-Length: 0


<----------
200 OK

<----------
200 OK

---------->
ACK sip:[toUsername]@[remote_ip]:[remote_port] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[fromUsername]@[local_ip]:[local_port]>;tag=[call_number]
To: <sip:[toUsername]@[remote_ip]:[remote_port]>;[peer_tag_param]
Call-ID: [call_number]@[local_ip]
CSeq: [cseq-1] ACK
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
Content-Length: 0


----------
cmd_PlayPcap=test.pcap

----------
cmd_Pause=0

---------->
BYE sip:[toUsername]@[remote_ip]:[remote_port] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port];branch=[branch]
From: <sip:[fromUsername]@[local_ip]:[local_port]>;tag=[call_number]
To: <sip:[toUsername]@[remote_ip]:[remote_port]>;[peer_tag_param]
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+2] BYE
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
Content-Length: 0


<----------
200 OK

////////////////////////////////////////////////////////////////////////////////
// Respond to INVITE with 302 REDIRECT to Carol
// 302 Deflection with History-Info header
////////////////////////////////////////////////////////////////////////////////


<----------
INVITE


---------->
SIP/2.0 100 Trying
[last_Via:]
[last_From:]
[last_To:]
[last_Call-ID:]
[last_CSeq:]
User-Agent: SIP Test
Content-Length: 0


---------->
SIP/2.0 302 Moved temporarily
[last_Via:]
[last_From:]
[last_To:];tag=[call_number]
[last_Call-ID:]
[last_CSeq:]
Contact: <sip:[toUsername]@[field0]:5060;transport=[transport]>
User-Agent: SIP Test
History-Info sip[authUsername]@[field0];cause=302;text=unconditional;index=1,sip[toUsername]@[field0];index=1.1
Content-Length: 0


<----------
ACK



----------
label=Done












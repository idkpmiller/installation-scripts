21.3 Redirection 3xx

   3xx responses give information about the user's new location, or
   about alternative services that might be able to satisfy the call.
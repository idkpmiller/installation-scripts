21.4 Request Failure 4xx

   4xx responses are definite failure responses from a particular
   server.  The client SHOULD NOT retry the same request without
   modification (for example, adding appropriate authorization).
   However, the same request to a different server might be successful.
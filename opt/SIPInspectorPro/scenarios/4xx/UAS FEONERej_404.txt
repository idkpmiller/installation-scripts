////////////////////////////////////////////////////////////////////////////////
// Reject Call scenario
// 21.4.5 404 Not Found
//
// The server has definitive information that the user does not exist at
// the domain specified in the Request-URI.  This status is also
// returned if the domain in the Request-URI does not match any of the
// domains handled by the recipient of the request.
// 
//  SIP Messages
//  ============
//
//  <--- OPTIONS*
//  <--- INVITE*
//  ---> 404
//  <--- ACK
//
//  End:
//  Done: 
// Comments are allowed anywhere// in scenario files, and start with "//"
////////////////////////////////////////////////////////////////////////////////



<----------*
OPTIONS


<----------*
INVITE


---------->
SIP/2.0 404 Not Found�
[last_Via:]�
[last_From:]�
[last_To:];tag=[call_number]
[last_Call-ID:]�
[last_CSeq:]�
Contact: <sip:[fromUsername]@[local_ip]:[local_port]>
Content-Length: 0�
�


<----------
ACK


----------
label=End


----------
label=Done

////////////////////////////////////////////////////////////////////////////////
// Reject Call scenario
//
// 
//  SIP Messages
//  ============
//
//  <--- OPTIONS*
//  <--- INVITE*
//  ---> 409
//  <--- ACK
//
//  End:
//  Done: 
// Comments are allowed anywhere// in scenario files, and start with "//"
////////////////////////////////////////////////////////////////////////////////



<----------*
OPTIONS


<----------*
INVITE


---------->
SIP/2.0 409 Conflict�
[last_Via:]�
[last_From:]�
[last_To:];tag=[call_number]
[last_Call-ID:]�
[last_CSeq:]�
Contact: <sip:[fromUsername]@[local_ip]:[local_port]>
Content-Length: 0�
�


<----------
ACK


----------
label=End


----------
label=Done

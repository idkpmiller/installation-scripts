////////////////////////////////////////////////////////////////////////////////
// Call with timed Near End Teardown scenario
// Calling party: 	[fromUsername]
// Called party: 	[toUsername]
//
// SIP Domain: 		[field0] cpg.co.nz
// PilotAOR: 		[authUsername]
//
//  SIP Messages
//  ============
//
//  ---> INVITE
//  <--- 100*
//  <--- 401*
//  ---> ACK*
//  ---> INVITE*
//  <--- 100*
//  <--- 180*
//  <--- 183*
//  <--- 200
//  ---> ACK
//  ---- Play pcap=<test_file>.pcap
//  ---- Pause 10000 ms // CALL DURATION
//  ---> BYE
//  <--- 200
//
//  End:
//
//  Done: 
// NOTE: Change From and To usernames as required. Comments are allowed anywhere// in scenario files, and start with "//"
////////////////////////////////////////////////////////////////////////////////


---------->
INVITE sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133[call_number]
To: <sip:[toUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] INVITE
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
P-Asserted-Identity: <sip:[authUsername]@[field0]> 
Content-Type: application/sdp
Content-Length: [len]

v=0
o=DUT 843670094 1 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 8 101
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20


<----------*
100 Trying


<----------*
401 Unauthorized

---------->*
ACK sip:[toUsername]@[field0] SIP/2.0�
Via: SIP/2.0/[transport] [local_ip]:[local_port]�
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133[call_number]
To: <sip:[toUsername]@[field0]>;[peer_tag_param]�
Call-ID: [call_number]@[local_ip]�
CSeq: [cseq] ACK�
Max-Forwards: 70�
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>�
User-Agent: SIPInspector_v_[ver]�
Content-Length: 0�
�



---------->*
INVITE sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133[call_number]
To: <sip:[toUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] INVITE
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
P-Asserted-Identity: <sip:[authUsername]@[field0]> 
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Type: application/sdp
Content-Length: [len]

v=0
o=DUT 843670094 1 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 8 101
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20


<----------*
100 Trying


<----------*
180 Ringing

// cope with asterisk bug that sends two 180's
<----------*
180 Ringing


<----------*
183 Session in Progress


<----------
200 OK

---------->
ACK sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133[call_number]
To: <sip:[toUsername]@[field0]>;[peer_tag_param]
Call-ID: [call_number]@[local_ip]
CSeq: [cseq] ACK
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
P-Asserted-Identity: <sip:[authUsername]@[field0]> 
Authorization: [authentication username=[authUsername]; password=[authPassword];]
User-Agent: SIPInspector_v_[ver]
Content-Length: 0



----------
cmd_PlayPcap=g711a.pcap



----------
cmd_Pause=10000


---------->
BYE sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133[call_number]
To: <sip:[toUsername]@[field0]>;[peer_tag_param]
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] BYE
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
P-Asserted-Identity: <sip:[authUsername]@[field0]> 
Authorization: [authentication username=[authUsername]; password=[authPassword];]
User-Agent: SIPInspector_v_[ver]
Content-Length: 0


<----------
200 OK

----------
label=End

----------
label=Done

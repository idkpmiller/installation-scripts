Global Failures 6xx

   6xx responses indicate that a server has definitive information about
   a particular user, not just the particular instance indicated in the
   Request-URI.

////////////////////////////////////////////////////////////////////////////////
// Voice Connect Rx Call FEOFET scenario
// Called party: [fromUsername]
//
//
// 
//  SIP Messages
//  ============
//
//  <--- INVITE
//  <--- INVITE
//  <--- INVITE
//  ---> 100
//  ---> 180
//  ---- Pause[ms]=3000
//  ---> 200
//  <--- ACK
//  ---- Play pcap=<test_file..pcap [ms]=60000
//  <--- BYE*
//  ---> 200
//
//  End:
//  Done: 
// NOTE: Change From and To usernames as required.
// Comments are allowed anywhere// in scenario files, and start with "//"
////////////////////////////////////////////////////////////////////////////////


<----------
INVITE

<----------
INVITE


<----------
INVITE


---------->
SIP/2.0 100 Trying�
[last_Via:]�
[last_From:]�
[last_To:]�
[last_Call-ID:]�
[last_CSeq:]�
Content-Length: 0�
�

---------->
SIP/2.0 180 Ringing�
[last_Via:]�
[last_From:]�
[last_To:];tag=[call_number]
[last_Call-ID:]�
[last_CSeq:]�
Contact: <sip:[fromUsername]@[local_ip]:[local_port]>
Content-Length: 0�
�

----------
cmd_Pause=3000

---------->
SIP/2.0 200 OK
[last_Via:]
[last_From:]
[last_To:]
[last_Call-ID:]
[last_CSeq:]
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port]>
Content-Type: application/sdp
Content-Length: [len]

v=0
o=SUT 843670094 1 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 8 101
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20

<----------
ACK


----------
cmd_PlayPcap=g711a.pcap;duration=15000


// Remote Party hangs up early
<----------*
BYE

---------->
SIP/2.0 200 OK�
[last_Via:]�
[last_From:]�
[last_To:]�
[last_Call-ID:]�
[last_CSeq:]�
Contact: <sip:[fromUsername]@[local_ip]:[local_port]>
Content-Length: 0�
�


----------
label=End

----------
label=Done

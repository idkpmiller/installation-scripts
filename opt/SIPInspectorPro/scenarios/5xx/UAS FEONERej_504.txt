////////////////////////////////////////////////////////////////////////////////
// Reject Call scenario
// 21.5.5 504 Server Time-out
//
// The server did not receive a timely response from an external server
// it accessed in attempting to process the request.  408 (Request
// Timeout) should be used instead if there was no response within the
// period specified in the Expires header field from the upstream
// server.
// 
//  SIP Messages
//  ============
//
//  <--- OPTIONS*
//  <--- INVITE*
//  ---> 504
//  <--- ACK
//
//  End:
//  Done: 
// Comments are allowed anywhere// in scenario files, and start with "//"
////////////////////////////////////////////////////////////////////////////////


<----------*
OPTIONS


<----------*
INVITE


---------->
SIP/2.0 504 Server Time-out�
[last_Via:]�
[last_From:]�
[last_To:];tag=[call_number]
[last_Call-ID:]�
[last_CSeq:]�
Retry-After: 2
Contact: <sip:[fromUsername]@[local_ip]:[local_port]>
Content-Length: 0�
�


<----------
ACK


----------
label=End


----------
label=Done

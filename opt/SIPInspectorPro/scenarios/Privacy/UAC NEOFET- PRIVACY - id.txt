////////////////////////////////////////////////////////////////////////////////
// Call with Far End Teardown scenario
// Calling party: [fromUsername]
// Pilot/AOR: [authUsername]
// Called party: [toUsername]
//
// SIP Domain : [field0] cpg.co.nz
// Pilot User: [field1] 44718607
// Registers and check for response. If 401 provide authentication if 200 OK
//     make a call, play PCAP file and terminate the call.
//
//
// Anonymised RFC 3323 USER calling techniques employed:
// UserAgent header removed
// From: Display Name = "Anonymous"
// From: URI = Anonymous@anonymous.invalid
// Call-ID header - removed IP address
// Privacy header request Network for id privacy
//
// 
// Extract from RFC 5379
//    Target headers    where   user     header    session   id   history
//    --------------------------------------------------------------------
//    Call-ID (Note)      R   anonymize    -         -       -      -
//    Call-Info           Rr   delete    not add     -       -      -
//    Contact             R      -      anonymize    -       -      -
//    From                R   anonymize    -         -       -      -
//    History-Info        Rr     -       delete    delete    -    delete
//    In-Reply-To         R    delete      -         -       -      -
//    Organization        Rr   delete    not add     -       -      -
//    P-Asserted-Identity Rr     -       delete      -     delete   -
//    Record-Route        Rr     -      anonymize    -       -      -
//    Referred-By         R   anonymize*   -         -       -      -
//    Reply-To            Rr   delete      -         -       -      -
//    Server              r    delete    not add     -       -      -
//    Subject             R    delete      -         -       -      -
//    User-Agent          R    delete      -         -       -      -
//    Via                 R      -      anonymize    -       -      -
//    Warning             r   anonymize    -         -       -      -
// 
//            Table 1: Recommended PS behavior for each SIP header
//  
// NOTE: Change From and To usernames as required. Comments are allowed anywhere// in scenario files, and start with "//"
////////////////////////////////////////////////////////////////////////////////



---------->
INVITE sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=[$fromTag][call_number]
To: <sip:[toUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] INVITE
Call-Info: <http://wwww.telecom.co.nz/alice/photo.jpg> ;purpose=icon,;<http://www.telecom.co.nz/alice/> ;purpose=info
Organization: Telecom
Subject: Privacy Test
Server: SIP-Inspector
Geolocation: <cid:target123@bullseye.telecom.co.nz>
Warning: 301 telecom.co.nz "Not sure this 'Warning' header will work"
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY 
Max-Forwards: 70
Privacy: id
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
P-Asserted-Identity: <sip:[authUsername]@[field0]> 
Content-Type: application/sdp
Content-Length: [len]

v=0
o=DUT 843670094 1 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 8 101
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20


<----------*
100 Trying


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 500) goto End


---- if (Resp == 503) goto End


<----------*
401 Unauthorized

---------->*
ACK sip:[toUsername]@[field0] SIP/2.0�
Via: SIP/2.0/[transport] [local_ip]:[local_port]�
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=[$fromTag][call_number]
To: <sip:[toUsername]@[field0]>;[peer_tag_param]�
Call-ID: [call_number]@[local_ip]�
CSeq: [cseq] ACK�
Call-Info: <http://wwww.telecom.co.nz/alice/photo.jpg> ;purpose=icon,;<http://www.telecom.co.nz/alice/> ;purpose=info
Organization: Telecom
Subject: Privacy Test
Server: SIP-Inspector
Geolocation: <cid:target123@bullseye.telecom.co.nz>
Warning: 301 telecom.co.nz "Not sure this 'Warning' header will work"
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY 
Max-Forwards: 70�
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>�
User-Agent: SIPInspector_v_[ver]�
Content-Length: 0�
�



---------->*
INVITE sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=[$fromTag][call_number]
To: <sip:[toUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] INVITE
Call-Info: <http://wwww.telecom.co.nz/alice/photo.jpg> ;purpose=icon,;<http://www.telecom.co.nz/alice/> ;purpose=info
Organization: Telecom
Subject: Privacy Test
Server: SIP-Inspector
Geolocation: <cid:target123@bullseye.telecom.co.nz>
Warning: 301 telecom.co.nz "Not sure this 'Warning' header will work"
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY 
Max-Forwards: 70
Privacy: id
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
P-Asserted-Identity: <sip:[authUsername]@[field0]> 
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Type: application/sdp
Content-Length: [len]

v=0
o=DUT 843670094 1 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 8 101
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20


<----------*
100 Trying


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 500) goto End


---- if (Resp == 503) goto End




<----------*
100 Trying


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 500) goto End


---- if (Resp == 503) goto End


<----------*
401 Unauthorized

---------->*
ACK sip:[toUsername]@[field0] SIP/2.0�
Via: SIP/2.0/[transport] [local_ip]:[local_port]�
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=[$fromTag][call_number]
To: <sip:[toUsername]@[field0]>;[peer_tag_param]�
Call-ID: [call_number]@[local_ip]�
CSeq: [cseq] ACK�
Call-Info: <http://wwww.telecom.co.nz/alice/photo.jpg> ;purpose=icon,;<http://www.telecom.co.nz/alice/> ;purpose=info
Organization: Telecom
Subject: Privacy Test
Server: SIP-Inspector
Geolocation: <cid:target123@bullseye.telecom.co.nz>
Warning: 301 telecom.co.nz "Not sure this 'Warning' header will work"
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY 
Max-Forwards: 70�
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>�
User-Agent: SIPInspector_v_[ver]�
Content-Length: 0�
�



---------->*
INVITE sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=[$fromTag][call_number]
To: <sip:[toUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] INVITE
Call-Info: <http://wwww.telecom.co.nz/alice/photo.jpg> ;purpose=icon,;<http://www.telecom.co.nz/alice/> ;purpose=info
Organization: Telecom
Subject: Privacy Test
Server: SIP-Inspector
Geolocation: <cid:target123@bullseye.telecom.co.nz>
Warning: 301 telecom.co.nz "Not sure this 'Warning' header will work"
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY 
Max-Forwards: 70
Privacy: id
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
User-Agent: SIPInspector_v_[ver]
P-Asserted-Identity: <sip:[authUsername]@[field0]> 
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Type: application/sdp
Content-Length: [len]

v=0
o=DUT 843670094 1 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 8 101
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20


<----------*
100 Trying


---- if (Resp == 403) goto End


---- if (Resp == 404) goto End


---- if (Resp == 500) goto End


---- if (Resp == 503) goto End



<----------*
180 Ringing


<----------*
183 Session in Progress


<----------
200 OK

---------->
ACK sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: <sip:[fromUsername]@[field0]:[local_port]>;tag=[$fromTag][call_number]
To: <sip:[toUsername]@[field0]>;[peer_tag_param]
Call-ID: [call_number]@[local_ip]
CSeq: [cseq] ACK
Call-Info: <http://wwww.telecom.co.nz/alice/photo.jpg> ;purpose=icon,;<http://www.telecom.co.nz/alice/> ;purpose=info
Organization: Telecom
Subject: Privacy Test
Server: SIP-Inspector
Geolocation: <cid:target123@bullseye.telecom.co.nz>
Warning: 301 telecom.co.nz "Not sure this 'Warning' header will work"
Allow: INVITE, ACK, CANCEL, OPTIONS, BYE, REFER, NOTIFY 
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
P-Asserted-Identity: <sip:[authUsername]@[field0]> 
Authorization: [authentication username=[authUsername]; password=[authPassword];]
User-Agent: SIPInspector_v_[ver]
Content-Length: 0



----------
cmd_PlayPcap=g711a.pcap


----------
cmd_Pause=60000


// Remote Party hangs up (FET)
<----------
BYE


---------->
SIP/2.0 200 OK�
[last_Via:]�
[last_From:]�
[last_To:]�
[last_Call-ID:]�
[last_CSeq:]�
Contact: <sip:[fromUsername]@[local_ip]:[local_port]>
Content-Length: 0�


----------
label=End

----------
label=Done

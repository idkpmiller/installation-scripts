////////////////////////////////////////////////////////////////////////////////
// A Party Pass-through CLID with Far End Teardown scenario
// Redirecting party: [authUsername]
// A party: [fromUsername]
// Called party: [toUsername]
//
// SIP Domain : [field0] cpg.co.nz
// Registers and check for response. If 401 provide authentication if 200 OK
//     make a call, play PCAP file and terminate the call.
// 
// Diversion header without PAI
//
// NOTE: Change From and To usernames as required. Comments are allowed anywhere// in scenario files, and start with "//"
////////////////////////////////////////////////////////////////////////////////




---------->
INVITE sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: "A Party" <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133
To: <sip:[toUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] INVITE
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
Diversion: <sip:[authUsername]@[field0]>;privacy=off;reason=unconditional;screen=yes
Content-Type: application/sdp
Content-Length: [len]

v=0
o=[fromUsername] 843670094 1 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 8 101
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20


<----------*
100 Trying



<----------*
401 Unauthorized


---------->*
ACK sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: "A Party" <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133
To: <sip:[toUsername]@[field0]>;[peer_tag_param]
Call-ID: [call_number]@[local_ip]
CSeq: [cseq] ACK
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
Content-Length: 0


---------->*
INVITE sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: "A Party" <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133
To: <sip:[toUsername]@[field0]>
Call-ID: [call_number]@[local_ip]
CSeq: [cseq+1] INVITE
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
Diversion: <sip:[authUsername]@[field0]>;privacy=off;reason=unconditional;screen=yes
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Type: application/sdp
Content-Length: [len]

v=0
o=[fromUsername] 843670094 1 IN IP4 [local_ip]
s=-
c=IN IP4 [local_ip]
t=0 0
a=sendrecv
m=audio [media_port] RTP/AVP 8 101
a=rtpmap:8 PCMA/8000
a=rtpmap:101 telephone-event/8000
a=fmtp:101 0-15
a=ptime:20


<----------*
100 Trying


<----------*
180 Ringing


<----------*
180 Ringing


<----------*
183 Session in Progress


<----------
200 OK


---------->
ACK sip:[toUsername]@[field0] SIP/2.0
Via: SIP/2.0/[transport] [local_ip]:[local_port]
From: "A Party" <sip:[fromUsername]@[field0]:[local_port]>;tag=24417923471779367133
To: <sip:[toUsername]@[field0]>;[peer_tag_param]
Call-ID: [call_number]@[local_ip]
CSeq: [cseq] ACK
Max-Forwards: 70
Contact: <sip:[fromUsername]@[local_ip]:[local_port];transport=[transport]>
Authorization: [authentication username=[authUsername]; password=[authPassword];]
Content-Length: 0


----------
cmd_PlayPcap=g711a.pcap;duration=60000


----------
cmd_Pause=60000


// Remote Party hangs up (FET)
<----------
BYE


---------->
SIP/2.0 200 OK�
[last_Via:]�
[last_From:]�
[last_To:]�
[last_Call-ID:]�
[last_CSeq:]�
Contact: <sip:[fromUsername]@[local_ip]:[local_port]>
Content-Length: 0�


----------
label=End

----------
label=Done

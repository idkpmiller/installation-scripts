 *******************************************************************************
 *                                                                             *
 *  LICENSE AGREEMENT:                                                         *
 *                                                                             *
 *  SIP Inspector Pro is not a free utility. To use it, users have to pay      *
 *  a license fee. For more info or problems related with the tool or          *
 *  with licensing please contact:                                             *
 *                                                                             *
 *                                                                             *
 *                      sipinspcustcare@gmail.com                              *
 *                                                                             *
 *  To obtain a license key, please provide Machine-ID with a proof of         *
 *  payment and license key will be sent through email.                        *
 *                                                                             *
 *******************************************************************************

Visit SIP Inspector website for more details: /www.sipinspector.com
Rel 2.40
New Features:
  * It is no more required to program gateway
  * Removed all dependencies on 3rd party JPcap library
  * Fully functional on all operating systems where JAVA runs
  * Progress dialog screen prints unexpected messages received
  * Scenario screen is displayed on the main window title
  * Added video port for streaming video.

Bug fixed:
  * If value file contains: dans-diner.com;041231234 called into a header in a 
    scenario like so [field1]@[field0] I would expect 0412...@dans-diner.com
    Instead it resloves to 041231234@dans - Thanks Paul Miller

Rel 2.20
New Features:
  * Pause is not needed immediately after playing a pcap file. Play pcap
    command now has a duration. Duration is in ms. If duration is not
    set it means forever. Pause after play pcap is only required not if
    subsequent outgoing message has to be delayed from the moment play
    pcap was executed.
  * It is possible to execute multiple play pcap command back to back
  * Added message retransmissions. For each message a retransmission
    count can be set. Retransmission happen according to specs: 0.5, 1,
    2, 4,... sec. Retransmission can be left empty which means the
    message is only sent once.
  * If possible the tool binds to a selected local port. This is to
    avoid unsolicited ICMP messages. If the port is already in use by
    another application this is fine and tool will be able to listen and
    send message as if it successfully bound to the given port.
  * Improved logic for Machine-ID detection. Will work now if for
    example a laptop is detached from its docking station.
  * Added few more scenarios and fixed the old ones.
  * Added pcap files for G.711aLaw and G.729 codecs.

Rel 2.00 
New Features:
  * Multiple dialogs can be combined in a single scenario file
  * Batch scenario processing
  * Scenario progress logging
  * Multiple instaces can be ran and bound to the same IP and port
  * QoS traffic flagging
  * Ability to select and pin down SIP and RTP traffic to particular Network Interface Card (where before JAVA was at liberty to pick its preferred physical card)
  * Windows installer and executable
  * 32-bit and 64-bit executables
  * Still mutiplatform SW solution with the ease and emphasis put on Windows environment
  * Numerous bug fixes

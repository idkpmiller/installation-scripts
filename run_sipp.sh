#!/bin/bash
# runsipp.sh - Run SIPp using dialog box
# -------------------------------------------------
# Author Paul Miller
# ~/sipp/matt/sipp.svn/sipp 10.111.111.245:5060 -l 2 -m 2 -sf a1-diversion-noPAI.xml -inf a1-pp.csv -trace_msg -trace_err -s 077791898

#Default settings START: change to set the default opts to suit the test environemnt

# set the host system to  TX/RX SIP
  RMT_IPv4="192.168.0.23"
  RMT_PORT="5060"
  RMT_SYSTEM=${RMT_IPv4}:${RMT_PORT}

  # set the RURI User part of the addreess typically to a DN by default it will use Service
RURI_USER="service"

# Users should Change nothing below this line
#==================================================
#Default settings END:

SCRIPT_VERSION="0.1"
SCRIPT_NAME=`basename ${0}`
SCRIPT_AUTHOR="Paul Miller"
SCEIPT_PATH=$( cd "$( dirname "$0" )" && pwd)

(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
type dialog >/dev/null 2>&1 || { echo >&2 "I require the dialog package but it's not installed.  Aborting."; exit 1; }

function chk_file_exists
{
   # function for checking if sipp exists
FILE=$1

echo "checking for file $FILE"
   type $FILE >/dev/null 2>&1 || { msg "I require $FILE but it's not installed.  Aborting." 0 0; exit 1; }
}

function msg
{
   # function for making a messagebox
   # if it has less than two arguments
   if [[ $# < 2 ]]
   then
      # use auto-size for the messagebox
      dialog --msgbox "$1" 0 0
   else
      # use specified height and width
      dialog --msgbox "$1" $2 $3
   fi
   clear
}

function get_rmtsystem
{
# chk if file exists /tmp/RMT_SYSecho "1 - $RMT_SYSTEM"TEM
TMPRMT="/tmp/RMT_SYSTEM.val"
if [ -f /tmp/RMT_SYSTEM.val ]
then
    RMT_SYSTEM=`cat $TMPRMT`
else
    echo $RMT_SYSTEM>$TMPRMT 
fi

RMT_SYSTEM=$(whiptail --inputbox "Remote SIP system IP and Port" 8 80 $RMT_SYSTEM --title "Remote SIP server" 3>&1 1>&2 2>&3)
echo $RMT_SYSTEM>$TMPRMT
}

function get_scnfile
{
DIALOG=${DIALOG=dialog}

SCNFILE=`$DIALOG --stdout --title "Please choose an xml scenario file to load" --fselect $SCEIPT_PATH/ 14 48`

case $? in
        0)
                echo "\"$SCNFILE\" chosen";;
        1)
                echo "Cancel pressed.";;
        255)
                echo "Box closed.";;
esac
}

function get_datafile
{
DIALOG=${DIALOG=dialog}

DATAFILE=`$DIALOG --stdout --title "Please choose a data file .csv" --fselect $SCEIPT_PATH/ 14 48`

case $? in
        0)
                echo "\"$DATAFILE\" chosen";;
        1)
                echo "Cancel pressed.";;
        255)
                echo "Box closed.";;
esac
}


function set_ruri_user
{
# chk if file exists /tmp/RURI_USER
TMPRUSER="/tmp/RURI_USER.val"
if [ -f /tmp/RURI_USER.val ]
then
    RURI_USER=`cat $TMPRUSER`
else
    echo $RURI_USER>$TMPRUSER 
fi

RURI_USER=$(whiptail --inputbox "Remote URI User part" 8 80 $RURI_USER --title "Remote URI User address - DN" 3>&1 1>&2 2>&3)
echo $RURI_USER>$TMPRUSER
}



-s               : Set the username part of the resquest URI. Default is
                      'service'.


#====================================
# Start the Wizard


   #Step one, simple welcome
msg "This application will run a SIPp instance and offers a simple to use interface." 0 0

   #checking sipp exists#
chk_file_exists sipp

   # get the remote system address and port
get_rmtsystem

   # set the target RURI user part of the URI default = service
set_ruri_user


   # step X get the scenario file
get_scnfile

   # step X get the data file
get_datafile

# apply late over-rides
SCNFILE="/root/sipp/Asterisk/Registration/register.xml"
DATAFILE="/root/sipp/Asterisk/Registration/users.csv"

## ~/sipp/matt/sipp.svn/sipp 10.111.111.245:5060 -l 2 -m 2 -sf a1-diversion-noPAI.xml -inf a1-pp.csv -trace_msg -trace_err -s 077791898
SIPPCMD="sipp $RMT_SYSTEM -l 2 -m 2 -sf $SCNFILE -inf $DATAFILE -trace_msg -trace_err -s $RURI_USER"
echo $SIPPCMD
logger "$SIPPCMD" -t DEBUG
$SIPPCMD
  

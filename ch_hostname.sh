#!/usr/bin/env bash
##############################
# Version 0.1 28/10/14
#
# by Paul Miller
##############################

VERSION=0.1
OLD_HOSTNAME="$( hostname -s )"
HOSTNAMEDOMAIN="$( hostname -d )"
HOSTNAMEFQDN="$( hostname )"
NEW_HOSTNAME="$1"

# establish text format options
bold=`tput bold`
normal=`tput sgr0`

#---------------------
# Functions
#---------------------
function pTitle () {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as arguament to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot() {
# Supports: text, whiptail, dialog
# Requires:
# DIALOG="$(which whiptail dialog)" # variable
##############################################

logger "$0 Entered OS ENVIRONMENT CHECKS" 

if [[ $DIALOG ]]; then
	(( `id -u` )) && $DIALOG --msgbox --title "Checking for root Priviledges:" "Must be ran as root, try prefixing with sudo." 8 44 && exit 1
else
	pTitle "Checking for root Priviledges:"
	(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
fi
#func_chkRoot
}

function confirm () {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case $response in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

function inputHostname () {
if [ -z "$NEW_HOSTNAME" ]; then
 echo -n "Enter new host portion of the hostname that will replace $OLD_HOSTNAME: "
 read NEW_HOSTNAME < /dev/tty
fi

if [ -z "$NEW_HOSTNAME" ]; then
 echo "Error: no hostname entered. Exiting."
 exit 1
fi

# Check 1 - host has less than 63 characters
	NEW_HOSTNAMELEN=$(echo ${#NEW_HOSTNAME})
	if [ $NEW_HOSTNAMELEN -gt 63 ]; then
		echo "$NEW_HOSTNAME has failed the length check - host portion cannot be longer than 63 characters."
		exit 1
	fi

# Check 2 - host and domain has less than 255 characters

	HOSTNAMEDOMAINLEN=$(echo ${#HOSTNAMEDOMAIN})
	# allow one character for missing period delimiter
	FQDNLEN=$((NEW_HOSTNAMELEN + HOSTNAMEDOMAINLEN + 1))
	if [ $FQDNLEN -gt 255 ]; then
		echo "$NEW_HOSTNAME has failed the length check - FQDN cannot be longer than 255 characters."
		exit 1
	fi

# Check 3 - character set is permitted
	if [[ $NEW_HOSTNAME =~ [^A-Z0-9a-z\-] ]]; then
		echo "Host provided failed check, only alphanumeric and hyphen are permitted characters"
		exit 1
	fi
	
# Check 4 - host does not start with a hyphen character
	HOSTNAMESTART=${NEW_HOSTNAME:0:1}
	if [ $HOSTNAMESTART == "-" ]; then
		echo "Host name cannot start with a hyphen"
		exit 1
	fi	


# Check 5 - host does not end with a hyphen character
	HOSTNAMEEND=${NEW_HOSTNAME:$((NEW_HOSTNAMELEN - 1)):$NEW_HOSTNAMELEN}
	if [ $HOSTNAMEEND == "-" ]; then
		echo "Host name cannot end with a hyphen"
		exit 1
	fi	

# End getHostname
}

function changeHostname () {
echo "Changing hostname from $OLD_HOSTNAME to $NEW_HOSTNAME..."
hostname "${NEW_HOSTNAME}.${HOSTNAMEDOMAIN}"
sed -i "s/HOSTNAME=.*/HOSTNAME=$NEW_HOSTNAME/g" /etc/sysconfig/network
 
# End changeHostname
}

function changeHosts () {
if [ -n "$( grep "$OLD_HOSTNAME" /etc/hosts )" ]; then
 sed -i "s/$OLD_HOSTNAME/$NEW_HOSTNAME/g" /etc/hosts
else
 echo -e "$( hostname -I | awk '{ print $1 }' )\t$NEW_HOSTNAME" >> /etc/hosts
 fi
# End changeHosts
}

# check for root privileges
func_chkRoot

# Provide information about the script to the user
echo ""
echo ""
pTitle "Script Objective"
echo "This  script will change the host portion on the hostname."
echo "It will not change the domain portion"
echo ""
echo "	Host:    $OLD_HOSTNAME" 
echo "	Domain:  $HOSTNAMEDOMAIN"
echo "	FQDN:    $HOSTNAMEFQDN"
echo ""
# check user wishes to continue
    read -r -p "Continue with script? [y/N]} " response
    case $response in
        [yY][eE][sS]|[yY]) 
            true;
			echo "";
            ;;
        *)
            echo "Script terminated at user request";
			exit 0
            ;;
    esac

inputHostname
changeHostname
changeHosts
POSTHOSTNAME="$( hostname )"
echo ""
pTitle "Hostname is now set as ${POSTHOSTNAME}"
echo "logout and log back in and the prompt should be updated to reflect the hostname change."
echo ""
echo "Script completed normally - Done."


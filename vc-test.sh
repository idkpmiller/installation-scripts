#!/bin/bash
##############################
# by Paul Miller
##############################
# DESCRIPTION:
# Script to provide overall 
# menu control to run 
# PTC simulated tests of Voice
# Connect
##############################


##############################
# VARABLES:
# section sets defaults & 
# script variables needed
##############################


DIR=$(cd $(dirname $0) && pwd)
SRC_IP="122.56.253.104"

##############################
# END OF VARIABLES
##############################


##############################
# FUNCTIONS:
# Only functions should exist
# in this area 
##############################

function _rmdir () {
if [ -d $1 ];then
  /bin/rm -r $1
fi
}

function _createdirs () {
  /bin/mkdir $DIR/results
  cd $DIR/results
  
  # create all test directories
  /bin/mkdir test1
  /bin/mkdir test2
  /bin/mkdir test3.1
  /bin/mkdir test3.2
  /bin/mkdir test4
  /bin/mkdir test5
  /bin/mkdir test7
}


do_finish () {
  echo "Normal Close - User exited."
  rm -f /tmp/*$$
  exit
}

do_Test1 () {
# Registration - Incorrect Password
  cd $DIR/scripts
  ./ctrl-chkhost.sh
}

do_Test2 () {
# setup result output DIR
 t2DIR=$DIR/results/test2

# Registration - Successful
 cd $DIR/results/test2
 CMD=""
 CMD="sipp -aa -sf /opt/ptc/TEST2_UAS-REG.xml -i $SRC_IP -m 1 -l 1 -trace_err -error_file $t2DIR/Test2-error.log -trace_msg -message_file $t2DIR/Test2-msg.log -trace_logs -log_file $t2DIR/Test2.log"
 echo "CMD: $CMD"
 $CMD
}

do_Test3.1 () {
# setup result output DIR
 t3aDIR=$DIR/results/test3.1

# PBX Originate Call - from Pilot DID
 cd $t3aDIR
 CMD=""
 CMD="sipp -aa -sf /opt/ptc/TEST3.1_UAS-FEONET.xml -i $SRC_IP -m 1 -l 1 -trace_err -error_file $t3aDIR/Test3.1-error.log -trace_msg -message_file $t3aDIR/Test3.1-msg.log -trace_logs -log_file $t3aDIR/Test3.1.log -watchdog_minor_threshold 2000 -rtp_echo"
 echo "CMD: $CMD"
 $CMD
}

do_Test3.2 () {
# PBX Terminate Call - to Pilot DID

  cd $DIR/scripts
 ./ctrl-sipp.sh
}

do_Test4 () {
# PBX Originate Call - from Child DID

  cd $DIR/scripts
 ./ctrl-sipp.sh
}

do_Test5 () {
# PBX Terminate Call - to Child DID

  cd $DIR/scripts
 ./ctrl-sipp.sh
}

do_Test7 () {
# PBX Originate Call - from extention non-DID (Unscreened Call)
# setup result output DIR
 t7DIR=$DIR/results/test7

 cd $t7DIR
 CMD=""
 CMD="sipp -aa -sf /opt/ptc/TEST7_UAS-FEONET.xml -i $SRC_IP -m 1 -l 1 -trace_err -error_file $t7DIR/Test7-error.log -trace_msg -message_file $t7DIR/Test7-msg.log -trace_logs -log_file $t7DIR/Test7.log -watchdog_minor_threshold 2000 -rtp_echo"
 echo "CMD: $CMD"
 $CMD
}

do_Reset () {
 # The next line looks for any existing log files n a specified directory and deletes them
 find /opt/ptc -iname \*.log -delete
 # The next line looks for any existing log files n a specified directory and deletes them
 find $DIR/results -iname \*.log -delete
 # make the result directory structure
  _rmdir $DIR/results
  _createdirs 
}

do_Results () {
 NOW=$(date +"%Y%m%d")

 # The next line looks for any existing log files n a specified directory and archives them
 find $DIR/results -iname \*.log -exec /usr/bin/zip -r $DIR/results_$NOW {} $DIR/results \;
 
}


##############################
# END OF FUNCTIONS
##############################


##############################
# CODE:
# This area starts the script
# and joins functions 
# together to run the script.
##############################

cd $DIR

#tempfile3=/tmp/dialog_3_$$
##trap "rm -f $tempfile3" 0 1 2 5 15

# set traps so process kills will clean up
trap do_finish 0 1 2 5 15

while true; do
  FUN=$(whiptail --menu "The Voice Connect PTC Simulatior" 20 80 12 --cancel-button Quit --ok-button Select \
    "Test1" "Registration - Incorrect Password" \
    "Test2" "Registration - Successful" \
    "Test3.1" "PBX Originate Call - from Pilot DID" \
    "Test3.2" "PBX Terminate Call - to Pilot DID" \
    "Test4" "PBX Originate Call - from Child DID" \
    "Test5" "PBX Terminate Call - to Child DID" \
    "Test7" "PBX Originate Call - from extention non-DID (Unscreened Call)" \
    "Reset" "Delete ALL existing log files from previous test runs" \
    "Results" "archive ALL results to results.zip" \
    3>&1 1>&2 2>&3)
  RET=$?
  if [ $RET -eq 1 ]; then
    temp=
    do_finish
  elif [ $RET -eq 0 ]; then
    "do_$FUN" || whiptail --msgbox "There was an error running do_$FUN" 20 60 1
  else
    exit 1
  fi
done


#!/bin/bash
#
# Raspberry Pi install RTL8188CUS nano wifi USB adaptor
# by Paul Miller
# Revisions:
# 0.1 - 25/04/2013 Initial Release
# 0.2 - 03/06/2013 correct typos
# 0.3 - 01/01/2015 restructure script into functions
# 0.4 - 10/01/2015 changed from istaller to a manager
# 0.5 - 25/05/2016 Corrected from feedback (Thanks Dongkwon Kim.) the ap_country parameter
###################################################


###################################################
# Set default values
###################################################
# General
CURRENT_AUTHOR="idkpmiller@sip2serve.com"
# establish text format options
bold=`tput bold`
normal=`tput sgr0`

BOOTUP=color
RES_COL=49
MOVE_TO_COL="echo -en \\033[${RES_COL}G"
SETCOLOR_SUCCESS="echo -en \\033[1;32m"
SETCOLOR_FAILURE="echo -en \\033[1;31m"
SETCOLOR_WARNING="echo -en \\033[1;33m"
SETCOLOR_NORMAL="echo -en \\033[0;39m"


# Network Interface
IP4_CONF_TYPE=DHCP
IP4_ADDRESS=192.168.0.150
IP4_NETMASK=255.255.255.0
IP4_GATEWAY=192.168.0.2
IP4_DNS1=8.8.8.8
IP4_DNS2=8.8.4.4

# Wifi Access Point
AP_COUNTRY=NZ
AP_CHAN=1
AP_SSID=MyAP
AP_PASSPHRASE=PASSWORD

# Wifi Client
ETHERNET_BR=true
CLIENT_SSID=MyAP
CLIENT_PASSPHRASE=PASSWORD

###################################################
echo " performing a series of pre-checks..."
###################################################

###################################################
# Functions
###################################################

chk_root () {
#check current user privileges, exit if not sudo or root.
  (( `id -u` )) && echo "This script MUST be ran with root privileges, try prefixing with sudo. i.e sudo $0" && exit 1
}

chk_device () {
#check  that USB wifi device is plugged in and seen
if [[ -n $(lsusb | grep RTL8188CUS) ]]; then
    echo "The RTL8188CUS device has been successfully located."
else
    echo "The RTL8188CUS device has not been located, check it is inserted and run script again when done."
    exit 1
fi
}

chk_internet () {
#check that internet connection is available, sets the INTERNET flag

# the hosts below are selected for their high availability,
# if it is more apprioriate to change a host to equal one that is
# required for the script then simply change the FQDN chosen below 
# to check the availabilty for the host before the script gets underway

echo "Checking Internet connectivity; this can take over a minute, if unavailable"

host1=google.com
host2=wikipedia.org

if ping -w5 -c3 $host1 > /dev/null 2>&1 ; then
  INTERNET=true
  echo "Internet connectivity - OK"
elif ping -w5 -c3 $host2 > /dev/null 2>&1 ; then
  INTERNET=true
  echo "Internet connectivity - OK"
else
  INTERNET=false
  printf "Internet connectivity - Down.\n\n\r"
fi
}

echo_success() {
  [ "$BOOTUP" = "color" ] && $MOVE_TO_COL
  echo -n "["
  [ "$BOOTUP" = "color" ] && $SETCOLOR_SUCCESS
  echo -n $"  OK  "
  [ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 0
}

echo_done() {
  [ "$BOOTUP" = "color" ] && $MOVE_TO_COL
  echo -n "["
  [ "$BOOTUP" = "color" ] && $SETCOLOR_SUCCESS
  echo -n $" DONE "
  [ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 0
}

echo_failure() {
  [ "$BOOTUP" = "color" ] && $MOVE_TO_COL
  echo -n "["
  [ "$BOOTUP" = "color" ] && $SETCOLOR_FAILURE
  echo -n $"FAILED"
  [ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 1
}

backup_interface () {
# backup the existing interfaces file
echo -n "Backup network interface configuration"
cp /etc/network/interfaces /etc/network/interfaces.bak
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
  exit $rc
else
  echo_done
fi
}

restart_network () {
# gets positive confirmation from user to restart network
echo "
${bold} Network Interface Restart Required. ${normal}
The changes require that the network interface is
restarted this can be done now or when you next reboot.

Warning! If you are using the network to configure your RPi
         you may lose connection.

"
read -p "Do you want to restart the network now? Press Y to confirm" -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo ""
    echo "Any configuration changes will take affecton next reboot!"
else
    service networking restart
    echo "Configuration changes complete."
fi
}

reload_network () {
echo "###################INSTALL COMPLETE###############"######
echo "The services will now be reloaded to activate the changes"
read -p "Press [Enter] key to reload services..."

# Reload the networking configuration to acctivate the changes
/etc/init.d/networking reload

}

get_SSID () {
grep -v '^#' "/etc/network/interfaces" | grep 'wpa-ssid' | awk {'print $2'} >/dev/null
rc=$?
if [[ $rc == 0 ]] ; then
  CLIENT_SSID=$(grep -v '^#' "/etc/network/interfaces" | grep 'wpa-ssid' | awk {'print $2'})
fi
}

get_PSK () {
grep -v '^#' "/etc/network/interfaces" | grep 'wpa-psk' | awk {'print $2'} >/dev/null
rc=$?
if [[ $rc == 0 ]] ; then
  CLIENT_PASSPHRASE=$(grep -v '^#' "/etc/network/interfaces" | grep 'wpa-psk' | awk {'print $2'})
fi
}

get_AP_SSID () {
grep -v '^#' "/etc/hostapd/hostapd.conf" | grep 'ssid=' |  awk -F '=' '{print $2}' >/dev/null
rc=$?
if [[ $rc == 0 ]] ; then
  AP_SSID=$(grep -v '^#' "/etc/hostapd/hostapd.conf" | grep 'ssid=' |  awk -F '=' '{print $2}')
fi
}

get_AP_CHAN () {
grep -v '^#' "/etc/hostapd/hostapd.conf" | grep 'channel=' |  awk -F '=' '{print $2}' >/dev/null
rc=$?
if [[ $rc == 0 ]] ; then
  AP_CHAN=$(grep -v '^#' "/etc/hostapd/hostapd.conf" | grep 'channel=' |  awk -F '=' '{print $2}')
fi
}

get_AP_COUNTRY () {
grep -v '^#' "/etc/hostapd/hostapd.conf" | grep 'country_code=' |  awk -F '=' '{print $2}' >/dev/null
rc=$?
if [[ $rc == 0 ]] ; then
  AP_COUNTRY=$(grep -v '^#' "/etc/hostapd/hostapd.conf" | grep 'country_code=' |  awk -F '=' '{print $2}')
fi
}

get_input () {
#clear the screen
clear

# Get Input from User
echo "Capture User Settings:"
echo "====================="
echo "Please answer the following questions."
echo "Hitting [Enter] will continue with the option in the square brackets"
echo
echo
}

get_tcpip () {
IP4_ADDRESS=$(ip addr | awk '/inet/ && /br0/{sub(/\/.*$/,"",$2); print $2}')
read -p "IPv4 Address [$IP4_ADDRESS]: " -e t1
if [ -n "$t1" ]; then IP4_ADDRESS="$t1";fi

IP4_NETMASK=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f4 | awk '{ print $1}')
read -p "IPv4 Netmask [$IP4_NETMASK]: " -e t1
if [ -n "$t1" ]; then IP4_NETMASK="$t1";fi

IP4_GATEWAY=$(ip route show 0.0.0.0/0 dev br0 | cut -d\  -f3)
read -p "IPv4 Gateway Address [$IP4_GATEWAY]: " -e t1
if [ -n "$t1" ]; then IP4_GATEWAY="$t1";fi

IP4_DNS1=$(grep -v '^#' "/etc/resolv.conf" | awk '/nameserver/{i++}i==1' /etc/resolv.conf | awk '{ print $2}')
read -p "IPv4 Primary DNS server [$IP4_DNS1]: " -e t1
if [ -n "$t1" ]; then IP4_DNS1="$t1";fi

IP4_DNS2=$(grep -v '^#' "/etc/resolv.conf" | awk '/nameserver/{i++}i==2' /etc/resolv.conf | awk '{ print $2}')
read -p "IPv4 Secondary DNS server [$IP4_DNS2]: " -e t1
if [ -n "$t1" ]; then IP4_DNS2="$t1";fi
}

function get_wifi_ap () {
# wifi settings
get_AP_COUNTRY
read -p "Wifi Country [$AP_COUNTRY]: " -e t1
if [ -n "$t1" ]; then AP_COUNTRY="$t1";fi

get_AP_CHAN
read -p "Wifi Channel No [$AP_CHAN]: " -e t1
if [ -n "$t1" ]; then AP_CHAN="$t1";fi

get_AP_SSID
read -p "Wifi SSID [$AP_SSID]: " -e t1
if [ -n "$t1" ]; then AP_SSID="$t1";fi

read -s -p "Wifi PassPhrase (min 8 max 63 characters) [$AP_PASSPHRASE]: " -e t1
if [ -n "$t1" ]; then AP_PASSPHRASE="$t1";fi
echo ""

}

function get_wifi_client () {
# wifi client settings
get_SSID
read -p "Wifi SSID [$CLIENT_SSID]: " -e t1
if [ -n "$t1" ]; then CLIENT_SSID="$t1";fi

get_PSK 
read -s -p "Wifi PassPhrase (min 8 max 63 characters) [$CLIENT_PASSPHRASE]: " -e t1
if [ -n "$t1" ]; then CLIENT_PASSPHRASE="$t1";fi
echo ""
}

function cfg_dhcp_ip () {

# USE Cases:
# 1 - Wifi AP already configured, change from current IP to DHCP 
# 2 - Wifi Client already configured, change from current IP to DHCP

backup_interface
if grep -v '^#' "/etc/network/interfaces.bak" | grep 'wpa-ssid' >/dev/null ; then
  # it's a wifi client setup
  # get the wpa-ssid
  get_SSID
  
  # get the wpa-psk
  get_PSK
  
  # create the following network interface file based on backup file
  echo -n "Create DHCP wifi client network interface configuration"
  cat <<EOF > /etc/network/interfaces
#created by $0
auto lo
auto br0
        iface lo inet loopback
        iface br0 inet dhcp
        bridge_fd 1
        bridge_hello 3
        bridge_maxage 10
        bridge_stp off
        bridge_ports eth0 wlan0
allow-hotplug eth0
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet manual
iface wlan0 inet manual
   wpa-ssid "${CLIENT_SSID}"
   wpa-psk "${CLIENT_PASSPHRASE}"
EOF
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    exit $rc
  else
    echo_done
	restart_network
  fi

else

  # it's a wifi AP
  echo -n "Create DHCP wifi AP network interface configuration"
 
  cat <<EOF > /etc/network/interfaces
#created by $0
auto lo
auto br0
        iface lo inet loopback
        iface br0 inet dhcp
        bridge_fd 1
        bridge_hello 3
        bridge_maxage 10
        bridge_stp off
        bridge_ports eth0 wlan0
allow-hotplug eth0
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet manual
EOF
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    exit $rc
  else
    echo_done
	restart_network
  fi
fi
}

function cfg_static_ip () {
# USE Cases:
# 1 - Wifi AP already configured, change from current IP to STATIC 
# 2 - Wifi Client already configured, change from current IP to STATIC

# get the IP settings from the user
get_input
get_tcpip


echo -n "Create new DNS resolv.conf configuration"
 
cat <<EOF > /etc/resolv.conf
#created by $0
nameserver $IP4_DNS1
nameserver $IP4_DNS2
EOF
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
else
  echo_done
fi


backup_interface
if grep -v '^#' "/etc/network/interfaces" | grep 'wpa-ssid' >/dev/null ; then
#it's a wifi client

  # get the wpa-ssid
  get_SSID
  
  # get the wpa-psk
  get_PSK
  
  echo -n "Create static wifi client network interface configuration"
 
  cat <<EOF > /etc/network/interfaces
#created by $0
auto lo
auto br0
        iface lo inet loopback
        iface br0 inet static
        address $IP4_ADDRESS
        netmask $IP4_NETMASK
        gateway $IP4_GATEWAY
        dns-nameservers $IP4_DNS1 $IP4_DNS2
        bridge_fd 1
        bridge_hello 3
        bridge_maxage 10
        bridge_stp off
        bridge_ports eth0 wlan0
allow-hotplug eth0
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet manual
   wpa-ssid ${CLIENT_SSID}
   wpa-psk ${CLIENT_PASSPHRASE}
EOF
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    exit $rc
  else
    echo_done
	restart_network
  fi

else
# it;s a wifi AP
  
  echo -n "Create static wifi AP network interface configuration"
 
  cat <<EOF > /etc/network/interfaces
#created by $0
auto lo
auto br0
        iface lo inet loopback
        iface br0 inet static
        address $IP4_ADDRESS
        netmask $IP4_NETMASK
        gateway $IP4_GATEWAY
        dns-nameservers $IP4_DNS1 $IP4_DNS2
        bridge_fd 1
        bridge_hello 3
        bridge_maxage 10
        bridge_stp off
        bridge_ports eth0 wlan0
allow-hotplug eth0
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet manual
EOF
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    exit $rc
  else
    echo_done
	restart_network
  fi
fi
}

function cfg_wifi_ap () {
echo "Configuring device as an Access Point...."
echo ""
echo "#####################PLEASE WAIT##################"######

echo "Configuring device as a Wifi client...."

get_input
get_wifi_ap

echo "--------------------"
echo ""

if [ "$INTERNET" = true ] ; then 
  echo -n "Package list update"
  apt-get -qq update 
  echo_done
else
  echo "Unable to update system, no internet connectivity"
fi
  
for PACKAGE in hostapd bridge-utils iw
do
  if [ $(dpkg-query -W -f='${Status}' $PACKAGE 2>/dev/null | grep -c "ok installed") -eq 0 ];
  then
    if [ INTERNET = true ]; then
      echo -n "Adding package $PACKAGE "
      apt-get -y -qq install $PACKAGE > /dev/null 2>&1;
      echo_done
    else
      echo "Unable to add required package, no internet connectivity"
	  exit 1
    fi
  else
    echo -n "Package $PACKAGE already installed"
    echo_done  
  fi
done

#check  that iw list fails with 'nl80211 not found'
echo -n "iw list check"
iw list > /dev/null 2>&1 | grep 'nl80211 not found'
rc=$?
if [[ $rc = 0 ]] ; then
  echo_failure
  echo ""
  echo "A check that should have a known outcome has has not"
  echo "produced the expected result, it is best to manually"
  echo "proceed and ask for assistance from the RPi forum or"
  echo "the authour of this version of the script."
  echo $CURRENT_AUTHOR
  exit $rc
else
  echo_done
fi

#create the default file to point at the configuration file
echo -n "Create Default hostapd file"
cat <<EOF > /etc/default/hostapd
#created by $0
DAEMON_CONF="/etc/hostapd/hostapd.conf"
EOF
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
  echo ""
  exit $rc
else
  echo_done
fi

#create the hostapd configuration to match what the user has provided
echo -n "Create hostapd.conf file"
cat <<EOF > /etc/hostapd/hostapd.conf
#created by $0
interface=wlan0
bridge=br0
driver=rtl871xdrv
country_code=$AP_COUNTRY
ctrl_interface=wlan0
ctrl_interface_group=0
ssid=$AP_SSID
hw_mode=g
channel=$AP_CHAN
wpa=3
wpa_passphrase=$AP_PASSPHRASE
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP
beacon_int=100
auth_algs=3
macaddr_acl=0
wmm_enabled=1
eap_reauth_period=360000000
EOF
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
  echo ""
  exit $rc
else
  echo_done
fi

if [ $INTERNET = true ]; then
  #deal with the hostapd binary file
  echo -n "change directory"
  cd /usr/sbin
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
  fi

  # backup the old hostapd file
  if [ -f /usr/sbin/hostapd ] ; then
    echo -n "Backup hostapd file"
    cp /usr/sbin/hostapd /usr/sbin/hostapd.bak
    rc=$?
    if [[ $rc != 0 ]] ; then
      echo_failure
      echo ""
      exit $rc
    else
     echo_done
    fi
	# delete the old hostapd file
    echo -n "Delete old hostapd file"
    rm -f hostapd
    rc=$?
    if [[ $rc != 0 ]] ; then
      echo_failure
      echo ""
      exit $rc
    else
      echo_done
    fi
  fi

  # Download the replacement file
  echo -n "Download the hostapd file"
  wget http://dl.dropbox.com/u/1663660/hostapd/hostapd > /dev/null 2>&1
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
  fi

  # make sure root has ownership of the file
  echo -n "Modify hostapd ownership"
  chown root:root hostapd
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
  fi

  # make the new file executable
  echo -n "Modify the hostapd file permissions"
  chmod 755 hostapd
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
  fi
fi

backup_interface

InetModeBR0=$(grep 'iface\ br0' "/etc/network/interfaces" | awk -F ' ' '{print $4}');
if [ $InetModeBR0 == "dhcp" ] ; then
  echo -en "Create new Wifi AP network interface configuration"

  cat <<EOF > /etc/network/interfaces
#created by $0
auto lo
auto br0
iface lo inet loopback
iface br0 inet dhcp
        bridge_fd 1
        bridge_hello 3
        bridge_maxage 10
        bridge_stp off
        bridge_ports eth0 wlan0
allow-hotplug eth0
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet manual
EOF
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
	reload_network
  fi
fi

if [ $InetModeBR0 == "static" ] ; then

  # create the following network interface file based on user input
  echo -en "Create new network interface configuration"

  IP4_ADDRESS=$(ip addr | awk '/inet/ && /br0/{sub(/\/.*$/,"",$2); print $2}')
  IP4_NETMASK=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f4 | awk '{ print $1}')
  IP4_GATEWAY=$(ip route show 0.0.0.0/0 dev br0 | cut -d\  -f3)
  IP4_DNS1=$(grep -v '^#' "/etc/network/interfaces" | awk '/nameserver/{i++}i==1' /etc/resolv.conf | awk '{ print $2}')
  IP4_DNS2=$(grep -v '^#' "/etc/network/interfaces" | awk '/nameserver/{i++}i==2' /etc/resolv.conf | awk '{ print $2}')
  
  cat <<EOF > /etc/network/interfaces
#created by $0
auto lo
auto br0
iface lo inet loopback
iface br0 inet static
        address $IP4_ADDRESS
        netmask $IP4_NETMASK
        gateway $IP4_GATEWAY
        dns-nameservers $IP4_DNS1 $IP4_DNS2
        bridge_fd 1
        bridge_hello 3
        bridge_maxage 10
        bridge_stp off
        bridge_ports eth0 wlan0
allow-hotplug eth0
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet manual
EOF
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
	reload_network
  fi
fi 

update-rc.d hostapd enable
# Restart the access point software
/etc/init.d/hostapd restart

####################################################################

}

function cfg_wifi_client () {
echo "Configuring device as a Wifi client...."

get_input
get_wifi_client

# stop hostapd if exists
echo ""
echo -n "Stop hostapd service"
service hostapd stop >/dev/null
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
  echo ""
  exit $rc
else
  echo_done
fi
  
#remove the default file to point at the configuration file if it exists
if [[ -f /etc/default/hostapd ]]; then
  echo -en "Removing Default hostapd file"
  rm -f /etc/default/hostapd >/dev/null
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
  fi
fi

backup_interface

InetModeBR0=$(grep iface\ br0 "/etc/network/interfaces" | awk -F ' ' '{print $4}')
if [ $InetModeBR0 == "dhcp" ] ; then

  # create the following network interface file based on user input
  echo -en "Create new network interface configuration"

  cat <<EOF > /etc/network/interfaces
#created by $0
auto lo
auto br0
iface lo inet loopback
iface br0 inet dhcp
        bridge_fd 1
        bridge_hello 3
        bridge_maxage 10
        bridge_stp off
        bridge_ports eth0 wlan0
allow-hotplug eth0
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet manual
   wpa-ssid ${CLIENT_SSID}
   wpa-psk ${CLIENT_PASSPHRASE}
EOF
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
	reload_network
  fi
fi

if [ $InetModeBR0 == "static" ] ; then

  # create the following network interface file based on user input
  echo -en "Create new network interface configuration"

  IP4_ADDRESS=$(ip addr | awk '/inet/ && /br0/{sub(/\/.*$/,"",$2); print $2}')
  IP4_NETMASK=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f4 | awk '{ print $1}')
  IP4_GATEWAY=$(ip route show 0.0.0.0/0 dev br0 | cut -d\  -f3)
  IP4_DNS1=$(grep -v '^#' "/etc/network/interfaces" | awk '/nameserver/{i++}i==1' /etc/resolv.conf | awk '{ print $2}')
  IP4_DNS2=$(grep -v '^#' "/etc/network/interfaces" | awk '/nameserver/{i++}i==2' /etc/resolv.conf | awk '{ print $2}')
  
  cat <<EOF > /etc/network/interfaces
#created by $0
auto lo
auto br0
iface lo inet loopback
iface br0 inet static
        address $IP4_ADDRESS
        netmask $IP4_NETMASK
        gateway $IP4_GATEWAY
        dns-nameservers $IP4_DNS1 $IP4_DNS2
        bridge_fd 1
        bridge_hello 3
        bridge_maxage 10
        bridge_stp off
        bridge_ports eth0 wlan0
allow-hotplug eth0
iface eth0 inet manual
allow-hotplug wlan0
iface wlan0 inet manual
   wpa-ssid ${CLIENT_SSID}
   wpa-psk ${CLIENT_PASSPHRASE}
EOF
  rc=$?
  if [[ $rc != 0 ]] ; then
    echo_failure
    echo ""
    exit $rc
  else
    echo_done
	reload_network
  fi
fi 
}

function go_AP () {
chk_device
echo ${bold}"Access Point"
echo "======================"${normal}
echo "Please answer the following question."
echo "Hitting [Enter] will continue with the default 'No' option"
echo

# Point of no return
read -p "Press Y to continue with Setting up your device as an Access Point." -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo ""
    echo "Goodbye!"
    exit 1
fi
cfg_wifi_ap
}

function go_Client () {
chk_device
echo ${bold}"Wifi Client"
echo "======================"${normal}
echo "Please answer the following question."
echo "Hitting [Enter] will continue with the default 'No' option"
echo

# Point of no return
read -p "Press Y to continue with Setting up your device as a Wifi Client." -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo ""
    echo "Goodbye!"
    exit 1
fi
cfg_wifi_client
}

function chk_status () {
echo "${bold}Current Settings:"
echo "=================${normal}"

#Check if current profile is Access Point
echo "Having a guess at current  profile..."
if grep -v '^#' "/etc/network/interfaces" | grep 'wpa-ssid' >/dev/null ; then
  WIFI_PROFILE=CLIENT
else
  WIFI_PROFILE=AP
fi


IPv4_NETMASK=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f4 | awk '{ print $1}');
IPv4_ADDRESS_BR=$(ip addr | awk '/inet/ && /br0/{sub(/\/.*$/,"",$2); print $2}');
InetModeBR0=$(grep 'iface\ br0' "/etc/network/interfaces" | awk -F ' ' '{print $4}');
BR_Iface1=$(cat /etc/network/interfaces |grep bridge_ports | awk -F ' ' '{print $2}');
BR_Iface2=$(cat /etc/network/interfaces |grep bridge_ports | awk -F ' ' '{print $3}');
GATEWAY_BR0=$(ip route show 0.0.0.0/0 dev br0 | cut -d\  -f3);
IP4_DNS1=$(grep -v '^#' "/etc/resolv.conf" | awk '/nameserver/{i++}i==1' /etc/resolv.conf | awk '{ print $2}');
IP4_DNS2=$(grep -v '^#' "/etc/resolv.conf" | awk '/nameserver/{i++}i==2' /etc/resolv.conf | awk '{ print $2}');

#Display
echo "${bold}Wifi $WIFI_PROFILE.${normal}";
echo "";

if [[ $WIFI_PROFILE = CLIENT ]] ; then
 get_SSID;
 printf "  SSID:                     ${CLIENT_SSID}\n\r";
 echo "";
fi

if [[ $WIFI_PROFILE = AP ]] ; then
 get_AP_SSID
 printf "  SSID:                     ${AP_SSID}\n\r";
 get_AP_CHAN
 printf "  Channel:                  ${AP_CHAN}\n\r";
 get_AP_COUNTRY
 printf "  Country:                  ${AP_COUNTRY}\n\r";
 echo ""
fi

printf "${bold}Interface - br0 (Bridge)${normal}\n\r";
printf "  Bridged I/F:              ${BR_Iface1}, ${BR_Iface2}\n\r";
printf "  br0 Inet Mode:            ${InetModeBR0}\n\r";
printf "  br0 IPv4 Address:         ${IPv4_ADDRESS_BR}\n\r";
printf "  br0 gateway:              ${GATEWAY_BR0}\n\r";
printf "  Primary DNS:              ${IP4_DNS1}\n\r";
printf "  Secondary DNS:            ${IP4_DNS2}\n\r";
echo "-----------------------------------------";

}

function scan_wifi () {
chk_device
clear
printf "%s\t\t%s\t%s\n" "${bold}ESSID" "Security" "Signal${normal}"
iwlist wlan0 scan | while read line
do   
   case $line in
    *ESSID*)
        line=${line#*ESSID:}
	    printf $line
        ;;
	*Encryption*)
		line=${line#*Encryption key:}
	    printf "%s\t $line"
        ;;
	*Signal*level*)
		line=${line#*Signal level=}
	    printf "%s\t\t $line \n\r"
        ;;	
   esac
done 
}

function _menu () {
echo "${bold}  Choose an Option"
echo " ===================${normal}"
	echo "  1 - Wifi Access Point"
	echo "  2 - Wifi Client"
	echo "  3 - Configure IP - Static"
	echo "  4 - Configure IP - DHCP"
	echo "  5 - Current Status"
	echo "  6 - Wifi Scan"
	echo "  N - None"
	while true; do
		read -p " Select an option from the list above. " answer
		case $answer in
			1 ) clear; go_AP; break;;
			2 ) clear; go_Client; break;;
			3 ) clear; cfg_static_ip; break;;
			4 ) clear; cfg_dhcp_ip; break;;
			5 ) clear; chk_status; break;;
			6 ) clear; scan_wifi; break;;
			[Nn] ) break;;
			* ) echo "Please select a valid option.";;
		esac
	done
}

###################################################
# Environmental Checks
###################################################
clear
chk_root
chk_internet

###################################################
# Main
###################################################

_menu

exit

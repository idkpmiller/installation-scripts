#!/bin/bash

DIR=$(cd $(dirname $0) && pwd)
cd $DIR


do_finish () {
  echo "Normal Close - User exited."
  exit
}

tempfile3=/tmp/dialog_3_$$
trap "rm -f $tempfile3" 0 1 2 5 15

_main () {
   dialog --title "VoIP ToolSet" \
           --menu "Please choose an option:" 15 55 5 \
                   1 "Port Scan" \
                   2 "SIPp" \
                   Q "Quit" 2> $tempfile3

   retv=$?
   choice=$(cat $tempfile3)
   [ $retv -eq 1 -o $retv -eq 255 ] && do_finish
cd scripts
   case $choice in
       1) ./ctrl-chkhost.sh
           ;;
       2) ./ctrl-sipp.sh
           ;;
       Q) clear
          exit 0
           ;;
   esac
clear
}

_main


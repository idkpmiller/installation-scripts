#!/bin/bash +x
#
# Monit install for apt and yum package managers 

### USER EDITS START HERE
EMAILADDR="idkpmiller@sip2serve.com"
EMAILPASSWD="Networking1"
PRIMAILSVR="192.168.0.7"
SECMAILSVR="smtp.gmail.com"
SECMAILPORT="587"
WEBPORT="2812"
CYCLEINTERVAL="60"
STARTDELAY="120"
HOSTNM=$(hostname -f)
IPv4_ADDRESS=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
### USER EDITS STOP HERE

#---------------------
# Functions
#---------------------
function func_chkRoot() {
echo "================================================================================"
echo "Checking for root Priviledges:"
echo "================================================================================"
logger "$0 entered OS ENVIRONMENT CHECKS" 
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
}

function func_chkInternet () {
/usr/bin/wget -q --tries=10 --timeout=5 http://www.google.com -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection working"
        /bin/rm /tmp/index.google
fi
}

function func_chkPkgMgr() {
type yum >/dev/null 2>&1 || {  PKGMGR="apt"; }
type apt-get >/dev/null 2>&1 || {  PKGMGR="yum"; }
}

function func_updSystem() {
echo '###############'
echo 'update system'
echo '###############'
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  apt-get -y update
else
  # RHEL code
  yum update  
fi
}

################END of FUNCTIONS ################

#---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
func_chkInternet
func_chkPkgMgr
func_updSystem


#### install requirements
echo '###############'
echo 'install requirements'
echo '###############'
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  echo "No known requirements"
else
  # RHEL code
  /bin/rpm -ivh http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
  echo "Requirement Installed."
fi

#### Download and Install Monit
echo '###############'
echo 'Install Monit '
echo '###############'
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  apt-get update
  apt-get install -y monit
else
  # RHEL code
  yum update
  yum install monit -y  
fi

#### Configure base Monit
echo '###############'
echo 'Configure Monit '
echo '###############'
file="/etc/monit/monitrc"
if [ -f "$file" ]
then

  #creat the default conf file to point at the configuration directory
  echo -en "Create Default monitrc file                         "
  cat <<EOF > /etc/monit/monitrc
set daemon $CYCLEINTERVAL
  with start delay $STARTDELAY
# set logfile syslog facility log_daemon                       
set logfile /var/log/monit.log
set idfile /var/lib/monit/id
set statefile /var/lib/monit/state
set mail-format { subject: monit alert --  $EVENT $SERVICE $HOST }
set mailserver $PRIMAILSVR, $SECMAILSVR port $SECMAILPORT
    username "$EMAILADDR" password "$EMAILPASSWD"
    using tlsv1
    with timeout 15 seconds

set eventqueue
    basedir /var/lib/monit/events # set the base directory where events will be stored
    slots 20                     # optionally limit the queue size
set alert $EMAILADDR
set httpd port $WEBPORT
    allow localhost
    allow 192.168.0.0/16
    allow 172.16.0.0/12
    allow 10.0.0.0/8

include /etc/monit/conf.d/*
EOF

# add the initial configuration file
cat <<EOF > /etc/monit/conf.d/system_base.conf
# Basic systemm checks
CHECK SYSTEM $HOSTNM
    if loadavg (1min) > 4 then alert
    if loadavg (5min) > 2 then alert
    if memory usage > 75% then alert
    if swap usage > 25% then alert
    if cpu usage (user) > 70% then alert 
    if cpu usage (system) > 30% then alert
    if cpu usage (wait) > 20% then alert
EOF


else
  # RHEL code
  :
  :  
fi

/etc/init.d/monit reload

echo ""
echo "The Monit Web UI can be found at:"
echo "HTTP://$IPv4_ADDRESS:2812"
echo "Script complete, this could be a great time to reboot."

exit


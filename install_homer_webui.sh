#!/bin/bash
##############################
# Version 0.1 - 
#   Initial release 09/11/2014
#   last updated 09/11/2014
#
# by Paul Miller
##############################
# DESCRIPTION:
# installation script for 
# LAMP (Apache, MySQL, PHP).
#
# Optionally PHP GUI
# Script is focused on Raspberry Pi
# Wheezy installations. But may
# function on other Debian/Ubuntu
# Installs. 
##############################

IPv4_ADDRESS=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
FQDN=$(hostname -f)
DIALOG="$(which whiptail dialog)"

# Locate any existing apps
APACHE="$(which apache2)"
MYSQL="$(which mysqld)"
PGS=""

# establish text format options
bold=`tput bold`
normal=`tput sgr0`

# initialise variables
WEBSVR="NONE"
DBSVR="NONE"
PHPSVR=0
PHPMYADMIN=0
WEBUIREPO="https://code.google.com/p/homer/"
WEBUIDEST="/var/www/html/"

# Define web app packages
APACHEPKG="apache2"

# Define php app packages
PHPPKG="php5 zip unzip"
PHPMYSQLPKG="php5-mysql"
PHPAPACHEPKG="php5-cli php-db php5-cgi libapache2-mod-php5"


# Define database app packages
MYSQLPKG="mysql-server mysql-client"


#---------------------
# Functions
#---------------------
function pTitle () {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as arguament to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot() {
# Supports: text, whiptail, dialog
# Requires:
# DIALOG="$(which whiptail dialog)" # variable
##############################################

logger "$0 Entered OS ENVIRONMENT CHECKS" 

if [[ $DIALOG ]]; then
	(( `id -u` )) && $DIALOG --msgbox --title "Checking for root Priviledges:" "Must be ran as root, try prefixing with sudo." 8 44 && exit 1
else
	pTitle "Checking for root Priviledges:"
	(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
fi
#func_chkRoot
}

function func_chkInternet () {
pTitle "Checking Internet Connectivity."
/usr/bin/wget -q --tries=10 --timeout=5 "http://www.google.com" -O /tmp/index.google &> /dev/null
if [[ ! -s /tmp/index.google ]];then
	echo "No Internet connection. Exiting."
	/bin/rm /tmp/index.google
	exit 1
else
	echo "Internet connection working"
	/bin/rm /tmp/index.google
fi
#func_chkInternet
}

function func_updSystem() {
pTitle 'Updating system, please Wait.'
apt-get -y -qq update
  
if [[ $DIALOG ]]; then
	#Check if user want to upgrade now.
	whiptail --yesno --title "Upgrade" "Would you like to upgrade the system now?" 10 50
	if [[ $? -ne 0 ]]; then
		echo "NO, selected so continuing..."
	else
		apt-get -y upgrade;
		apt-get -y autoremove;
	fi
else  
	while true; do
	read -p "Would you like to upgrade the system now? (Y/N)" answer
		case $answer in
			[Yy] ) 
				echo "Upgrading please wait..";
				apt-get -y upgrade;
				apt-get -y autoremove;
				break;;
			[Nn] ) echo "NO, selected so continuing"; break;;
			* ) echo "Please answer Y or N.";;
		esac
	done
fi
#End func_updSystem    
}

function _rmdir () {
if [ -d $1 ];then
  /bin/rm -r $1
  RESULT=$?
  if [[ $RESULT != 0 ]]; then echo "Remove Directory $1 Failed."; exit 1; fi
fi

}

function func_InstPackages(){
#Usage:
# call function with list of packages to be installed as the argument"
# e.g. 
# MusicPKG="mp3 sox"
# func_InstPackages MusicPKG
##########################
PACKAGES="${!1}"

for pkg in $PACKAGES; do
	if apt-get -y -qq install $pkg; then
		echo "${bold}Successfully installed $pkg ${normal}"
	else
		echo "${bold}Error installing $pkg ${normal}"
		exit 1
	fi
done

pTitle "Package $1 installation complete."
#End func_InstPackages
}

func_getGIT () {
GITREPO="${!1}"
DESTDIR="${!2}"
dpkg -l | grep -qw git || apt-get install git-core

_rmdir "/tmp/git-tmp"
git clone $GITREPO /tmp/git-tmp/
cp -R /tmp/git-tmp/ $DESTDIR
mv ${DESTDIR}git-tmp ${DESTDIR}webhomer
_rmdir "/tmp/git-tmp"

}

########### WEB #############
  
function func_InstApache() {
logger "$0 Entered func_InstApache"
pTitle "Installing Apache:"
WEBSVR="apache2"
func_InstPackages APACHEPKG

#setup the servername
echo 'ServerName       $FQDN' > /etc/apache2/conf.d/servername.conf

chown www-data:www-data /var/www
chmod 775 /var/www
usermod -a -G www-data spark

a2enmod ssl 
a2ensite default-ssl

/etc/init.d/apache2 restart
# End func_InstApache
}

######### DATABASE ##########

function func_InstMySQL() {
logger "$0 Entered func_InstMySQL"
pTitle "Installing MySQL:"
func_InstPackages MYSQLPKG
#Set MySQL to start automatically
update-rc.d mysql remove
update-rc.d mysql defaults
DBSVR="mysqld"
# End func_InstMySQL
}

########### PHP #############
function func_InstPHP () {
# install PHP Web
	echo "Adding apache module"
    func_InstPackages PHPAPACHEPKG
    pTitle "Installing PhP:"
	func_InstPackages PHPPKG
	  
	# create a basic php test page
    echo '<?php phpinfo(); ?>' > /var/www/phpinfo.php
	PHPSVR=1
	
# install PHP admin for mysql
	func_InstPackages PHPMYSQLPKG
    func_InstPHPMyAdmin
# END func_InstPHP
}

function func_InstPHPMyAdmin () {
# actions optionally to perform when a specific database is installed and PHP
echo "Entering func_InstPHPMyAdmin, PHPSVR=$PHPSVR, DBSVR=$DBSVR"
if [[ $PHPSVR -eq "1" ]] && [[ $DBSVR == "mysqld" ]]
then
  #if there is also a dialog based service then provide GUI feedback to the user
  if [[ $DIALOG ]]
  then
    $DIALOG --yesno "Would you like phpmyadmin installed, a web GUI to adminstate MySQL?" 10 70
    answer=$?
    if [[ $answer == 0 ]]; then
      echo "You have selected to install the web GUI app"
	  apt-get -y -qq install phpmyadmin
	  PHPMYADMIN=1
    else
      echo "You have selected to not install the web GUI app"
    fi
  else
    #if there is no dialog based service then provide text feedback to the user
    while true; do
      read -p "Would you like phpmyadmin installed, a web GUI to adminstate MySQL? (Y/N)" answer
        case $answer in
          [Yy] ) 
			apt-get -y -qq install phpmyadmin
			PHPMYADMIN=1
			echo "|================================================================================|"
			echo "| The next package to be installed is PHPMyAdmin as part of the installation     |"
			echo "| a selection of the Web Server is Required.                                     |"
			echo "| You will see the following questions:                                          |"
			echo "|                                                                                |"
			case $WEBSVR in
				apache2 ) 
				  echo 'Include /etc/phpmyadmin/apache.conf' >> /etc/apache2/apache2.conf
				  echo "| Web server to reconfigure automatically: ${bold}<-- Apache2${normal}           |"
				  echo "| Configure database for phpmyadmin with dbconfig-common? ${bold}<-- No${normal} |"
				  ;;
				* ) echo "An unexpected error has occurred.";;
			esac
			echo "|================================================================================|"
			break
			;;
          [Nn] ) echo "You have selected to not install the web GUI app"; break;;
          * ) echo "Please select a valid option.";;
        esac
    done
  fi
fi

# END func_InstPHPMyAdmin
}

########### OTHER #############
function func_configWebUI () {
# Copy the examples and rename
cp /var/www/html/webhomer/configuration_example.php /var/www/html/webhomer/configuration.php
cp /var/www/html/webhomer/preferences_example.php /var/www/html/webhomer/preferences.php
cd /var/www/html/webhomer/

# modify configuration parameters
# TODO
#PWo="('PW', "root")"
#PWn="('PW', "spark")"
#sed -i -e 's/('PW', "root")/('PW', "spark")/g' configuration.php

# modify preferences parameters


}

function func_Complete(){
#func_Complete
logger "$0 Entered func_Complete"
MESSAGE="A $WEBSVR webserver can be found by "
MESSAGE=$MESSAGE"pointing your web browser at:\n"
MESSAGE=$MESSAGE"\n"

for addr in $IPv4_ADDRESS; do
  MESSAGE=$MESSAGE"HTTP:          http://$addr\n"
  
  if [[ $PHPSVR ]]; then
    MESSAGE=$MESSAGE"PhP Test:      http://$addr/phpinfo.php\n"
  fi
  
  if [[ $PHPMYADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPMyAdmin     http://$addr/phpmyadmin\n"
  fi
  
  if [[ $PHPLITEADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPLiteAdmin   http://$addr/phpliteadmin.php (passwd = admin)\n"
  fi
  
  if [[ $PHPPGADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPPGAdmin   http://$addr/phppgadmin (login postgres, blank passwd)\n"
  fi
done

if [[ $DIALOG ]]
then
  $DIALOG --msgbox --title "Script is Complete!" "$MESSAGE" 18 78
else
  pTitle "${bold}The script is complete.${normal}"
  echo -e "$MESSAGE"
fi
# func_Complete
}

##---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
#func_chkInternet
#func_updSystem

#---------------------
# INSTALL Applications
#---------------------

# Install LAMP
func_InstMySQL
func_InstApache
func_InstPHP

# Get the Homer Web UI
func_getGIT WEBUIREPO WEBUIDEST  	
# set permissions
chown -R www-data:www-data /var/www/html

#---------------------
# FINISHING OFF
#---------------------	
if [[ $WEBSVR != "NONE" ]]; then
  /etc/init.d/$WEBSVR restart
  func_Complete
else
  echo "Web server not installed."
fi
echo "Done!"
echo "================================================================================"

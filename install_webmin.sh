#!/bin/bash +x
#
# webmin install for apt and yum package managers 

#### check distro
type yum >/dev/null 2>&1 || {  PKGMGR="apt"; }
type apt-get >/dev/null 2>&1 || {  PKGMGR="yum"; }


function func_chkRoot() {
echo "================================================================================"
echo "Checking for root Priviledges:"
echo "================================================================================"
logger "$0 entered OS ENVIRONMENT CHECKS" 
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
}

function func_chkInternet () {
/usr/bin/wget -q --tries=10 --timeout=5 http://www.google.com -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection working"
        /bin/rm /tmp/index.google
fi
}


func_chkRoot
func_chkInternet

#### update system
echo '###############'
echo 'update system'
echo '###############'
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  apt-get update
else
  # RHEL code
  yum update  
fi

#### install requirements
echo '###############'
echo 'install requirements'
echo '###############'
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  apt-get install -y perl libnet-ssleay-perl openssl libauthen-pam-perl libpam-runtime libio-pty-perl apt-show-versions python
else
  # RHEL code
  yum -y install wget perl
  echo "selinux will be disabled permanently after next reboot"
  sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config
  echo "disable selinux temporary (without reboot)"
  setenforce 0
  iptables -I INPUT 4 -m state --state NEW -m tcp -p tcp --dport 10000 -j ACCEPT
  echo "Save rule"
  service iptables save
  echo "Now activate new rule."
  service iptables restart
fi

#### update repo
echo '###############'
echo '   Repo setup  '
echo '###############' 
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  echo "deb http://download.webmin.com/download/repository sarge contrib" >> /etc/apt/sources.list
  echo "deb http://webmin.mirror.somersettechsolutions.co.uk/repository sarge contrib" >> /etc/apt/sources.list
else
  # RHEL code
  echo "[Webmin]" >> /etc/yum.repos.d/webmin.repo
  echo "name=Webmin Distribution Neutral" >> /etc/yum.repos.d/webmin.repo
  echo "#baseurl=http://download.webmin.com/download/yum" >> /etc/yum.repos.d/webmin.repo
  echo "mirrorlist=http://download.webmin.com/download/yum/mirrorlist" >> /etc/yum.repos.d/webmin.repo
  echo "enabled=1" >> /etc/yum.repos.d/webmin.repo  
fi

#### Install GPG key
echo '###############'
echo '  Install GPG  '
echo '###############' 
cd /root
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  wget http://www.webmin.com/jcameron-key.asc
  apt-key add jcameron-key.asc
else
  # RHEL code
  wget http://www.webmin.com/jcameron-key.asc
  rpm --import jcameron-key.asc  
fi

#### Download and Install webmin
echo '###############'
echo 'Install Webmin '
echo '###############'
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  apt-get update
  apt-get install -y webmin
else
  # RHEL code
  yum update
  yum install webmin  
fi
exit






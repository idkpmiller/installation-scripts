#!/bin/bash
# taken from article at:
# http://bloggeek.me/webrtc-raspberripi-nodejs/

# grab node.js and install websockets
wget http://node-arm.herokuapp.com/node_latest_armhf.deb
sudo dpkg -i node_latest_armhf.deb
npm install websocket

# get the webrtc example code
# For me, that would be Rob Manson�s Getting Started With WebRTC. 
# This code is explained in detail in his book under the same name
# which I reviewed here already. Since this is on Github, we follow
# these instructions:
git clone git://github.com/buildar/getting_started_with_webrtc
cd getting_started_with_webrtc

# Rob�s sample requires one additional step. The Node.js code sends
# a .html file to the browser, and in it, there�s an indication where
# the Websocket signaling need to go to. In the HTML file, that�s set
# to �localhost�. I had to change that value to the IP address of my
# Raspberry Pi. For that, just use:
nano video_call_with_chat_and_file_sharing.html

# Change in line 149 from �ws://localhost:1234? to �ws//<ip-address>:1234?

# Start the server by calling:
node webrtc_signal_server.js
exit




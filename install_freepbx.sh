#/bin/bash

##############################
# by Paul Miller
# installation script for 
# FreePBX with
# a focus on Raspberry Pi
# Wheezy installations.  
##############################

function funcfreepbx () {

#check if asterisk is running, before FreePBX is installed.


if test -f /var/run/asterisk/asterisk.pid; then
   
   #Set Apache to run as asterisk
	sed -i 's/www-data/asterisk/g'  /etc/apache2/envvars
	/etc/init.d/apache2 restart
	mysqladmin -u root password 'passw0rd'

   
	# Get FreePBX - Unzip and modify
	cd /usr/src
	rm -rf freepbx*.tar.gz
	wget http://mirror.freepbx.org/freepbx-2.11.0.tar.gz
	tar zxfv freepbx*.tar.gz
	rm -rf freepbx*.tar.gz
	mv freepbx-2* freepbx
	mkdir /usr/share/freepbx /var/lib/asterisk/bin

	cd /usr/src/freepbx

	#make some changes to FreePBX
	sed -i 's/AUTHTYPE=none/AUTHTYPE=database/g'  amportal.conf
	sed -i 's/SERVERINTITLE=false/SERVERINTITLE=true/g'  amportal.conf
	sed -i 's/\/var\/www\/html/\/usr\/share\/freepbx/g'  amportal.conf
	sed -i 's/# ZAP2DAHDICOMPAT=true|false/ZAP2DAHDICOMPAT=true/g'  amportal.conf
	#sed -i 's/FOPRUN=true/FOPRUN=false/g'  amportal.conf

	#create the MySQL databases
	mysqladmin -uroot -ppassw0rd create asterisk
	mysqladmin -uroot -ppassw0rd create asteriskcdrdb
	mysql -uroot -ppassw0rd  asterisk < SQL/newinstall.sql
	mysql -uroot -ppassw0rd asteriskcdrdb < SQL/cdr_mysql_table.sql
	mysql -uroot -ppassw0rd -e "GRANT ALL PRIVILEGES ON asterisk.* TO asteriskuser@localhost IDENTIFIED BY 'amp109'"
	mysql -uroot -ppassw0rd -e "GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO asteriskuser@localhost IDENTIFIED BY 'amp109'"

	cp amportal.conf /etc/amportal.conf
	chown -R asterisk:asterisk /etc/amportal.conf
	./install_amp --username=asteriskuser --password=amp109



	chown -R asterisk:asterisk /etc/asterisk
	chown -R asterisk:asterisk /usr/share/freepbx
	chown -R asterisk:asterisk /var/lib/asterisk



	#Bring modules upto date and get useful modules
	/var/lib/asterisk/bin/module_admin upgradeall
	
	/var/lib/asterisk/bin/module_admin download asterisk-cli
	/var/lib/asterisk/bin/module_admin download asteriskinfo 
	/var/lib/asterisk/bin/module_admin download backup 
	/var/lib/asterisk/bin/module_admin download fw_ari
	/var/lib/asterisk/bin/module_admin download fw_fop
	/var/lib/asterisk/bin/module_admin download iaxsettings 
	/var/lib/asterisk/bin/module_admin download javassh 
	/var/lib/asterisk/bin/module_admin download languages 
	/var/lib/asterisk/bin/module_admin download logfiles 
	/var/lib/asterisk/bin/module_admin download phpinfo 
	/var/lib/asterisk/bin/module_admin download sipsettings 
	/var/lib/asterisk/bin/module_admin download weakpasswords 
	/var/lib/asterisk/bin/module_admin download fw_langpacks

	/var/lib/asterisk/bin/module_admin install asterisk-cli
	/var/lib/asterisk/bin/module_admin install asteriskinfo 
	/var/lib/asterisk/bin/module_admin install backup 
	/var/lib/asterisk/bin/module_admin install fw_ari
	/var/lib/asterisk/bin/module_admin install fw_fop
	/var/lib/asterisk/bin/module_admin install iaxsettings 
	/var/lib/asterisk/bin/module_admin install javassh 
	/var/lib/asterisk/bin/module_admin install languages 
	/var/lib/asterisk/bin/module_admin install logfiles 
	/var/lib/asterisk/bin/module_admin install phpinfo 
	/var/lib/asterisk/bin/module_admin install sipsettings 
#	/var/lib/asterisk/bin/module_admin install weakpasswords 
	/var/lib/asterisk/bin/module_admin install fw_langpacks

	/var/lib/asterisk/bin/module_admin reload

	#Setup FreePBX web pages.
	touch /etc/apache2/sites-available/freepbx.conf
echo '

Alias /pbx /usr/share/freepbx/

DocumentRoot /usr/share/freepbx

<directory /usr/share/freepbx>
	AllowOverride all
	Options Indexes FollowSymLinks
	order allow,deny
	allow from all
	AuthName "PBX Administrator"
	AuthType Basic
	AuthUserFile /dev/null 
	AuthBasicAuthoritative off
	Auth_MySQL on
	Auth_MySQL_Authoritative on
	Auth_MySQL_Username asteriskuser
	Auth_MySQL_Password amp109
	Auth_MySQL_DB asterisk
	Auth_MySQL_Password_Table ampusers
	Auth_MySQL_Username_Field username
	Auth_MySQL_Password_Field password_sha1
	Auth_MySQL_Empty_Passwords off
	Auth_MySQL_Encryption_Types SHA1Sum
	Require valid-user
</directory>

<directory /usr/share/panel>
	AllowOverride all
	Options Indexes FollowSymLinks
	order allow,deny
	allow from all
	AuthName "Operator Panel"
	AuthType Basic
	AuthUserFile /dev/null 
	AuthBasicAuthoritative off
	Auth_MySQL on
	Auth_MySQL_Authoritative on
	Auth_MySQL_Username asteriskuser
	Auth_MySQL_Password amp109
	Auth_MySQL_DB asterisk
	Auth_MySQL_Password_Table ampusers
	Auth_MySQL_Username_Field username
	Auth_MySQL_Password_Field password_sha1
	Auth_MySQL_Empty_Passwords off
	Auth_MySQL_Encryption_Types SHA1Sum
	Require valid-user
</directory>

<IfModule mod_php5.c>
php_flag magic_quotes_gpc Off
php_flag track_vars On
php_flag register_globals Off
php_value upload_max_filesize 100M
php_value memory_limit 100M
php_value magic_quotes_gpc off
</IfModule>

<IfModule mod_auth_mysql.c>

' > /etc/apache2/sites-available/freepbx.conf

ln -s  /etc/apache2/sites-available/freepbx.conf /etc/apache2/sites-enabled/freepbx.conf


	echo "
	Options -Indexes
	<Files .htaccess>
	deny from all
	</Files> 
	" > /usr/share/freepbx/admin/modules/.htaccess
	
	
	#Set the AMI to only listen on 127.0.0.1
	sed -i 's/bindaddr = 0.0.0.0/bindaddr = 127.0.0.1/g' /etc/asterisk/manager.conf


#Get FreePBX to start automatically on boot.

	echo '#!/bin/bash' > /etc/init.d/amportal-start
	echo '/usr/local/sbin/amportal start' >> /etc/init.d/amportal-start
	chmod +x /etc/init.d/amportal-start
	update-rc.d amportal-start start 99 2 3 4 5 .
	

	echo '#!/bin/bash' > /etc/init.d/amportal-stop
	echo '/usr/local/sbin/amportal stop' >> /etc/init.d/amportal-stop
	chmod +x /etc/init.d/amportal-stop
	update-rc.d amportal-stop stop 10 0 1 6 .

	/etc/init.d/asterisk stop
	update-rc.d -f asterisk remove

	/etc/init.d/apache2 restart
	amportal kill
	dahdi_genconf -F
	/etc/init.d/dahdi restart
	amportal start

else
	clear
	echo "asterisk is not running"
	echo "please correct this before installing FreePBX"
	echo "Press enter to return to the install menu."
	read temp
fi

	
#funcfreepbx
}

function funcdependencies(){
#Install Dependencies
clear

KERNELARCH=$(uname -p)

apt-get -y autoremove
apt-get -f install

apt-get -y update

apt-get -y remove sendmail

apt-get -y upgrade

echo ""
echo ""
echo "If the Kernel has been updated, you are advised to reboot your server and run the install script again!"
echo "If you are not sure whether the kernel has been updated, reboot and start again (once only!)"
echo ""
echo "Press CTRL C to exit and reboot, or enter to continue"
read TEMP


#check timezone
dpkg-reconfigure tzdata

#install dependencies

#for asterisk 11
apt-get -y install libsqlite3-dev sqlite3

apt-get -y install mysql-server
apt-get -y install mysql-client libmysqlclient-dev build-essential sysvinit-utils libxml2 libxml2-dev libncurses5-dev libcurl4-openssl-dev libvorbis-dev libspeex-dev unixodbc unixodbc-dev libiksemel-dev wget iptables php5 php5-cli php-pear php5-mysql php-db libapache2-mod-php5 php5-gd php5-curl sqlite libnewt-dev libusb-dev zlib1g-dev  libsqlite0-dev  libapache2-mod-auth-mysql sox mpg123 postfix flite php5-mcrypt python-setuptools python-mysqldb python-psycopg2 python-sqlalchemy ntp

#extras 
apt-get -y install wget iptables vim subversion flex bison libtiff-tools ghostscript autoconf gcc g++ automake libtool patch

apt-get -y install linux-headers-$(uname -r)

#remove the following packages for security.
apt-get -y remove nfs-common portmap

mkfifo /var/spool/postfix/public/pickup

#Enable Mod_Auth_MySQL
ln -s /etc/apache2/mods-available/auth_mysql.load /etc/apache2/mods-enabled/auth_mysql.load

#Set MySQL to start automatically
update-rc.d mysql remove
update-rc.d mysql defaults


#Install a TFTP server
    apt-get install xinetd tftpd tftp -y
    echo '
    service tftp
    {
    protocol        = udp
    port            = 69
    socket_type     = dgram
    wait            = yes
    user            = nobody
    server          = /usr/sbin/in.tftpd
    server_args     = /tftpboot
    disable         = no
    }
    ' > /etc/xinetd.d/tftp
    mkdir /tftpboot
    chmod -R 777 /tftpboot
    echo 'includedir /etc/xinetd.d' >> /etc/xinetd.conf
    /etc/init.d/xinetd start

#funcdependencies
}

echo "================================================================================"
echo "Pre-Installation Checks:"
echo "================================================================================"
funcdependencies

echo "================================================================================"
echo "FreePBX Installation:"
echo "================================================================================"
funcfreepbx
	
done

#!/bin/sh

#	Script name:			R19_AS_InstallationChecklist.sh
#	Version:		     	3.0
#	Original Author:    	Voice Engineering group Alcatel-Lucent NZ
#	Maintainer:				Spark Connect
#	Purpose:		    	Captures Unix settings for BroadWork Unix installation check
# 	BroadWorks version:		R19.0 
#  	Environment:       		BroadWorks; Any
#  	Runas:					root
#  	Inputs:					none
# 	Output:					capture terminal session
#    
#    Revision history:
#      1.0    16 Sep 2013  	Released for MOP version 1
#      2.0    13 Oct 2014  	Released for MOP version 2.1
#							Updated to maintenance by Spark
#      3.0    15 Dec 2014   Added check for root privilege
#                           Tidied up output
#      3.1    15 Dec 2014   Improved the cosmetics to the sh_package function                           

BOOTUP=color
RES_COL=60
MOVE_TO_COL="echo -en \\033[${RES_COL}G"
SETCOLOR_SUCCESS="echo -en \\033[1;32m"
SETCOLOR_FAILURE="echo -en \\033[1;31m"
SETCOLOR_WARNING="echo -en \\033[1;33m"
SETCOLOR_NORMAL="echo -en \\033[0;39m"

#---------------------
# Functions
#---------------------
func_chkRoot() {
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
}

echo_success() {
  [ "$BOOTUP" = "color" ] && $MOVE_TO_COL
  echo -n "["
  [ "$BOOTUP" = "color" ] && $SETCOLOR_SUCCESS
  echo -n $"  OK  "
  [ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 0
}

echo_failure() {
  [ "$BOOTUP" = "color" ] && $MOVE_TO_COL
  echo -n "["
  [ "$BOOTUP" = "color" ] && $SETCOLOR_FAILURE
  echo -n $"FAILED"
  [ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 1
}

sh_package () {
PACKAGE="${1}"
echo -n "$PACKAGE"
if rpm -q $PACKAGE &> /dev/null; then
  echo_success
else
  echo_failure
fi
}

cmd(){
	echo ">>> $*"
	$*
	returnValue=$?
	echo
	
	return $returnValue
}

heading(){
	echo '+---------------------------------------------------------------------+'
	echo "|  $*"
	echo '+---------------------------------------------------------------------+'
    echo ""
}


PackageList() {

	heading "Spark & BroadWorks Agreed packages for installation"
	sh_package bash
	sh_package compat-libstdc++-33
	sh_package expect
	sh_package gawk
	sh_package gdb
	sh_package ksh
	sh_package libaio
	sh_package lm_sensors
	sh_package logrotate
	sh_package lsof
	sh_package net-snmp
	sh_package nss-pam-ldapd
	sh_package ntp
	sh_package openssh
	sh_package openssl
	sh_package procps
	sh_package redhat-lsb
	sh_package rsync
	sh_package sssd
	sh_package sudo
	sh_package sysstat
	sh_package xinetd
	echo ""

	heading "BroadWorks R19 packages for database servers (not agreed with Spark)"
	sh_package compat-libstdc++-33
	sh_package cpp
	sh_package elfutils-libelf
	sh_package elfutils-libelf-devel
	sh_package elfutils-libelf-devel-static
	sh_package gcc
	sh_package gcc-c++
	sh_package glibc
	sh_package glibc-common
	sh_package glibc-devel
	sh_package glibc-headers
	sh_package libaio
	sh_package libaio-devel
	sh_package libgcc
	sh_package libstdc++
	sh_package libstdc++-devel
	sh_package make
	sh_package numactl
	sh_package Ppstack
	sh_package sysstat
	sh_package unixODBC
	sh_package unixODBC-devel
	
	heading "BroadWorks R19 required (optional) support packages"
	sh_package dstat
	sh_package nmap
	sh_package screen
	echo ""
}

#3.2.1 Verify Interface Configuration and Basic IP Set-up
#
VerifyInterfaceConfig(){
	heading '3.2.1 Verify Interface Configuration and Basic IP Setup'
	echo '1) All server /etc/sysconfig/network files have proper hostname and gateway information.'
	cmd cat /etc/sysconfig/network
	echo '2) All server /etc/hosts files have proper IP addresses with one entry that has the server hostname and loghost as an alias and the localhost.localdomain has been removed.'
	echo '3) Application Server and Network Server redundant servers /etc/hosts contain entries for peer servers as outlined in the BroadWorks Software Management Guide.'
	cmd cat /etc/hosts
	echo '4) ifconfig –a indicates that all interfaces are active.'
	cmd /sbin/ifconfig -a
	echo '5) All interfaces are full-duplex after a reboot (check /var/log/messages logs).'
	if [ -r /var/log/messages ];then
		echo '>>> Should dump /var/log/messages for last reboot and check'
		echo
	else
		echo "Error: Cant read /var/log/messages. Need root access."
		echo
	fi
}

#3.2.1.1 Bonding (If Configured)
#
VerifyBonding(){
#	heading '3.2.1.1 Bonding If Configured'
	echo '1) Bonding is properly configured as defined in the BroadWorks Software Maintenance Guide as active/standby.'
	echo '2\) Unplugging the active bonded interface causes the standby interface to take over check /var/log/messages logs.'
	echo '3\)Plugging the active bonded interface back in, causes bonding to fail back check /var/log/messages logs.'
	echo "Check that the bonded interfaces configuration matches the Infrastructure design"
	echo "Fail over and fall back tests of each bonded interface are required."
	echo
}

#3.2.2 Verify DNS Configuration
VerifyDNSConfig(){
	heading '3.2.2 Verify DNS Configuration'
	echo '1) Application Server and Network Server /etc/resolv.conf and /etc/nsswitch.conf files contain entries as outlined in the BroadWorks Software Management Guide.'
	cmd cat /etc/resolv.conf
	cmd cat /etc/nsswitch.conf
	echo '2) Primary DNS server is operational (nslookup).'
	echo '3) Secondary DNS server is reachable when primary DNS is down (nslookup).'
	echo "Manually test Primary and Secondary DNS"
	echo 
	echo '4) Cluster and individual box FQDNs resolve (both A and SRV records for cluster FQDNs) (nslookup).'
	resolvers=`grep "^nameserver" /etc/resolv.conf|sed -n 's/^nameserver //
		s/ .*//p'`
	
	for clusterAddress in ns.sgnl.telecom.co.nz ps.sgnl.telecom.co.nz; do
		for resolver in $resolvers; do
			cmd dig @$resolver $clusterAddress +norecurse
			if [ $? -ne 0 ]; then #Error return value
			    echo ""
				echo "Error: dig @$resolver $clusterAddress +norecurse failed." >&2
			    echo ""
			fi
		done
	done
	
	echo
}

#3.2.3 Verify Hard Drive and Disk Mirroring Configuration
#
VerifyHardDriveConfig(){
	heading '3.2.3 Verify Hard Drive and Disk Mirroring Configuration'
	echo '1) A minimum 72 GB drive is required. There is no maximum size.'
	cmd df -k
	echo '2) Partition / is present as outlined in the BroadWorks Software Management Guide (df –k).'
	echo "Check from above df output."
	echo
	echo '3) Swap space is 1.5 times the amount of RAM on the server up to a maximum of 8 GB.'
	cmd cat /proc/swaps
	cmd cat /proc/meminfo
	echo '4) Directory /usr/local does not exist (BroadWorks creates a link to /bw/local).'
	cmd ls -ld /usr/local
	cmd ls -l /bw
	cmd ls -l /var
	echo '5) Ensure that Hardware RAID level 1 mirroring is configured.'
	echo "   - Not required"
	echo 
}

#3.2.4 Verify Linux Miscellaneous Items
#
VerifyLinuxMiscellaneous(){
	heading '3.2.4 Verify Linux Miscellaneous Items'
	echo '1) BroadWorks required RPMs as defined in the BroadWorks Software Management Guide are installed.'
	PackageList
	echo '2) RedHat up2date has been run to update all RPMs, but the kernel updates were skipped.'
	echo "What does this mean!"
	echo 
	echo '3) System time zone is correct (date).'
	cmd date -u
	cmd date
	echo '4) As required, external firewall is in place protecting all internal BroadWorks ports (as defined in the BroadWorks Server Security Guide.'
	echo "   - Not tested here"
	echo 
	echo '5) Default locale LANG is set to en_US (locale).'
	cmd echo $LANG
	echo '6) System Activity Reporter (sar) is enabled (sar).'
	cmd sar
	echo "Check for 5 minute interval"
	echo 
	echo '7) Network timing protocol is enabled and functioning (ntpq> lpeer). This can be done as part of the BroadWorks installation process.'
	cmd /usr/sbin/ntpq -c lpeers
	echo '8) Server is equipped with adequate memory (see the BroadWorks Recommended Hardware Guide).'
	cmd cat /proc/meminfo
}


# Execution starts here
#main()
###########
#Check root access
func_chkRoot

#Based on BW-InstallationChecklist-R17.0
echo "+---------------------------------------------------------------------+"
echo "  `date` HOSTNAME:`hostname`"
echo "+---------------------------------------------------------------------+"
echo
echo

#3.2.1 Verify Interface Configuration and Basic IP Setup
VerifyInterfaceConfig

#3.2.1.1 Bonding (If Configured)
VerifyBonding

#3.2.2 Verify DNS Configuration
VerifyDNSConfig

#3.2.3 Verify Hard Drive and Disk Mirroring Configuration
VerifyHardDriveConfig

#3.2.4 Verify Linux Miscellaneous Items
VerifyLinuxMiscellaneous

#3.2.5 Linux Virtualization
#For a Linux virtualized system, the standard Linux checklist above should be run for each host and guest domain.
#Virtualization checklist defined in the BroadWorks Virtualization Configuration Guide Appendix A has been verified.
echo ""
echo "3.2.5 Linux Virtualization"
echo "Manually verify the Virtualization configuration with the BroadWorks Virtualization Configuration Guide."

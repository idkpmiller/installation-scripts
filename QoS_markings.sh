#!/bin/bash
#Paul's DSCP marking iptables
# Releases
# 0.1 Initial December 2011
# 0.2 Add sipgate trunk specific markings for SIP
# changed torrent (scavenger)to port 57451


# Flush the tables first
#iptables -F
#iptables -t mangle -F

# Routing Group (48)
#=============================
# RIP, RIP-NG, OSPF, BGP
# Not used currently
#iptables -t mangle -A OUTPUT -p tcp --dport 179 -j DSCP --set-dscp 16 # BGP
#iptables -t mangle -A OUTPUT -p tcp --sport 179 -j DSCP --set-dscp 16 # BGP
#iptables -t mangle -A OUTPUT -p udp --dport 179 -j DSCP --set-dscp 16 # BGP
#iptables -t mangle -A OUTPUT -p udp --sport 179 -j DSCP --set-dscp 16 # BGP
iptables -t mangle -A OUTPUT -p udp --dport 520 -j DSCP --set-dscp 16 # RIP
iptables -t mangle -A OUTPUT -p udp --sport 520 -j DSCP --set-dscp 16 # RIP

# RealTime Group (46)
#=============================
# RTP & RTCP
#should be marked by application so audio and video differentiation can be obtained

#iptables -t mangle -A OUTPUT -p udp -m udp --sport 12000:12099 -j DSCP --set-dscp-class ef # mark RTP& RTCP packets with EF
#iptables -t mangle -A OUTPUT -p udp -m udp --sport 13000:13099 -j DSCP --set-dscp-class ef # mark RTP& RTCP packets with EF

# Interactive Group (40)
#=============================
# Telnet, SSH, RDP, VNC, NX, Xterm
iptables -t mangle -A OUTPUT -p tcp --dport 22 -j DSCP --set-dscp 40 # SSH
iptables -t mangle -A OUTPUT -p tcp --sport 22 -j DSCP --set-dscp 40 # SSH
iptables -t mangle -A OUTPUT -p tcp --dport 23 -j DSCP --set-dscp 40 # telnet
iptables -t mangle -A OUTPUT -p tcp --sport 23 -j DSCP --set-dscp 40 # telnet
iptables -t mangle -A OUTPUT -p tcp --dport 992 -j DSCP --set-dscp 40 # telnets
iptables -t mangle -A OUTPUT -p tcp --sport 992 -j DSCP --set-dscp 40 # telnets
iptables -t mangle -A OUTPUT -p tcp --dport 194 -j DSCP --set-dscp 40 # irc
iptables -t mangle -A OUTPUT -p tcp --sport 194 -j DSCP --set-dscp 40 # irc
iptables -t mangle -A OUTPUT -p tcp --dport 3389 -j DSCP --set-dscp 40 # RDP
iptables -t mangle -A OUTPUT -p tcp --sport 3389 -j DSCP --set-dscp 40 # RDP
iptables -t mangle -A OUTPUT -p tcp --dport 5900 -j DSCP --set-dscp 40 # VNC
iptables -t mangle -A OUTPUT -p tcp --sport 5900 -j DSCP --set-dscp 40 # VNC
iptables -t mangle -A OUTPUT -p tcp --dport 5800 -j DSCP --set-dscp 40 # VNC
iptables -t mangle -A OUTPUT -p tcp --sport 5800 -j DSCP --set-dscp 40 # VNC
iptables -t mangle -A OUTPUT -p tcp --dport 6000:6003 -j DSCP --set-dscp 40 # xterm
iptables -t mangle -A OUTPUT -p tcp --sport 6000:6003 -j DSCP --set-dscp 40 # xterm

# OAM Group (16)
#=============================
# webmin NRPE SNMP TRAPs
iptables -t mangle -A OUTPUT -p tcp --dport 10000 -j DSCP --set-dscp 16 # Webmin
iptables -t mangle -A OUTPUT -p tcp --sport 10000 -j DSCP --set-dscp 16 # Webmin
iptables -t mangle -A OUTPUT -p tcp --dport 5666 -j DSCP --set-dscp 16 # NRPE
iptables -t mangle -A OUTPUT -p tcp --sport 5666 -j DSCP --set-dscp 16 # NRPE
iptables -t mangle -A OUTPUT -p tcp --dport 161 -j DSCP --set-dscp 16 # SNMP
iptables -t mangle -A OUTPUT -p tcp --sport 161 -j DSCP --set-dscp 16 # SNMP
iptables -t mangle -A OUTPUT -p tcp --dport 162 -j DSCP --set-dscp 16 # SNPMTRAP
iptables -t mangle -A OUTPUT -p tcp --sport 162 -j DSCP --set-dscp 16 # SNMPTRAP
iptables -t mangle -A OUTPUT -p udp --dport 514 -j DSCP --set-dscp 16 # Syslog
iptables -t mangle -A OUTPUT -p udp --sport 514 -j DSCP --set-dscp 16 # Syslog
iptables -t mangle -A OUTPUT -p tcp --dport 389 -j DSCP --set-dscp 16 # ldap
iptables -t mangle -A OUTPUT -p tcp --sport 389 -j DSCP --set-dscp 16 # ldap
iptables -t mangle -A OUTPUT -p tcp --dport 636 -j DSCP --set-dscp 16 # ldaps
iptables -t mangle -A OUTPUT -p tcp --sport 636 -j DSCP --set-dscp 16 # ldaps
iptables -t mangle -A OUTPUT -p tcp --dport 1169 -j DSCP --set-dscp 16 # tripwire
iptables -t mangle -A OUTPUT -p tcp --sport 1169 -j DSCP --set-dscp 16 # tripwire
iptables -t mangle -A OUTPUT -p udp --dport 9996 -j DSCP --set-dscp 16 # cflow
iptables -t mangle -A OUTPUT -p udp --sport 9996 -j DSCP --set-dscp 16 # cflow
iptables -t mangle -A OUTPUT -p udp --dport 67 -j DSCP --set-dscp 16 # bootps
iptables -t mangle -A OUTPUT -p udp --sport 67 -j DSCP --set-dscp 16 # bootps
iptables -t mangle -A OUTPUT -p udp --dport 68 -j DSCP --set-dscp 16 # bootpc
iptables -t mangle -A OUTPUT -p udp --sport 68 -j DSCP --set-dscp 16 # bootpc


# Signalling Group (24)
#=============================
# SIP IAX
# Should be set by application, only to ne used when app not capable.
iptables -t mangle -A OUTPUT -d 217.10.79.23 -p udp -m udp --sport 5060 -j DSCP --set-dscp-class cs4 # mark SIPGate SIP UDP packets
#  DSCP 10
iptables -t mangle -A OUTPUT -d 217.10.79.23 -p tcp --sport 5060 -j DSCP --set-dscp-class cs3 # mark SIP TCP packets with CS3

iptables -t mangle -A OUTPUT -p udp -m udp --sport 5070 -j DSCP --set-dscp-class cs3 # mark SIP UDP packets with CS3
iptables -t mangle -A OUTPUT -p tcp --sport 5070 -j DSCP --set-dscp-class cs3 # mark SIP TCP packets with CS3

iptables -t mangle -A OUTPUT -p udp -m udp --sport 5080 -j DSCP --set-dscp-class cs3 # mark SIP UDP packets with CS3
iptables -t mangle -A OUTPUT -p tcp --sport 5080 -j DSCP --set-dscp-class cs3 # mark SIP TCP packets with CS3

iptables -t mangle -A OUTPUT -p udp -m udp --sport 5090 -j DSCP --set-dscp-class cs3 # mark SIP UDP packets with CS3
iptables -t mangle -A OUTPUT -p tcp --sport 5090 -j DSCP --set-dscp-class cs3 # mark SIP TCP packets with CS3

iptables -t mangle -A OUTPUT -p udp -m udp --sport 5061 -j DSCP --set-dscp-class cs3 # mark SIP UDP packets with CS3
iptables -t mangle -A OUTPUT -p tcp --sport 5061 -j DSCP --set-dscp-class cs3 # mark SIP TCP packets with CS3

iptables -t mangle -A OUTPUT -p udp -m udp --sport 4569 -j DSCP --set-dscp-class cs3 # mark IAX2 packets with CS3
iptables -t mangle -A OUTPUT -p udp -m udp --dport 4569 -j DSCP --set-dscp-class cs3 # mark IAX2 packets with CS3
iptables -t mangle -A OUTPUT -p udp -m udp --sport 4570 -j DSCP --set-dscp-class cs3 # mark IAX2 packets with CS3
iptables -t mangle -A OUTPUT -p udp -m udp --dport 4570 -j DSCP --set-dscp-class cs3 # mark IAX2 packets with CS3


# Gaming Group (32)
#=============================
# Battlenet WOW
iptables -t mangle -A OUTPUT -p tcp --dport 1119 -j DSCP --set-dscp 32 # bnetgame
iptables -t mangle -A OUTPUT -p tcp --sport 1119 -j DSCP --set-dscp 32 # bnetgame
iptables -t mangle -A OUTPUT -p udp --dport 1119 -j DSCP --set-dscp 32 # bnetgame
iptables -t mangle -A OUTPUT -p udp --sport 1119 -j DSCP --set-dscp 32 # bnetgame
iptables -t mangle -A OUTPUT -p tcp --dport 1120 -j DSCP --set-dscp 32 # bnetfile
iptables -t mangle -A OUTPUT -p tcp --sport 1120 -j DSCP --set-dscp 32 # bnetfile
iptables -t mangle -A OUTPUT -p udp --dport 1120 -j DSCP --set-dscp 32 # bnetfile
iptables -t mangle -A OUTPUT -p udp --sport 1120 -j DSCP --set-dscp 32 # bnetfile


# High Throughput Data Group (10,12,14)
#=============================
# ftp nfs tftp smb scp bittorrent pop2/3 smtp imap
iptables -t mangle -A OUTPUT -p tcp --dport 20 -j DSCP --set-dscp 10 # ftp-data
iptables -t mangle -A OUTPUT -p tcp --sport 20 -j DSCP --set-dscp 10 # ftp-data
iptables -t mangle -A OUTPUT -p udp --dport 20 -j DSCP --set-dscp 10 # ftp-data
iptables -t mangle -A OUTPUT -p udp --sport 20 -j DSCP --set-dscp 10 # ftp-data
iptables -t mangle -A OUTPUT -p tcp --dport 21 -j DSCP --set-dscp 10 # ftp
iptables -t mangle -A OUTPUT -p tcp --sport 21 -j DSCP --set-dscp 10 # ftp
iptables -t mangle -A OUTPUT -p udp --dport 21 -j DSCP --set-dscp 10 # ftp
iptables -t mangle -A OUTPUT -p udp --sport 21 -j DSCP --set-dscp 10 # ftp
iptables -t mangle -A OUTPUT -p tcp --dport 115 -j DSCP --set-dscp 10 # sftp
iptables -t mangle -A OUTPUT -p tcp --sport 115 -j DSCP --set-dscp 10 # sftp
iptables -t mangle -A OUTPUT -p udp --dport 115 -j DSCP --set-dscp 10 # sftp
iptables -t mangle -A OUTPUT -p udp --sport 115 -j DSCP --set-dscp 10 # sftp
iptables -t mangle -A OUTPUT -p tcp --dport 989 -j DSCP --set-dscp 10 # ftps-data
iptables -t mangle -A OUTPUT -p tcp --sport 989 -j DSCP --set-dscp 10 # ftps-data
iptables -t mangle -A OUTPUT -p udp --dport 989 -j DSCP --set-dscp 10 # ftps-data
iptables -t mangle -A OUTPUT -p udp --sport 989 -j DSCP --set-dscp 10 # ftps-data
iptables -t mangle -A OUTPUT -p tcp --dport 990 -j DSCP --set-dscp 10 # ftps
iptables -t mangle -A OUTPUT -p tcp --sport 990 -j DSCP --set-dscp 10 # ftps
iptables -t mangle -A OUTPUT -p udp --dport 990 -j DSCP --set-dscp 10 # ftps
iptables -t mangle -A OUTPUT -p udp --sport 990 -j DSCP --set-dscp 10 # ftps
iptables -t mangle -A OUTPUT -p tcp --dport 119 -j DSCP --set-dscp 10 # nntp
iptables -t mangle -A OUTPUT -p tcp --sport 119 -j DSCP --set-dscp 10 # nntp
iptables -t mangle -A OUTPUT -p udp --dport 119 -j DSCP --set-dscp 10 # nntp
iptables -t mangle -A OUTPUT -p udp --sport 119 -j DSCP --set-dscp 10 # nntp
iptables -t mangle -A OUTPUT -p tcp --dport 563 -j DSCP --set-dscp 10 # nntps
iptables -t mangle -A OUTPUT -p tcp --sport 563 -j DSCP --set-dscp 10 # nntps
iptables -t mangle -A OUTPUT -p udp --dport 563 -j DSCP --set-dscp 10 # nntps
iptables -t mangle -A OUTPUT -p udp --sport 563 -j DSCP --set-dscp 10 # nntps
iptables -t mangle -A OUTPUT -p tcp --dport 137 -j DSCP --set-dscp 10 # smb netbios-ns
iptables -t mangle -A OUTPUT -p tcp --sport 137 -j DSCP --set-dscp 10 # smb netbios-ns
iptables -t mangle -A OUTPUT -p udp --dport 137 -j DSCP --set-dscp 10 # smb netbios-ns
iptables -t mangle -A OUTPUT -p udp --sport 137 -j DSCP --set-dscp 10 # smb netbios-ns
iptables -t mangle -A OUTPUT -p tcp --dport 138 -j DSCP --set-dscp 10 # smb netbios-dgm
iptables -t mangle -A OUTPUT -p tcp --sport 138 -j DSCP --set-dscp 10 # smb netbios-dgm
iptables -t mangle -A OUTPUT -p udp --dport 138 -j DSCP --set-dscp 10 # smb netbios-dgm
iptables -t mangle -A OUTPUT -p udp --sport 138 -j DSCP --set-dscp 10 # smb netbios-dgm
iptables -t mangle -A OUTPUT -p tcp --dport 139 -j DSCP --set-dscp 10 # smb netbios-ssn
iptables -t mangle -A OUTPUT -p tcp --sport 139 -j DSCP --set-dscp 10 # smb netbios-ssn
iptables -t mangle -A OUTPUT -p udp --dport 139 -j DSCP --set-dscp 10 # smb netbios-ssn
iptables -t mangle -A OUTPUT -p udp --sport 139 -j DSCP --set-dscp 10 # smb netbios-ssn
iptables -t mangle -A OUTPUT -p tcp --dport 445 -j DSCP --set-dscp 10 # smb Microsoft-DS
iptables -t mangle -A OUTPUT -p tcp --sport 445 -j DSCP --set-dscp 10 # smb Microsoft-DS
iptables -t mangle -A OUTPUT -p udp --dport 445 -j DSCP --set-dscp 10 # smb Microsoft-DS
iptables -t mangle -A OUTPUT -p udp --sport 445 -j DSCP --set-dscp 10 # smb Microsoft-DS
iptables -t mangle -A OUTPUT -p tcp --dport 873 -j DSCP --set-dscp 10 # rsync
iptables -t mangle -A OUTPUT -p tcp --sport 873 -j DSCP --set-dscp 10 # rsync
iptables -t mangle -A OUTPUT -p udp --dport 873 -j DSCP --set-dscp 10 # rsync
iptables -t mangle -A OUTPUT -p udp --sport 873 -j DSCP --set-dscp 10 # rsync

iptables -t mangle -A OUTPUT -p tcp --dport 69 -j DSCP --set-dscp 11 # tftp
iptables -t mangle -A OUTPUT -p tcp --sport 69 -j DSCP --set-dscp 11 # tftp
iptables -t mangle -A OUTPUT -p udp --dport 69 -j DSCP --set-dscp 11 # tftp
iptables -t mangle -A OUTPUT -p udp --sport 69 -j DSCP --set-dscp 11 # tftp
iptables -t mangle -A OUTPUT -p tcp --dport 860 -j DSCP --set-dscp 11 # iSCSI
iptables -t mangle -A OUTPUT -p tcp --sport 860 -j DSCP --set-dscp 11 # iSCSI
iptables -t mangle -A OUTPUT -p udp --dport 860 -j DSCP --set-dscp 11 # iSCSI
iptables -t mangle -A OUTPUT -p udp --sport 860 -j DSCP --set-dscp 11 # iSCSI
iptables -t mangle -A OUTPUT -p tcp --dport 2049 -j DSCP --set-dscp 11 # NFS
iptables -t mangle -A OUTPUT -p tcp --sport 2049 -j DSCP --set-dscp 11 # NFS
iptables -t mangle -A OUTPUT -p udp --dport 2049 -j DSCP --set-dscp 11 # NFS
iptables -t mangle -A OUTPUT -p udp --sport 2049 -j DSCP --set-dscp 11 # NFS

iptables -t mangle -A OUTPUT -p tcp --dport 25 -j DSCP --set-dscp 12 # smtp
iptables -t mangle -A OUTPUT -p tcp --sport 25 -j DSCP --set-dscp 12 # smtp
iptables -t mangle -A OUTPUT -p udp --dport 25 -j DSCP --set-dscp 12 # smtp
iptables -t mangle -A OUTPUT -p udp --sport 25 -j DSCP --set-dscp 12 # smtp
iptables -t mangle -A OUTPUT -p tcp --dport 109 -j DSCP --set-dscp 12 # pop2
iptables -t mangle -A OUTPUT -p tcp --sport 109 -j DSCP --set-dscp 12 # pop2
iptables -t mangle -A OUTPUT -p udp --dport 109 -j DSCP --set-dscp 12 # pop2
iptables -t mangle -A OUTPUT -p udp --sport 109 -j DSCP --set-dscp 12 # pop2
iptables -t mangle -A OUTPUT -p tcp --dport 110 -j DSCP --set-dscp 12 # pop3
iptables -t mangle -A OUTPUT -p tcp --sport 110 -j DSCP --set-dscp 12 # pop3
iptables -t mangle -A OUTPUT -p udp --dport 110 -j DSCP --set-dscp 12 # pop3
iptables -t mangle -A OUTPUT -p udp --sport 110 -j DSCP --set-dscp 12 # pop3
iptables -t mangle -A OUTPUT -p tcp --dport 995 -j DSCP --set-dscp 12 # pop3s
iptables -t mangle -A OUTPUT -p tcp --sport 995 -j DSCP --set-dscp 12 # pop3s
iptables -t mangle -A OUTPUT -p udp --dport 995 -j DSCP --set-dscp 12 # pop3s
iptables -t mangle -A OUTPUT -p udp --sport 995 -j DSCP --set-dscp 12 # pop3s
iptables -t mangle -A OUTPUT -p tcp --dport 143 -j DSCP --set-dscp 12 # imap
iptables -t mangle -A OUTPUT -p tcp --sport 143 -j DSCP --set-dscp 12 # imap
iptables -t mangle -A OUTPUT -p udp --dport 143 -j DSCP --set-dscp 12 # imap
iptables -t mangle -A OUTPUT -p udp --sport 143 -j DSCP --set-dscp 12 # imap
iptables -t mangle -A OUTPUT -p tcp --dport 993 -j DSCP --set-dscp 12 # imaps
iptables -t mangle -A OUTPUT -p tcp --sport 993 -j DSCP --set-dscp 12 # imaps
iptables -t mangle -A OUTPUT -p udp --dport 993 -j DSCP --set-dscp 12 # imaps
iptables -t mangle -A OUTPUT -p udp --sport 993 -j DSCP --set-dscp 12 # imaps



# Streaming Video and content (26,28,30)
#=============================
# upnp traffic
iptables -t mangle -A OUTPUT -p tcp --dport 49152 -j DSCP --set-dscp 30 # upnp AV
iptables -t mangle -A OUTPUT -p tcp --sport 49152 -j DSCP --set-dscp 30 # upnp AV
iptables -t mangle -A OUTPUT -p udp --dport 49152 -j DSCP --set-dscp 30 # upnp AV
iptables -t mangle -A OUTPUT -p udp --sport 49152 -j DSCP --set-dscp 30 # upnp AV
iptables -t mangle -A OUTPUT -p tcp --dport 458 -j DSCP --set-dscp 30 # Apple quick time
iptables -t mangle -A OUTPUT -p tcp --sport 458 -j DSCP --set-dscp 30 # Apple quick time
iptables -t mangle -A OUTPUT -p udp --dport 458 -j DSCP --set-dscp 30 # Apple quick time
iptables -t mangle -A OUTPUT -p udp --sport 458 -j DSCP --set-dscp 30 # Apple quick time
iptables -t mangle -A OUTPUT -p tcp --dport 554 -j DSCP --set-dscp 30 # RTSP
iptables -t mangle -A OUTPUT -p tcp --sport 554 -j DSCP --set-dscp 30 # RTSP
iptables -t mangle -A OUTPUT -p udp --dport 554 -j DSCP --set-dscp 30 # RTSP
iptables -t mangle -A OUTPUT -p udp --sport 554 -j DSCP --set-dscp 30 # RTSP

# Client Server Transactions (18,20,22)
#=============================
# mysql dns http https ntp
iptables -t mangle -A OUTPUT -p tcp --dport 80 -j DSCP --set-dscp 18 # http
iptables -t mangle -A OUTPUT -p tcp --sport 80 -j DSCP --set-dscp 18 # http
iptables -t mangle -A OUTPUT -p tcp --dport 81 -j DSCP --set-dscp 18 # http
iptables -t mangle -A OUTPUT -p tcp --sport 81 -j DSCP --set-dscp 18 # http
iptables -t mangle -A OUTPUT -p tcp --dport 8080 -j DSCP --set-dscp 18 # http
iptables -t mangle -A OUTPUT -p tcp --sport 8080 -j DSCP --set-dscp 18 # http

iptables -t mangle -A OUTPUT -p tcp --dport 443 -j DSCP --set-dscp 18 # https
iptables -t mangle -A OUTPUT -p tcp --sport 443 -j DSCP --set-dscp 18 # https

iptables -t mangle -A OUTPUT -p tcp --dport 1194 -j DSCP --set-dscp 18 # openvpn
iptables -t mangle -A OUTPUT -p tcp --sport 1194 -j DSCP --set-dscp 18 # openvpn
iptables -t mangle -A OUTPUT -p udp --dport 1194 -j DSCP --set-dscp 18 # openvpn
iptables -t mangle -A OUTPUT -p udp --sport 1194 -j DSCP --set-dscp 18 # openvpn

iptables -t mangle -A OUTPUT -p tcp --dport 3306 -j DSCP --set-dscp 20 # mysql
iptables -t mangle -A OUTPUT -p tcp --sport 3306 -j DSCP --set-dscp 20 # mysql
iptables -t mangle -A OUTPUT -p tcp --dport 123 -j DSCP --set-dscp 20 # ntp
iptables -t mangle -A OUTPUT -p tcp --sport 123 -j DSCP --set-dscp 20 # ntp
iptables -t mangle -A OUTPUT -p udp --dport 123 -j DSCP --set-dscp 20 # ntp
iptables -t mangle -A OUTPUT -p udp --sport 123 -j DSCP --set-dscp 20 # ntp

iptables -t mangle -A OUTPUT -p tcp --dport 53 -j DSCP --set-dscp 22 # DNS
iptables -t mangle -A OUTPUT -p tcp --sport 53 -j DSCP --set-dscp 22 # DNS
iptables -t mangle -A OUTPUT -p udp --dport 53 -j DSCP --set-dscp 22 # DNS
iptables -t mangle -A OUTPUT -p udp --sport 53 -j DSCP --set-dscp 22 # DNS
iptables -t mangle -A OUTPUT -p tcp --dport 953 -j DSCP --set-dscp 22 # DNS-xfer
iptables -t mangle -A OUTPUT -p tcp --sport 953 -j DSCP --set-dscp 22 # DNS-xfer
iptables -t mangle -A OUTPUT -p udp --dport 953 -j DSCP --set-dscp 22 # DNS-xfer
iptables -t mangle -A OUTPUT -p udp --sport 953 -j DSCP --set-dscp 22 # DNS-xfer

iptables -t mangle -A OUTPUT -p tcp --dport 1186 -j DSCP --set-dscp 22 # mysql-cluster
iptables -t mangle -A OUTPUT -p tcp --sport 1186 -j DSCP --set-dscp 22 # mysql-cluster
iptables -t mangle -A OUTPUT -p udp --dport 1186 -j DSCP --set-dscp 22 # mysql-cluster
iptables -t mangle -A OUTPUT -p udp --sport 1186 -j DSCP --set-dscp 22 # mysql-cluster

iptables -t mangle -A OUTPUT -p tcp --dport 546 -j DSCP --set-dscp 22 # dhcpv6-client
iptables -t mangle -A OUTPUT -p tcp --sport 546 -j DSCP --set-dscp 22 # dhcpv6-client
iptables -t mangle -A OUTPUT -p udp --dport 546 -j DSCP --set-dscp 22 # dhcpv6-client
iptables -t mangle -A OUTPUT -p udp --sport 546 -j DSCP --set-dscp 22 # dhcpv6-client
iptables -t mangle -A OUTPUT -p tcp --dport 547 -j DSCP --set-dscp 22 # dhcpv6-server
iptables -t mangle -A OUTPUT -p tcp --sport 547 -j DSCP --set-dscp 22 # dhcpv6-server
iptables -t mangle -A OUTPUT -p udp --dport 547 -j DSCP --set-dscp 22 # dhcpv6-server
iptables -t mangle -A OUTPUT -p udp --sport 547 -j DSCP --set-dscp 22 # dhcpv6-server

# Video Conferencing (32,34,36)
#=============================
# set by applications such as asterisk
# part of RTP so needs session awareness


# Best Efforts (0)
#=============================
# all traffic will by default end up in this group


# Scavenger Group (8)
#=============================
# well known virus and trojans ports
iptables -t mangle -A OUTPUT -p tcp --dport 1214 -j DSCP --set-dscp 8 # KAZAA
iptables -t mangle -A OUTPUT -p tcp --sport 1214 -j DSCP --set-dscp 8 # KAZAA
iptables -t mangle -A OUTPUT -p udp --dport 1214 -j DSCP --set-dscp 8 # KAZAA
iptables -t mangle -A OUTPUT -p udp --sport 1214 -j DSCP --set-dscp 8 # KAZAA

iptables -t mangle -A OUTPUT -p tcp --dport 47451 -j DSCP --set-dscp 8 # Torrent
iptables -t mangle -A OUTPUT -p tcp --sport 47451 -j DSCP --set-dscp 8 # Torrent
iptables -t mangle -A OUTPUT -p udp --dport 47451 -j DSCP --set-dscp 8 # Torrent
iptables -t mangle -A OUTPUT -p udp --sport 47451 -j DSCP --set-dscp 8 # Torrent

iptables -t mangle -A OUTPUT -p tcp --dport 57451 -j DSCP --set-dscp 8 # Torrent
iptables -t mangle -A OUTPUT -p tcp --sport 57451 -j DSCP --set-dscp 8 # Torrent
iptables -t mangle -A OUTPUT -p udp --dport 57451 -j DSCP --set-dscp 8 # Torrent
iptables -t mangle -A OUTPUT -p udp --sport 57451 -j DSCP --set-dscp 8 # Torrent

# commit the changes to file
iptables-save > /etc/iptables.rules

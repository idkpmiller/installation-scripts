#!/bin/bash
# Por Jarlley Ribeiro @0fx66
echo " [x] Downloading Hydra..."
wget http://www.thc.org/releases/hydra-7.3-src.tar.gz 1> /dev/null 2> /dev/stdout

echo " [x] Unpacking files..."
tar -xvzf hydra-7.2-src.tar.gz 1> /dev/null 2> /dev/stdout
cd hydra-7.2-src 1> /dev/null 2> /dev/stdout

echo " [x] Starting the instalation..."
./configure 1> /dev/null 2> /dev/stdout
make 1> /dev/null 2> /dev/stdout
make install 1> /dev/null 2> /dev/stdout

echo "Run in the terminal: hydra --help for more options..."

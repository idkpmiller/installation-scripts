#!/bin/bash

#	Script name:			addAS_Stage1_Confirm_Environment.sh
#	Version:		     	1.0
#	Original Author:    	Paul Miller
#	Maintainer:				Spark Connect
#	Purpose:		    	Captures MOP Stage 1 Confirm Environment checks
# 	BroadWorks version:		R19.0 
#  	Environment:       		BroadWorks; Any
#  	Runas:					root (sudo)
#  	Inputs:					none
# 	Output:					capture terminal session
#    
#    Revision history:
#      1.0    15 Jan 2015   Released for MOP version 3.3                           
# establish text format options
bold=`tput bold`
normal=`tput sgr0`

BOOTUP=color
RES_COL=64
MOVE_TO_COL="echo -en \\033[${RES_COL}G"
SETCOLOR_SUCCESS="echo -en \\033[1;32m"
SETCOLOR_FAILURE="echo -en \\033[1;31m"
SETCOLOR_WARNING="echo -en \\033[1;33m"
SETCOLOR_NORMAL="echo -en \\033[0;39m"

#---------------------
# Functions
#---------------------
func_chkRoot() {
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1

}

echo_success() {
  [ "$BOOTUP" = "color" ] && $MOVE_TO_COL
  echo -n "["
  [ "$BOOTUP" = "color" ] && $SETCOLOR_SUCCESS
  echo -n $"  OK  "
  [ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 0
}

echo_warn() {
  [ "$BOOTUP" = "color" ] && $MOVE_TO_COL
  echo -n "["
  [ "$BOOTUP" = "color" ] && $SETCOLOR_FAILURE
  echo -n $" WARN "
  [ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 1
}

echo_failure() {
  [ "$BOOTUP" = "color" ] && $MOVE_TO_COL
  echo -n "["
  [ "$BOOTUP" = "color" ] && $SETCOLOR_FAILURE
  echo -n $"FAILED"
  [ "$BOOTUP" = "color" ] && $SETCOLOR_NORMAL
  echo -n "]"
  echo -ne "\n"
  return 1
}

sh_package () {
PACKAGE="${1}"
echo -n "$PACKAGE"
if rpm -q $PACKAGE &> /dev/null; then
  echo_success
else
  echo_failure
fi
}

cmd(){
	echo ">>> $*"
	$*
	returnValue=$?
	echo
	
	return $returnValue
}

heading(){
echo '+---------------------------------------------------------------------+'
echo "|  $*"
echo '+---------------------------------------------------------------------+'
echo ""
}

do_ssh () {
HOST=$1
USER=$2
PASS=$3
CMD="hostname"
echo -n "SSH using $USER@$HOST"

/usr/bin/expect > ssh.$HOST.log <<EOF
set timeout 20
spawn ssh $USER@$HOST $CMD 2>/dev/null

expect "yes/no" {
    send "yes\r"
    expect "*?assword" { send "$PASS\r" }
    } "*?assword" { send "$PASS\r" }
expect -re {[#$] }

#expect -re {[#$] } { send "hostname\r" }
#expect "$ " { send "hostname\r" }
#interact
#expect -re {[#$] } { send "exit\n\r" }
#expect "$ " { send "exit\n\r" }

EOF
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
else
  # check hostname returned is an actual hostname bu checking in the hosts file
  RMTHOST=$(tail -n1 "ssh.$HOST.log")
  #clean up - leave only alphanumeric characters
  RMTHOST=${RMTHOST//[!A-Z0-9a-z]/}
  grep ${RMTHOST} "/etc/hosts" >/dev/null
  rc=$?
  if [[ $rc == 0 ]] ; then
    echo_success
  else
    echo_failure
  fi
fi
}

rmt_ssh () {
HOST=$1
USER=$2
PASS="$3"
CMD="$4"

/usr/bin/expect > ssh.$HOST.log <<EOF
set timeout 20
spawn ssh $USER@$HOST ${CMD} 2>/dev/null

expect "yes/no" {
    send "yes\r"
    expect "*?assword: " { send "$PASS\r" }
    } "*?assword: " { send "$PASS\r" }

expect -re {[#$] } { send "$CMD 2>/dev/null\r" }
expect -re {[#$] }

EOF
rc=$?
return $rc
}

rmt_ssh_su () {
HOST=$1
USER=$2
PASS="$3"
CMD="$4"
SUPASS="$5"

/usr/bin/expect > ssh.$HOST.SU.log <<EOF
set timeout 20
spawn ssh $USER@$HOST

expect "yes/no" {
    send "yes\r"
    expect "*?assword" { send "$PASS\r" }
    } "*?assword" { send "$PASS\r" }

expect -re {[#$] } { send "su -\r" }
expect ": " { send "$SUPASS\r" }
expect -re {[#$] } { send "$CMD 2>/dev/null\r" }
expect -re {[#$] }
EOF
rc=$?
return $rc
}

IPToOctets () {
    local ip_address=$(echo $1 | awk -F/ {'print $1'})
    OLDIFS="$IFS"
    IFS=.
    set $ip_address
    local octet1=$1
    local octet2=$2
    local octet3=$3
    local octet4=$4
    IFS="$OLDIFS"
    echo $octet1 $octet2 $octet3 $octet4
}

NetworkAddress () {
    local ip_address="$1"
    local cidr=$(echo $ip_address | awk -F/ {'print $2'})
    if [ -z "$cidr" ]
    then
        local subnetmask=${2:-255.255.255.255}
    else
        local subnetmask=$(CIDRToSubnetMask $cidr)
    fi
    local octetip=$(IPToOctets $ip_address)
    local octetsn=$(IPToOctets $subnetmask)

    local octetip1=$(echo $octetip | awk {'print $1'})
    local octetip2=$(echo $octetip | awk {'print $2'})
    local octetip3=$(echo $octetip | awk {'print $3'})
    local octetip4=$(echo $octetip | awk {'print $4'})

    local octetsn1=$(echo $octetsn | awk {'print $1'})
    local octetsn2=$(echo $octetsn | awk {'print $2'})
    local octetsn3=$(echo $octetsn | awk {'print $3'})
    local octetsn4=$(echo $octetsn | awk {'print $4'})

    local netaddress=$(($octetip1 & $octetsn1)).$(($octetip2 & $octetsn2)).$(($octetip3 & $octetsn3)).$(($octetip4 & $octetsn4))
    echo $netaddress
}

BroadcastAddress () {
    local ip_address="$1"
    local cidr=$(echo $ip_address | awk -F/ {'print $2'})
    if [ -z "$cidr" ]
    then
        local subnetmask=${2:-255.255.255.255}
    else
        local subnetmask=$(CIDRToSubnetMask $cidr)
    fi
   
    local octetip=$(IPToOctets $ip_address)
    local octetsn=$(IPToOctets $subnetmask)
    local octetip1=$(echo $octetip | awk {'print $1'})
    local octetip2=$(echo $octetip | awk {'print $2'})
    local octetip3=$(echo $octetip | awk {'print $3'})
    local octetip4=$(echo $octetip | awk {'print $4'})

    local octetsn1=$(echo $octetsn | awk {'print $1'})
    local octetsn2=$(echo $octetsn | awk {'print $2'})
    local octetsn3=$(echo $octetsn | awk {'print $3'})
    local octetsn4=$(echo $octetsn | awk {'print $4'})

    local bcaddress=$(( 255 - $octetsn1 + ($octetip1 & $octetsn1))).$(( 255 - $octetsn2 + ($octetip2 & $octetsn2))).$(( 255 - $octetsn3 + ($octetip3 & $octetsn3))).$(( 255 - $octetsn4 + ($octetip4 & $octetsn4)))
    echo $bcaddress
}

IPInRange () {
    local ip_addresses=$(GetIPAddress $1)
    local rangeip_address=$(GetIPAddress $2 1)
    local range_cidr=$(echo $rangeip_address | awk -F/ {'print $2'})
    if [ -z "$range_cidr" ]
    then
        local subnetmask=${3:-255.255.255.255}
    else
        local subnetmask=$(CIDRToSubnetMask $range_cidr)
    fi
    local net_address=$(NetworkAddress $rangeip_address $subnetmask)
    local octetnet=$(IPToOctets $net_address)
    local bcast_address=$(BroadcastAddress $rangeip_address $subnetmask)
    local octetbcast=$(IPToOctets $bcast_address)

    local octetnet1=$(echo $octetnet | awk {'print $1'})
    local octetnet2=$(echo $octetnet | awk {'print $2'})
    local octetnet3=$(echo $octetnet | awk {'print $3'})
    local octetnet4=$(echo $octetnet | awk {'print $4'})

    local octetbcast1=$(echo $octetbcast | awk {'print $1'})
    local octetbcast2=$(echo $octetbcast | awk {'print $2'})
    local octetbcast3=$(echo $octetbcast | awk {'print $3'})
    local octetbcast4=$(echo $octetbcast | awk {'print $4'})

    for ip_address in $ip_addresses
    do
        octetip=$(IPToOctets $ip_address)

        local octetip1=$(echo $octetip | awk {'print $1'})
        local octetip2=$(echo $octetip | awk {'print $2'})
        local octetip3=$(echo $octetip | awk {'print $3'})
        local octetip4=$(echo $octetip | awk {'print $4'})

        if [[ "$octetip1" -ge "$octetnet1" ]] && [[ "$octetip1" -le "$octetbcast1" ]] &&
            [[ "$octetip2" -ge "$octetnet2" ]] && [[ "$octetip2" -le "$octetbcast2" ]] &&
            [[ "$octetip3" -ge "$octetnet3" ]] && [[ "$octetip3" -le "$octetbcast3" ]] &&
            [[ "$octetip4" -ge "$octetnet4" ]] && [[ "$octetip4" -le "$octetbcast4" ]]
        then
            return 0
        fi
    done
    return 1
}

GetIPAddress () {
    local address=$1
    local qty=$2
    local lookup_address=$(host $address | grep 'has address' | awk {'print $NF'})
    local xadd
    local count=0
    for xadd in $lookup_address
    do
        ((count++))
        [[ -z "$qty" || $count -le "$qty" ]] && xaddress+=\ $xadd
	done

    [ -z "$xaddress" ] && echo $address || echo $xaddress
	
}

CIDRToSubnetMask () {
    local i
    local subnetmask=""
    local full_octets=$(($1/8))
    local partial_octet=$(($1%8))
    for ((i=0;i<4;i+=1)); do
        if [ $i -lt $full_octets ]; then
            subnetmask+=255
        elif [ $i -eq $full_octets ]; then
            subnetmask+=$((256 - 2**(8-$partial_octet)))
        else
            subnetmask+=0
        fi   
        [ $i -lt 3 ] && subnetmask+=.
    done

    echo $subnetmask
}

#---------------------
# Sections
#---------------------

function get_UUID () {
heading "Collect UUID"
echo -n "Collect UUID of host: "
hostname;dmidecode | grep UUID >/dev/null
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
  exit $rc
else
  echo_success
  cmd hostname;dmidecode | grep UUID | sed "s/^/     /" 
fi
}

function chk_passwd_exp () {
USER=root
heading "Check bwadmin account password expiry"
PW_EXP=$(chage -l $USER | grep 'Password expires'| awk '{ print $4}')
MAX_DAYS_EXP=$(chage -l $USER | grep 'Maximum number of days between password change'| awk '{ print $9}')

echo -n "Check Passwords Never Expires [$PW_EXP]"
if [[ $PW_EXP = "never" ]]; then
  echo_success
else
  echo_failure
fi

echo -n "Check Max. Days (99999/-1) [$MAX_DAYS_EXP]"
if [ $MAX_DAYS_EXP -eq 99999 ] || [ $MAX_DAYS_EXP -eq -1 ]; then
  echo_success
else
  echo_failure
fi

#chage -l bwadmin
#Last password change                                    : Aug 07, 2013
#Password expires                                        : never
#Password inactive                                       : never
#Account expires                                         : never
#Minimum number of days between password change          : 0
#Maximum number of days between password change          : 99999
#Number of days of warning before password expires       : 7
}

function chk_host () {
heading "Check Host file"

echo -n "Check localhost.localdomain entry is removed"
if grep -v '^#' "/etc/hosts" | grep 'localhost.localdomain' >/dev/null ; then
  echo_failure
  echo -n "     "
  grep -v '^#' "/etc/hosts" | grep 'localhost.localdomain'
  echo ""
else
  echo_success
fi

echo -n "Check loghost entry is present"
if cat /etc/hosts | grep -v '^#' | grep 'loghost' >/dev/null ; then
  echo_success
  echo -n "     "
  cmd cat /etc/hosts | grep 'loghost'
  echo ""
else
  echo_failure
fi
}

function chk_resolv () {
DOMAIN=sgnl.telecom.co.nz
heading "Check DNS Resolver"

echo -n "Check domain entry is correct"
if grep -v '^#' "/etc/resolv.conf" | grep 'domain '$DOMAIN >/dev/null ; then
  echo_success
else
  echo_failure
  echo "     failure if transition to the preferred search directive is set correctly."
fi

echo -n "Retrieve nameserver entries"
if grep -v '^#' "/etc/resolv.conf" | grep 'nameserver' >/dev/null ; then
  echo_success
  echo "${bold}     Current Server ${normal}"
  grep -v '^#' "/etc/resolv.conf" | grep 'nameserver' | sed "s/^/     /"
  echo "
${bold}     Development Model ${normal}
     nameserver 10.207.44.138 #vg3905.sgnl.telecom.co.nz
     nameserver 10.207.44.200 #vg3914.sgnl.telecom.co.nz

${bold}     Staging Model ${normal}
     nameserver 10.173.29.10   # hp4660
     nameserver 10.173.29.74   # hp4706
	 
${bold}     Production ${normal}
     nameserver 122.56.252.20 #hp3928.sgnl.telecom.co.nz
     nameserver 122.56.252.50 #hp3936.sgnl.telecom.co.nz
     nameserver 122.26.252.150 #hp4990.sgnl.telecom.co.nz
  
  "
  echo ${bold}"     Check the entries are correct as per design."${normal}
  else
  echo_failure
  cmd cat /etc/resolv.conf | sed "s/^/     /"
fi

echo -n "Check options are correct"
if grep -v '^#' "/etc/resolv.conf" | grep 'options timeout:1 attempts:2' >/dev/null ; then
  echo_success
else
  echo_failure
fi

}

function chk_static_routes () {
heading "Check Static routes "

echo -n "Check for bond1 routes"
if route -n | grep bond1 >/dev/null ; then
  echo_success
  cmd cat /etc/sysconfig/network-scripts/route-bond0 | sed "s/^/     /"
  echo "     Check routes with the BroadWorks Platform - Core IP Services and Connectivity Design document."
else
  echo_failure 
  echo "     Development model assumed."
  echo -n "Check for bond0 routes"
  if route -n | grep bond0 >/dev/null ; then
    echo_success
	cmd cat /etc/sysconfig/network-scripts/route-bond0 | sed "s/^/     /"
    echo "     Check routes with the BroadWorks Platform - Core IP Services and"
    echo "     Connectivity Design document."	
  else
    echo_failure
    echo "     Bonding / static route issue report it to Unix team"	
  fi
fi
}

function chk_hostname () {
heading "Check HOSTNAME parameter "

aHOSTNAME=$(grep 'HOSTNAME=' "/etc/sysconfig/network")
bHOSTNAME=$(hostname -s)

echo -n "Check hostname matches [$bHOSTNAME]"
if [[ $aHOSTNAME == HOSTNAME=$bHOSTNAME ]]; then
  echo_success
  cmd cat /etc/sysconfig/network | sed "s/^/     /"
else
  echo_failure
  echo "     HOSTNAME in /etc/sysconfig/network = $aHOSTNAME"
  echo "     HOSTNAME short >hostname -s = $bHOSTNAME"
  fi

}

function chk_symlink () {
heading "Check Symlink"
# Non environment specic symlinks
for LINK in "/usr/local"
do
echo -n "Check symlink - $LINK"
if [ -L $LINK ]; then
  echo_success
  cmd readlink $LINK | sed "s/^/     /"
else
  echo_failure
fi
done


# check if libssl workaround required
echo -n "Add workaround for libssl.so.1.0.1e"
if [ -e "/usr/lib64/libssl.so.1.0.1e" ]; then
  ln -s /usr/lib64/libssl.so.1.0.1e /lib64/libssl.so.6 2>/dev/null
  echo_success
  cmd readlink /lib64/libssl.so.6 | sed "s/^/     /"
else
  echo_warn
  echo "     symbolic link for libssl.so.6 not set"
fi

# check if libcrypto workaround required
echo -n "Add workaround for libcrypto.so.1.0.1e"
if [ -e "/usr/lib64/libcrypto.so.1.0.1e" ]; then
  ln -s /usr/lib64/libcrypto.so.1.0.1e /lib64/libcrypto.so.6 2>/dev/null
  echo_success
  cmd  readlink /lib64/libcrypto.so.6 | sed "s/^/     /"
else
  echo_warn
  echo "     symbolic link for libcrypto.so.6 not set"i
fi
  
# Environment specic symlinks
LINK="/var/broadworks"

echo -n "Check symlink - $LINK"
if [ -L $LINK ]; then
  echo_success
  cmd ls -l $LINK | sed "s/^/     /"
else
  echo_warn
  echo "     N.B /bw may be symlinked to /app/Broadworks depending on the environment"  
  echo "     /var/broadworks -> /bw/broadworks" 
fi

}

function chk_telecom_dir () {
heading "Telecom directory"
LINK="/bw/Telecom"

echo -n "Check $LINK directory exists"
if [ -d "$LINK" ]; then
  echo_success
else
  echo_failure
fi

echo -n "Check symlink - $LINK"
if ls -lai /var/Telecom | grep '/bw/Telecom' >/dev/null ; then
  echo_success
  cmd readlink /var/Telecom | sed "s/^/     /"
else
  echo_failure
fi

echo -n "set Ownership (bwadmin) - $LINK"
cd /var/Telecom/
chown bwadmin:bwadmin *
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
else
  echo_success
fi

echo -n "set permissions (755) - $LINK"
cd /var/Telecom/
chmod 775 *
rc=$?
if [[ $rc != 0 ]] ; then
  echo_failure
else
  echo_success
fi
}

function chk_unix_accounts () {
heading "Confirm BroadWorks UNIX accounts"
username=bwadmin

echo -n "Check $username group exists"
if grep $username /etc/group >/dev/null ; then
  echo_success
  cmd grep $username /etc/group | sed "s/^/     /"
else
  echo_failure
fi

echo -n "Check $username user exists"
if grep $username /etc/passwd >/dev/null ; then
  echo_success
  cmd grep $username /etc/passwd | sed "s/^/     /"
else
  echo_failure
fi

echo -n "Check which group $username belongs too"
if id -Gn $username | grep $username >/dev/null; then
    echo_success
	cmd id -Gn $username | sed "s/^/     /"
	else
    echo_failure
fi


echo -n "Check if bworks or ttadmin user exists"
if egrep 'bworks|ttadmin' /etc/passwd >/dev/null; then
	echo_failure
	echo "     Test will fail if BW has been installed."
else
    echo_success
	cmd egrep 'bworks|ttadmin' /etc/passwd | sed "s/^/     /"
fi
}

function chk_rmt_svrs_host () {
heading "All existing server checks - Host Files"
echo ${bold}"Not yet Implemented!"${normal}

}

function chk_rmt_svrs_routes () {
heading "All existing server checks - Static Routes"
echo ${bold}"Not yet Implemented!"${normal}

BOND=99
RMTHOST="10.207.44.140"
RMTCMD="route -n"

echo ""
echo "Enter the tnumber credentials for existing BW servers."
read -p "Login:" RMTUSER
read -s -p "Password:" RMTPASS
echo ""
read -s -p "SU Password:" RMTSUPASS
echo ""

RMTUSER="bwadmin"
RMTPASS="bwadmin"
RMTSUPASS="sparky88"

echo -n "Get the remote routes"
rmt_ssh $RMTHOST $RMTUSER $RMTPASS $RMTCMD
if [[ $? != 0 ]] ; then
  echo_failure
else
  echo_success
  cat ssh.${RMTHOST}.log | sed "s/^/     /"

  echo "Check remote signalling routes for this AS (`hostname -i`)"
  if grep 'bond1' "ssh.${RMTHOST}.log" >/dev/null; then
    BOND=1
  elif grep bond0 "ssh.${RMTHOST}.log" >/dev/null; then
    BOND=0
  else
    echo_failure
    echo "     No suitable Bond i/f found on $RMTHOST, report issue to Unix team"	
  fi
fi




if [ $BOND -eq 0 ] || [ $BOND -eq 1 ] ; then 
  RMTCMD="cat /etc/sysconfig/network-scripts/route-bond$BOND | awk '{ print $1}'"
  echo -n "Retrieve remote network-scripts - bond${BOND} "  
  rmt_ssh_su $RMTHOST $RMTUSER $RMTPASS $RMTCMD $RMTSUPASS
  if [[ $? != 0 ]] ; then
    echo_failure
  else
    echo_success
    ROUTES=$(cat ssh.${RMTHOST}.log | awk '{ print $1}')
    for address in $ROUTES
    do
      echo -n "Check route $address "  
      IPInRange `hostname -i` $address
      if [[ $? != 0 ]] ; then
        echo_failure
      else
        echo_success	  
      fi
    done
  fi
fi
}

function chk_svr_connectivity () {
heading "Inter-server connectivity"

#This test is to test the new Server has SSH connectivity to the existing AS nodes.
# Existing AS IP Information
# Dev Model AS signalling addresses
# 10.207.44.132, 10.207.44.194, 10.207.44.100, 10.207.44.140
# Staging Model AS signalling addresses
# 10.173.29.4, 10.173.29.68
# Production AS signalling addresses
# 122.56.252.6, 122.56.252.35, 122.56.252.148, 122.56.252.18

# define the variable
AS_ENV="NONE"

# Determine environment using other AS addresses and presence in the host file
grep "10.207.44.138" "/etc/hosts"
RESP=$?
if [ $RESP -eq 0 ]; then
  AS_ENV="DEV"
fi 

grep "10.173.29.4" "/etc/hosts"
RESP=$?
if [ $RESP -eq 0 ]; then
  AS_ENV="STAG"
fi 

grep "122.56.252.6" "/etc/hosts"
RESP=$?
if [ $RESP -eq 0 ]; then
  AS_ENV="PROD"
fi 

echo ""
echo "Environment considered to be ${AS_ENV}"
echo "---------------------------------"

echo ""
echo "Enter the tnumber credentials to test the existing AS nodes."
read -p "Login:" USER
read -s -p "Password:" PASS
echo ""

echo "SSH to Existing AS nodes"
case $AS_ENV in
    DEV ) 
		 for SVR in "localhost" "10.207.44.132" "10.207.44.194" "10.207.44.100" "10.207.44.140"
		 do
		   do_ssh $SVR "$USER" "$PASS" 2>/dev/null;
           tail -n 1 ssh.$SVR.log | sed "s/^/     /"
           echo "-----------------------------------------------------------------------";
		 done
	     ;;
    STAG )
		 for SVR in "localhost" "10.173.29.4" "10.173.29.68"
		 do
		   do_ssh $SVR "$USER" "$PASS" 2>/dev/null;
           tail -n 1 ssh.$SVR.log | sed "s/^/     /"
           echo "-----------------------------------------------------------------------";
		 done
	     ;;
    PROD )
		 for SVR in "localhost" "122.56.252.6" "122.56.252.35" "122.56.252.148" "122.56.252.18"
		 do
		   do_ssh $SVR "$USER" "$PASS" 2>/dev/null;
           tail -n 1 ssh.$SVR.log | sed "s/^/     /"
           echo "-----------------------------------------------------------------------";
		 done
	     ;;
	NONE )
	     echo_failure;
	     echo "     Error! Engineering assistance required as environment was unable to be determined.";
	     ;;
esac
echo "Completed $AS_ENV server list".
}

#---------------------
# Main
#---------------------
func_chkRoot
clear
#Based on Method Of Procedure - Add Application Server (R19)
echo "+---------------------------------------------------------------------+"
echo "  `date` HOSTNAME:`hostname`"
echo "+---------------------------------------------------------------------+"
echo
echo
#Stage 1 Tests
get_UUID
chk_passwd_exp
chk_host
chk_resolv
chk_static_routes
chk_hostname
chk_symlink
chk_telecom_dir
chk_unix_accounts
#chk_rmt_svrs_host
#chk_rmt_svrs_routes
chk_svr_connectivity

echo "Completed script.".

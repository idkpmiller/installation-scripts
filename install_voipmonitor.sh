#!/bin/bash

##############################
# by Paul Miller
# installation script for 
# Voipmonitor centos installations.
# Limitations:
# ------------
#
##############################
# Version:
# --------
# 0.1 initial version
#  
##############################

bold=`tput bold`
normal=`tput sgr0`
useConfirm=true

SELF=$(basename $0)
SCRIPT_NAME="$0"
ARGS="$@"
VERSION="0.1"

#---------------------
# Functions
#---------------------
function pTitle () {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as argument to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function confirm() {
MESSAGE="${1}"
    echo ${bold} $MESSAGE ${normal}
	[ "$useConfirm" = true ] && read -p "Proceed? (Enter) - (^C to abort)"
}

function _rmdir () {
if [ -d $1 ];then
        /bin/rm -r $1
        pTitle "Removal of directory $1 - successful"		
  else
        pTitle "Removal of directory $1 - skipped, directory not found"
fi
}

function chkdistro(){
ARCH=$(uname -m)
KERNEL=$(uname -r)
if [ -f /etc/lsb-release ]; then
        OS=$(lsb_release -s -d)
elif [ -f /etc/debian_version ]; then
        OS="Debian $(cat /etc/debian_version)"
elif [ -f /etc/redhat-release ]; then
        OS=`cat /etc/redhat-release`
else
        OS="$(uname -s) $(uname -r)"
fi
# END chkdistro
}

function func_chkRoot() {
echo "================================================================================"
echo "Checking for root Privileges:"
echo "================================================================================"
logger "$0 v${VERSION}, entered OS ENVIRONMENT CHECKS" 
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
}

function func_chkInternet () {
/usr/bin/wget -q --tries=10 --timeout=5 http://www.google.com -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection is functioning"
        /bin/rm /tmp/index.google
fi
}

function func_updSystem() {
echo 'updating system, please Wait.'
echo ""
  apt-get -y -qq update
  
  #Check if user want to upgrade now.
  while true; do
    read -p "Would you like to upgrade the system now? (Y/N)" answer
    case $answer in
        [Yy] ) 
			echo "Upgrading ${bold}please wait..${normal}";
			apt-get -y upgrade;
			apt-get -y autoremove;
			break;;
        [Nn] ) echo "NO, selected so continuing"; break;;
        * ) echo "Please answer Y or N.";;
    esac
done
#func_updSystem
echo "================================================================================"
echo ""    
}

function func_InstVoIPMonitor () {
cd /usr/src/

    case $OS in
        *CentOS* | *Red* | *Debian* )
            wget --content-disposition http://www.voipmonitor.org/current-stable-sniffer-static-32bit.tar.gz
			tar xzf voipmonitor*.tar.gz
			cd voipmonitor*
			./install-script.sh
			if [ ! "$?" = "0" ]; then
				echo "Installation issue" 1>&2
				exit 1
			else
				pTitle "Installation complete"
				echo "edit the configuration at /etc/voipmonitor.conf"
				echo "To start monitoring."
				echo "  /etc/init.d/voipmonitor start"
			fi
            ;;
        *)
            echo "Your OS ( $OS ) was not assessed as one of the supported variants for this script"
            ;;
    esac



# END func_InstVoIPMonitor
} 

#---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
func_chkInternet
func_updSystem
chkdistro

echo $bold
echo "Arch: " $ARCH
echo "OS: " $OS
echo "Kernel: " $KERNEL 
echo $normal

#---------------------
# APPLICATION INSTALL
#---------------------
func_InstVoIPMonitor



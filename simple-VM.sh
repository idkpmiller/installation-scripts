#!/bin/bash
#
# Create New VM Qemu
#by Paul Miller
#Revisions:
# 0.1 - 20/05/2013 Initial Release
# 0.2 - Add hook point between selecting MAIN and running function
# 0.3 - Simplified the approach and bug fixes
###################################################

###################################################
#set default values
###################################################

# Initial Defaults
SELF=$(basename $0)
CONF_FILE=$SELF.conf


# environmental
#SRV_CPU='cat /proc/cpuinfo | grep "physical id" | sort | uniq | wc -l'
#SRV_CORESperCPU='cat /proc/cpuinfo | grep "cpu cores" | wc -l'
#SRV_TOTAL_CORES=$((SRV_CPU * SRV_CORESperCPU))
SRV_TOTAL_RAM="grep MemTotal /proc/meminfo"
SRV_TOTAL_SWAP="grep SwapTotal /proc/meminfo"

function update_Vars  {
# Defaults
HDDISK=$HDPATH$NAME.qcow2
VIRT_CMD="virt-install --NETWORK="${NETWORK}" --NAME="${NAME}" --disk "${HDDISK}",SIZE="${SIZE}" --RAM $RAM -c "$CDROM_IMG" --VCPUs=$VCPU --check-cpu --accelerate --cpuset auto --os-type $OS_TYPE --hvm --soundhw $AUDIO --graphics vnc,port='-1',listen=$HOST_IP,keymap="local""
}

update_Vars

function save_variables() {
# The variables here are saved to the conf file.
# if you add a variable here don't forget to add it
# to the restore function 
cat <<EOF > $CONF_FILE
NAME="$NAME"
RAM=$RAM
CDROM_IMG="$CDROM_IMG"
HDPATH=$HDPATH
HOST_IP=$HOST_IP
NETWORK=$NETWORK
OS_TYPE=$OS_TYPE
VCPU=$VCPU
AUDIO="$AUDIO"
SIZE=$SIZE
EOF

echo "configuration file saved."
}

function restore_variables() {
echo "Attempting to restore configuration..."
if [ -f $CONF_FILE ] ; then
  echo " $CONF_FILE file found and restoring configuration." 
  # restore them
  # ============
  source $CONF_FILE
else
  # initialise the configuration with a set of defaults to resolve the
  # current run issue.
  echo "$CONF_FILE file not found, using Defaults"
  NAME=Default
  RAM=512
  CDROM_IMG="/media/data2/Diskless/Operating systems/Linux distros/Centos/6.4 64bit/CentOS-6.4-x86_64-bin-DVD1.iso"
  HDPATH="/opt/images/"
  HOST_IP=127.0.0.1
  NETWORK="bridge:vmlan0"
  OS_TYPE="linux"
  VCPU=1
  AUDIO="default"
  SIZE=20

  # Next the same variables and values are used to start off the conf file
  save_variables
fi
}

function showInfo {
update_Vars
  whiptail --msgbox " \
Current status
-----------------------------------------------------
NAME: $NAME, RAM: $RAM MB, VCPU: $VCPU, HD SIZE: $SIZE GB, AUDIO: $AUDIO
OS   $OS_TYPE, NETWORK: $NETWORK, VNC IP: $HOST_IP
CDROM_IMG $CDROM_IMG
HDDISK    $HDDISK

The command below will be run if you select create.
 ================================================
$VIRT_CMD
" 24 80 1
}

do_NAME() {
NAME=$(whiptail --inputbox "What is the NAME of ther VM you wish to create?" 8 80 $NAME --title "The NAME mist be unique to this server" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $NAME
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"
}

do_RAM() {
RAM=$(whiptail --inputbox "Enter amount of RAM in MB?" 8 80 $RAM --title "1024 equals 1GB RAM" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $RAM
    
#	if ! [[ "$RAM" =~ ^[0-9]+$ ]] ; then
#    exec >&2; echo "error: Not a number"; do_RAM
	
#	if ! [[ "$RAM" < 64 ]] ; then
#    exec >&2; echo "error: value too small"; do_RAM

#	if ! [[ "$RAM" > $SRV_TOTAL_RAM ]] ; then
#    exec >&2; echo "error: value too large"; do_RAM

else
    echo "User selected Cancel."
fi

echo "Exit status was $exitstatus"
}

do_CPU() {
VCPU=$(whiptail --inputbox "How many CPU's would you like to allocate?" 8 80 $VCPU --title "You have 4 Phycical CPU/Cores" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $VCPU
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"
}

do_CDROM() {
CDROM_IMG=$(whiptail --inputbox "Enter full path and fileNAME including extention of the CD or DVD image file" 8 80 "$CDROM_IMG" --title "The default path is currently $CDROM_IMG" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $CDROM_IMG
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"
}

do_Hard_Drive() {
update_Vars
while true; do
HD=$(whiptail --menu "Create VM Hard drive" 20 80 12 --cancel-button Cancel --ok-button Back \
    "Disk" "Configure the Disk path and fileNAME" \
    "SIZE" "Set the HD SIZE" \
    3>&1 1>&2 2>&3)
	RET=$?
	  if [ $RET -eq 1 ]; then
    do_MAIN
  elif [ $RET -eq 0 ]; then
    "do_$HD" || whiptail --msgbox "There was an error running do_$HD" 20 60 1
  else
    exit 1
  fi
 done
}

do_Disk() {
update_Vars
HDDISK=$(whiptail --inputbox "Enter full path and fileNAME including extention of the hard drive image file" 8 80 $HDDISK --title "The default path is currently $HDPATH" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $HDDISK
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"

}

do_SIZE() {
SIZE=$(whiptail --inputbox "Enter amount of Hard Drive space in GB?" 8 80 $SIZE --title "1024 equals 1TB Disk Space" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $SIZE
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"

}

do_AUDIO() {
AUDIO=$(whiptail --title "AUDIO Hardware" --radiolist \
"Choose the AUDIO Hardware to Emulate" 20 80 6 \
"default" "if the hypervisor supports it, otherwise it will be ES1370" OFF \
"ich6" "Intel Input Output Controller Hub 6" OFF \
"ac97" "Intel AUDIO Labs 1997, AUDIO Codec 97" ON \
"es1370" "Ensoniq AUDIOPCI ES1370" OFF \
"sb16" "Creative Sound Blaster 16 bit Compatible" OFF \
"pcspk" "PC Speaker" OFF \
3>&1 1>&2 2>&3)
}

do_Create() {
update_Vars
if [ -f "$CDROM_IMG" ];
then
   echo "File $CDROM_IMG exists."
else
   echo "The CDROM image specified; $CDROM_IMG does not exist."
   exit 1
fi

 # whiptail --msgbox " \
 # Command below will be run to create your new VM.
 # ================================================
 # " 20 70 1
clear
  echo "Creating VM........"

save_variables

virt-install --connect=qemu:///system \
    --network ${NETWORK} \
    --name "${NAME}" \
    --disk ${HDDISK},SIZE=${SIZE} \
    --ram $RAM \
    --vcpus=$VCPU \
    --check-cpu \
	--cpuset auto \
    --accelerate \
    --hvm \
	--cdrom "$CDROM_IMG" \
	--soundhw ${AUDIO} \
    --graphics vnc,port='-1',listen=$HOST_IP,keymap="local"
RET=$?
exit $RET
 }

do_finish() {
echo "Finished was pressed."
save_variables
  exit 0
}

do_OS_TYPE() {
OS_TYPE=$(whiptail --title "OS Type" --radiolist \
"Choose the OS Type to Install" 20 80 3 \
"linux" "Covers many different linux DISTROs" OFF \
"windows" "Covers the Microsoft Windows OS family" ON \
3>&1 1>&2 2>&3)
}

do_MAIN() {
 while true; do
  FUNC=$(whiptail --menu "Create-VM" 20 80 12 --cancel-button Finish --ok-button Select \
    "showInfo" "Information about this tool" \
    "NAME" "Provide a NAME for the new VM" \
    "RAM" "Set the amiunt of RAM" \
    "CPU" "Set the number of VCPUs for the VM" \
    "CDROM" "Provide the CDROM image path and fileNAME" \
    "Hard_Drive" "Set the SIZE abd fileNAME for the Hard Drive" \
    "AUDIO" "Set the AUDIO HW to emulate" \
    "Create" "The final check and then creation" \
    3>&1 1>&2 2>&3)
  RET=$?
  if [ $RET -eq 1 ]; then
    do_finish
  elif [ $RET -eq 0 ]; then
  #  "do_$FUNC" || whiptail --msgbox "There was an error running do_$FUNC" 20 60 1
  case $FUNC in
        "showInfo")
            echo "you chose choice "${FUNC}""
			"${FUNC}"
            ;;
        "NAME")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
        "RAM")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
		"CPU")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
		"CDROM")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "Hard_Drive")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "OS_TYPE")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "AUDIO")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "Create")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
        *) echo invalid option;;
    esac
  else
    exit 1
  fi
 done
}


#check current user privileges
(( `id -u` )) && echo "This script MUST be ran with root privileges, try prefixing with sudo. i.e sudo $0" && exit 1

# check if Bridged NETWORKing is configured
show_bridge=`brctl show | awk 'NR==2 {print $1}'`
if [ $? -ne 0 ] ; then
	echo "Bridged NETWORKing is not configured, please do so to get an IP similar to your host."
	exit 255
fi 

restore_variables

do_MAIN


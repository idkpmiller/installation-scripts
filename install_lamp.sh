#!/bin/bash
##############################
# Version 0.3 - 
#   Initial release 09/01/2014
#   last updated 02/06/2014
#
# by Paul Miller
##############################
# DESCRIPTION:
# installation script for 
# LAMP/LEMP/LLMP (Apache,
# Nginx or Lighttpd with MySQL)
# LASP/LESP/LLSP (as above with
# SQLite).
#
# Each DB has optionally PHP GUI
# Script is focused on Raspberry Pi
# Wheezy installations. But may
# function on other Debian/Ubuntu
# Installs. 
##############################

IPv4_ADDRESS=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
FQDN=$(hostname -f)
DIALOG="$(which whiptail dialog)"

# Locate any existing apps
APACHE="$(which apache2)"
NGINX="$(which nginx)"
LIGHTTP="$(which lighttpd)"
MYSQL="$(which mysqld)"
SQLITE="$(which sqlite)"
PGS=""

# establish text format options
bold=`tput bold`
normal=`tput sgr0`

# initialise variables
WEBSVR="NONE"
DBSVR="NONE"
PHPSVR=0
PHPMYADMIN=0
PHPLITEADMIN=0
PHPPGADMIN=0

# Define web app packages
APACHEPKG="apache2"
LIGHTTPDPKG="lighttpd"
NGINXPKG="nginx"


# Define php app packages
PHPPKG="php5 zip unzip"
PHPMYSQLPKG="php5-mysql"
PHPSQLITE="php5-sqlite"
PHPPOSTGRE="php5-pgsql"
PHPLIGHTTPDPKG="php5-fpm php5-curl php5-gd php5-intl php-pear php5-imagick php5-imap php5-mcrypt"
PHPAPACHEPKG="php5-cli php-db php5-cgi libapache2-mod-php5"
PHPNGINXPKG="php5-fpm php-apc"
PHPLITEADMIN="https://phpliteadmin.googlecode.com/files/phpliteAdmin_v1-9-5.zip"


# Define database app packages
MYSQLPKG="mysql-server mysql-client"
SQLITEPKG="sqlite"
POSTGREPKG="postgresql"


#---------------------
# Functions
#---------------------
function pTitle () {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as arguament to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot() {
# Supports: text, whiptail, dialog
# Requires:
# DIALOG="$(which whiptail dialog)" # variable
##############################################

logger "$0 Entered OS ENVIRONMENT CHECKS" 

if [[ $DIALOG ]]; then
	(( `id -u` )) && $DIALOG --msgbox --title "Checking for root Priviledges:" "Must be ran as root, try prefixing with sudo." 8 44 && exit 1
else
	pTitle "Checking for root Priviledges:"
	(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
fi
#func_chkRoot
}

function func_chkInternet () {
pTitle "Checking Internet Connectivity."
/usr/bin/wget -q --tries=10 --timeout=5 "http://www.google.com" -O /tmp/index.google &> /dev/null
if [[ ! -s /tmp/index.google ]];then
	echo "No Internet connection. Exiting."
	/bin/rm /tmp/index.google
	exit 1
else
	echo "Internet connection working"
	/bin/rm /tmp/index.google
fi
#func_chkInternet
}

function func_updSystem() {
pTitle 'Updating system, please Wait.'
apt-get -y -qq update
  
if [[ $DIALOG ]]; then
	#Check if user want to upgrade now.
	whiptail --yesno --title "Upgrade" "Would you like to upgrade the system now?" 10 50
	if [[ $? -ne 0 ]]; then
		echo "NO, selected so continuing..."
	else
		apt-get -y upgrade;
		apt-get -y autoremove;
	fi
else  
	while true; do
	read -p "Would you like to upgrade the system now? (Y/N)" answer
		case $answer in
			[Yy] ) 
				echo "Upgrading please wait..";
				apt-get -y upgrade;
				apt-get -y autoremove;
				break;;
			[Nn] ) echo "NO, selected so continuing"; break;;
			* ) echo "Please answer Y or N.";;
		esac
	done
fi
#End func_updSystem    
}

function func_InstPackages(){
#Usuage:
# call finction with list of packages to be installed as the argument"
# e.g. 
# MusicPKG="mp3 sox"
# func_InstPackages MusicPKG
##########################
PACKAGES="${!1}"

for pkg in $PACKAGES; do
	if apt-get -y -qq install $pkg; then
		echo "${bold}Successfully installed $pkg ${normal}"
	else
		echo "${bold}Error installing $pkg ${normal}"
		exit 1
	fi
done

pTitle "Package $1 installation complete."
#End func_InstPackages
}

########### WEB #############

function func_InstWebSvr() {
#Prompts User to select which web server to install
logger "$0 Entered func_InstWebSvr"
if [[ $DIALOG ]]; then
	WEBSVR=$($DIALOG --menu "Select a Web Server to Install:" --cancel-button Exit --ok-button Select 12 48 4 \
	apache2 "" \
	nginx "" \
	lighttpd "" \
	NONE "" \
	3>&1 1>&2 2>&3)
if [[ $? -ne 0 ]]; then exit 1; fi
	case $WEBSVR in
		apache2 ) 
			func_InstApache
			;;
		nginx ) 
			func_InstNginx
			;;
		lighttpd )
			func_InstLighttpd
			;;
		NONE ) 
			:
			;;
		* ) echo "An unexpected error has occurred.";;
	esac
else
	pTitle "Select a Web Server to Install:"
	echo "1 - Apache2"
	echo "2 - Nginx"
	echo "3 - Lighttpd"
	echo "N - None"
	while true; do
		read -p "Which Web Server should be Installed?" answer
		case $answer in
			1 ) WEBSVR="apache2"; func_InstApache; break;;
			2 ) WEBSVR="nginx"; func_InstNginx; break;;
			3 ) WEBSVR="lighttpd"; func_InstLighttpd; break;;
			[Nn] ) WEBSVR="NONE" ; break;;
			* ) echo "Please select a valid option.";;
		esac
	done
fi
# End func_InstWebSvr
}
  
function func_InstApache() {
logger "$0 Entered func_InstApache"
pTitle "Installing Apache:"

func_InstPackages APACHEPKG

#setup the servername
echo 'ServerName       $FQDN' > /etc/apache2/conf.d/servername.conf

chown www-data:www-data /var/www
chmod 775 /var/www
usermod -a -G www-data pi

a2enmod ssl 
a2ensite default-ssl

/etc/init.d/apache2 restart
# End func_InstApache
}

function func_InstNginx () {
logger "$0 Entered func_InstNginx"
pTitle "Installing Nginx:"

#install
func_InstPackages NGINXPKG

# uncomment the line
sed -i 's/^worker_processes 4/worker_processes 1/g' /etc/nginx/nginx.conf

# sed -i 's/^cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g' /etc/php5/fpm/php.ini
mkdir /var/www
chown www-data:www-data /var/www
chmod 775 /var/www
usermod -a -G www-data pi

/etc/init.d/nginx stop
unlink /etc/nginx/sites-enabled/default

echo '
# added by $0
server {
        listen 80;
        root /var/www;
        index index.html index.htm;
		server_name $FQDN;
}

' > /etc/nginx/sites-available/default-site

ln -s /etc/nginx/sites-available/default-site /etc/nginx/sites-enabled/default-site

/etc/init.d/nginx restart

# End func_InstNginx
} 

function func_InstLighttpd () {
logger "$0 Entered func_InstLighttpd"
pTitle "Installing Lighttpd:"
func_InstPackages LIGHTTPDPKG
chown www-data:www-data /var/www
chmod 775 /var/www
usermod -a -G www-data pi

# move page to the standard test html page location
cd /var/www
mv index.lighttpd.html index.html

/etc/init.d/lighttpd restart

# End func_InstLighttpd
}

######### DATABASE ##########

function func_InstDBSvr() {
#Prompts User to select which database server to install
# Note: use the service name for selection.
logger "$0 Entered func_InstDBSvr" 
if [[ $DIALOG ]]; then
  DBSVR=$($DIALOG --menu "Select a Database Server to Install:" --cancel-button Exit --ok-button Select 12 48 4 \
  mysqld "" \
  sqlite "" \
  postgresql "" \
  NONE "" \
  3>&1 1>&2 2>&3)
  if [[ $? -ne 0 ]]; then exit 1; fi
    case $DBSVR in
      mysqld ) 
		func_InstMySQL
		;;
      sqlite ) 
		func_InstSQLite
		;;
	  postgresql )
	    func_InstPostGRE
		;;
  	  NONE ) 
        :
	    ;;
      * ) echo "An unexpected error has occurred.";;
    esac
  
else
  pTitle "Select a Database Server to Install:"
  echo "1 - MySQL"
  echo "2 - SQLite"  
  echo "N - None"
  while true; do
    read -p "Which Database Server should be Installed?" answer
      case $answer in
        1 ) DBSVR="mysqld"; func_InstMySQL; break;;
        2 ) DBSVR="sqlite"; func_InstSQLite; break;;
        3 ) DBSVR="postgresql"; func_InstPostGRE; break;;
		[Nn] ) DBSVR="NONE" ; break;;
        * ) echo "Please select a valid option.";;
      esac
  done
fi

# End func_InstDBSvr
}

function func_InstMySQL() {
logger "$0 Entered func_InstMySQL"
pTitle "Installing MySQL:"
func_InstPackages MYSQLPKG
#Set MySQL to start automatically
update-rc.d mysql remove
update-rc.d mysql defaults
DBSVR="mysqld"
# End func_InstMySQL
}

function func_InstSQLite() {
logger "$0 Entered func_InstSQLite"
pTitle "Installing SQLite:"
func_InstPackages SQLITEPKG
DBSVR="sqlite"
# End func_InstSQLite
}

function func_InstPostGRE() {
logger "$0 Entered func_InstPostGRE"
pTitle "Installing PostGreSQL"
func_InstPackages POSTGREPKG
DBSVR="postgresql"
# End func_InstPostGRE
}

########### PHP #############

function func_InstPhP () {
logger "$0 Entered func_InstPhP"
if [[ $WEBSVR != "NONE" ]]; then func_InstPhPWEB; fi
if [[ $DBSVR != "NONE" ]]; then func_InstPhPDB; fi

# END func_InstPhP
}

function func_InstPhPWEB() {
# select what to do based on the webserver previously installed.
logger "$0 Entered func_InstPhPWEB"
  case $WEBSVR in
    apache2 ) 
	  echo "Adding apache module"
      func_InstPackages PHPAPACHEPKG
      pTitle "Installing PhP:"
	  func_InstPackages PHPPKG
	  
	  # create a basic php test page
      echo '<?php phpinfo(); ?>' > /var/www/phpinfo.php
	  PHPSVR=1
	  ;;
	nginx )	
      pTitle "Installing PhP:"
	  func_InstPackages PHPNGINXPKG
      echo '
	  # added by $0
	  server {
        listen 80;
        root /var/www;
        index index.php index.html index.htm;
		server_name $FQDN;

        location ~ \.php$ {
                fastcgi_pass unix:/var/run/php5-fpm.sock;
                fastcgi_index index.php;
                include fastcgi_params;
        }
      }
	
	  ' > /etc/nginx/sites-available/default-site

	  /etc/init.d/nginx restart

	  # create a basic php test page
      echo '<?php phpinfo(); ?>' > /var/www/phpinfo.php
	  PHPSVR=1
	  ;;
	lighttpd )
	         echo "Adding Lighttpd modules."
                func_InstPackages PHPLIGHTTPDPKG
                pTitle "Installing PhP:"
	         func_InstPackages PHPPKG
                #lighty-enable-mod fastcgi-php

	         # create a basic php test page
                echo '<?php phpinfo(); ?>' > /var/www/phpinfo.php
	  
                # uncomment the line
                sed -i '' 's/^; cgi.fix_pathinfo=1/cgi.fix_pathinfo=1/g' /etc/php5/fpm/php.ini

                cd /etc/lighttpd/conf-available/
                cp 15-fastcgi-php.conf 15-fastcgi-php-spawnfcgi.conf
      
echo '
# /usr/share/doc/lighttpd-doc/fastcgi.txt.gz
# http://redmine.lighttpd.net/projects/lighttpd/wiki/Docs:ConfigurationOptions#mod_fastcgi-fastcgi
      
## Start an FastCGI server for php (needs the php5-cgi package)
fastcgi.server += ( ".php" =>
  ((
     "socket" => "/var/run/php5-fpm.sock",
     "broken-scriptfilename" => "enable"
  ))
  )
' > 15-fastcgi-php.conf
      
               lighttpd-enable-mod fastcgi
               lighttpd-enable-mod fastcgi-php
               /etc/init.d/lighttpd restart
	        PHPSVR=1
	        ;;
    NONE )
               echo "${bold}PHP${normal}		Not installing PhP as web server installation was skipped."
	        PHPSVR=0
	        ;;
    * ) 
               echo "An unexpected error has occurred.";;
  esac
  
# END func_InstPhPWEB
}

function func_InstPhPDB() {
# select which Database to install and offer any optionally admin GUI
logger "$0 Entered func_InstPhPDB"
#perform any specific configuration for PHP based on DB instaalled
case $DBSVR in
  mysqld )
         logger "$0 $DBSVR detected as mysqld"
	  func_InstPackages PHPMYSQLPKG
         funct_InstPHPMyAdmin  
         ;;
  sqlite ) 
         logger "$0 $DBSVR detected as sqlite"
	  func_InstPackages PHPSQLITE
         funct_InstPHPLiteAdmin
         ;;
  postgresql ) 
         logger "$0 $DBSVR detected as postgresql"
	  func_InstPackages PHPPOSTGRE
         funct_InstPHPPGAdmin
         ;;
  NONE )
         echo "Issue with Script flow detected Exiting.... [func_InstPhPDB]"
	  logger "$0 Issue with Script flow detected Exiting.... [func_InstPhPDB]"
	  exit 1
         ;;
  * ) 
         echo "An unexpected error has occurred.";;
esac

# End func_InstPhP
}

function funct_InstPHPMyAdmin () {
# actions optionally to perform when a specific database is installed and PHP
echo "Entering funct_InstPHPMyAdmin, PHPSVR=$PHPSVR, DBSVR=$DBSVR"
if [[ $PHPSVR -eq "1" ]] && [[ $DBSVR == "mysqld" ]]
then
  #if there is also a dialog based service then provide GUI feedback to the user
  if [[ $DIALOG ]]
  then
    $DIALOG --yesno "Would you like phpmyadmin installed, a web GUI to adminstate MySQL?" 10 70
    answer=$?
    if [[ $answer == 0 ]]; then
      echo "You have selected to install the web GUI app"
	  apt-get -y -qq install phpmyadmin
	  PHPMYADMIN=1
    else
      echo "You have selected to not install the web GUI app"
    fi
  else
    #if there is no dialog based service then provide text feedback to the user
    while true; do
      read -p "Would you like phpmyadmin installed, a web GUI to adminstate MySQL? (Y/N)" answer
        case $answer in
          [Yy] ) 
			apt-get -y -qq install phpmyadmin
			PHPMYADMIN=1
			echo "|================================================================================|"
			echo "| The next package to be installed is PHPMyAdmin as part of the installation     |"
			echo "| a selection of the Web Server is Required.                                     |"
			echo "| You will see the following questions:                                          |"
			echo "|                                                                                |"
			case $WEBSVR in
				apache2 ) 
				  echo 'Include /etc/phpmyadmin/apache.conf' >> /etc/apache2/apache2.conf
				  echo "| Web server to reconfigure automatically: ${bold}<-- Apache2${normal}           |"
				  echo "| Configure database for phpmyadmin with dbconfig-common? ${bold}<-- No${normal} |"
				  ;;
				nginx ) 
				  echo "| Web server to reconfigure automatically: ${bold}<-- None${normal}              |"
				  echo "| Configure database for phpmyadmin with dbconfig-common? ${bold}<-- No${normal} |"
				  ;;
				lighttpd )
				  echo "| Web server to reconfigure automatically: ${bold}<-- Lighttpd${normal}          |"
				  echo "| Configure database for phpmyadmin with dbconfig-common? ${bold}<-- No${normal} |"
				  ;;
				* ) echo "An unexpected error has occurred.";;
			esac
			echo "|================================================================================|"
			break
			;;
          [Nn] ) echo "You have selected to not install the web GUI app"; break;;
          * ) echo "Please select a valid option.";;
        esac
    done
  fi
fi

# END funct_InstPHPMyAdmin
}

function funct_InstPHPLiteAdmin () {
# actions optionally to perform when a specific database is installed and PHP
echo "Entering funct_InstPHPLiteAdmin, PHPSVR=$PHPSVR, DBSVR=$DBSVR" 
logger "$0 Entered funct_InstPHPLiteAdmin"
if [[ $PHPSVR -eq "1" ]] && [[ $DBSVR == "sqlite" ]]
then
  #if there is also a dialog based service then provide GUI feedback to the user
  if [[ $DIALOG ]]; then
    $DIALOG --yesno "Would you like phpliteadmin installed, a web GUI to adminstate SQLite?" 10 70
    answer=$?
    if [[ $answer == 0 ]]; then
      echo "You have selected to install the web GUI app"
         wget -qO- -O tmp.zip --no-check-certificate '$(PHPLITEADMIN)' && unzip -o tmp.zip -d /var/www && rm tmp.zip
	  logger "$0 Downloaded phpliteadmin: $(PHPLITEADMIN)" 
	  PHPLITEADMIN=1
    else
      echo "You have selected to not install the optional web GUI app"
    fi
  else
    #if there is no dialog based service then provide text feedback to the user
    while true; do
      echo "Request user input on PHP administration gui installation"
      read -p "Would you like phpliteadmin installed, a web GUI to adminstate SQLite? (Y/N)" answer
        case $answer in
          [Yy] ) 
               echo "You have selected to install the web GUI app"
	        # wget --no-check-certificate $PHPLITEADMIN
               wget -qO- -O tmp.zip --no-check-certificate '$(PHPLITEADMIN)' 
               echo "wget $(PHPLITEADMIN) executed"
               unzip -o tmp.zip -d /var/www
               echo "unzip of tmp.zip executed" 
               rm tmp.zip
               echo "cleanup removal of tmp.zip" 
	        logger "$0 Downloaded phpliteadmin: $(PHPLITEADMIN)" 
		 # unzip phpliteAdmin_v1-9-5.zip -d /var/www
		 PHPLITEADMIN=1
		 break
		 ;;
          [Nn] )
               echo "You have selected to not install the optional web GUI app"; break;;
          * )  
               echo "Please select a valid option.";;
        esac
    done
  fi
fi
# End funct_InstPHPLiteAdmin
}

function funct_InstPHPPGAdmin () {
# actions optionally to perform when a specific database is installed and PHP
if [[ $PHPSVR -eq "1" ]] && [[ $DBSVR == "postgresql" ]]
then
  #if there is also a dialog based service then provide GUI feedback to the user
  if [[ $DIALOG ]]
  then
    $DIALOG --yesno "Would you like phppgadmin installed, a web GUI to adminstate PostGreSQL?" 10 70
    answer=$?
if [[ $answer == 0 ]]; then
      echo "You have selected to install the web GUI app"
	  apt-get -y -qq install phppgadmin
	  sed -i '' 's/^$conf['extra_login_security'] = true;/$conf['extra_login_security'] = false;/g' /usr/share/phppgadmin/conf/config.inc.php
	  # create a symbolic link in /var/www/
	  ln -s /usr/share/phppgadmin /var/www/phppgadmin
	  PHPPGADMIN=1
    else
      echo "You have selected to not install the optional web GUI app"
    fi
  else
    #if there is no dialog based service then provide text feedback to the user
    while true; do
      read -p "Would you like phppgadmin installed, a web GUI to adminstate PostGreSQL? (Y/N)" answer
        case $answer in
          [Yy] ) 
            echo "You have selected to install the web GUI app"
	        apt-get -y -qq install phppgadmin
			sed -i 's/^$conf['extra_login_security'] = true;/$conf['extra_login_security'] = false;/g' /usr/share/phppgadmin/conf/config.inc.php
	        # create a symbolic link in /var/www/
			ln -s /usr/share/phppgadmin /var/www/phppgadmin
			PHPPGADMIN=1
			break
			;;
          [Nn] ) echo "You have selected to not install the optional web GUI app"; break;;
          * ) echo "Please select a valid option.";;
        esac
    done
  fi
fi
# END funct_InstPHPPGAdmin
}

function func_Complete(){
#func_Complete
logger "$0 Entered func_Complete"
MESSAGE="A $WEBSVR webserver can be found by "
MESSAGE=$MESSAGE"pointing your web browser at:\n"
MESSAGE=$MESSAGE"\n"

for addr in $IPv4_ADDRESS; do
  MESSAGE=$MESSAGE"HTTP:          http://$addr\n"
  
  if [[ $PHPSVR ]]; then
    MESSAGE=$MESSAGE"PhP Test:      http://$addr/phpinfo.php\n"
  fi
  
  if [[ $PHPMYADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPMyAdmin     http://$addr/phpmyadmin\n"
  fi
  
  if [[ $PHPLITEADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPLiteAdmin   http://$addr/phpliteadmin.php (passwd = admin)\n"
  fi
  
  if [[ $PHPPGADMIN -eq "1" ]]; then
    MESSAGE=$MESSAGE"PhPPGAdmin   http://$addr/phppgadmin (login postgres, blank passwd)\n"
  fi
done

if [[ $DIALOG ]]
then
  $DIALOG --msgbox --title "Script is Complete!" "$MESSAGE" 18 78
else
  pTitle "${bold}The script is complete.${normal}"
  echo -e "$MESSAGE"
fi
# func_Complete
}

##---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
#func_chkRoot
#func_chkInternet
#func_updSystem

#---------------------
# INSTALL Aplications
#---------------------

func_InstDBSvr
func_InstWebSvr
func_InstPhP

#---------------------
# FINISHING OFF
#---------------------	
if [[ $WEBSVR != "NONE" ]]; then
  /etc/init.d/$WEBSVR restart
  func_Complete
else
  echo "Web server not installed."
fi
echo "Done!"
echo "================================================================================"

exit
#!/bin/sh

DIR=$(cd $(dirname $0) && pwd)
cd $DIR

file='/home/user9/work/conf.txt'
tempfile1=/tmp/dialog_1_$$
tempfile2=/tmp/dialog_2_$$
tempfile3=/tmp/dialog_3_$$

trap "rm -f $tempfile1 $tempfile2 $tempfile3" 0 1 2 5 15

_main () {
   dialog --title "Operational Profile" \
           --menu "Please choose an option:" 15 55 5 \
                   1 "Voice Connect Asterisk" \
                   2 "Wireshark Capture HUB" \
                   3 "SIPp Testing" \
                   Q "Quit" 2> $tempfile3

   retv=$?
   choice=$(cat $tempfile3)
   [ $retv -eq 1 -o $retv -eq 255 ] && exit
cd profiles
   case $choice in
       1) ./asterisk-env.sh
           ;;
       2) ./wireshark-env.sh
           ;;
       3) ./sipp-env.sh
           ;;
       3) exit ;;
   esac
clear
}

_main


#!/bin/bash
# Hash Calculator for SIP
# v0.1  -  Original draft as recieved from Paul Brammer
# v0.2  - format change for initial testing


#Authorization: Digest 
#username="5999"
#realm="asterisk"
#nonce="6bdf8f20"
#uri="sip:0211850717@192.168.0.23"
#algorithm=MD5
#response="a94b3b4701413a56ba7517b405de5bb7"

###################################
# Example Usage
#Execute:
#$ hashcalc.sh <password> <nonce>
###################################

# HA1 = md5(username:realm:password)
# HA2 = md5(method:digestURI)
# Response = md5(HA1:nonce:HA2)
# Before us is the script that calculates md5 response digest:

HA1=$(echo -n "5999:192.168.0.23:$1" | md5sum | sed -e 's/\s*-$//');
echo "HA1="$HA1
HA2=$(echo -n "SIP:0211850717@192.168.0.23" | md5sum | sed -e 's/\s*-$//');
echo "HA2="$HA2
RESPONSE=$(echo -n "${HA1}:$2:${HA2}" | md5sum | sed -e 's/\s*-$//' );
echo "RESPONSE="$RESPONSE


#!/bin/bash
##############################
# by Paul Miller
# installation script for 
# TFTP Server with
# a focus on Raspberry Pi
# Wheezy installations.  
##############################

#---------------------
# Functions
#---------------------

function func_InstTFTPSvr () {
#Install a TFTP server
apt-get install xinetd tftpd tftp -y
cat <<EOF > /etc/xinetd.d/tftp
service tftp
{
protocol        = udp
port            = 69
socket_type     = dgram
wait            = yes
user            = nobody
server          = /usr/sbin/in.tftpd
server_args     = /tftpboot
disable         = no
}
EOF

mkdir /tftpboot
chmod -R 777 /tftpboot
echo 'includedir /etc/xinetd.d' >> /etc/xinetd.conf
/etc/init.d/xinetd restart
}

function func_chkRoot() {
echo "================================================================================"
echo "Checking for root Priviledges:"
echo "================================================================================"
logger "$0 entered OS ENVIRONMENT CHECKS" 
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
}

function func_chkInternet () {
/usr/bin/wget -q --tries=10 --timeout=5 http://www.google.com -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection working"
        /bin/rm /tmp/index.google
fi
}

#---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
func_chkInternet

#---------------------
# INSTALL SERVER
#---------------------
func_InstTFTPSvr
exit
	


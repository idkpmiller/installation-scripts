#!/bin/bash
#
# Raspberry Pi headless Optimisation
#by Paul Miller
#Revisions:
#1.0 - Initial 26/12/2013


#====END of User Varables==================

#check current user is root
(( `id -u` )) && echo "Must be ran as root, try prefixxing with sudo." && exit 1

#clear the screen
clear

# Get Input from User
echo "Capture User Options:"
echo "====================="
echo "Please answer the following questions."
echo "Hitting return will continue with the default option"
echo
echo

# REMOVE GRAPHICAL PACKAGES
read -p "Remove graphicl packages not typically needed for a headless install? (y/n) " RESP
if [ "$RESP" = "y" ]; then
  echo "Removing packages this will take some time"
  # update system
  echo ""
  echo "###############PLEASE WAIT##################"
  apt-get -qq update 
  echo "update complete"
  # install requirements
  echo "Removing Packages"
  apt-get -y -qq remove aspell hunspell-en-us iptraf libaspell15 libhunspell-1.2-0 lxde lxsession lxtask lxterminal squeak-vm whiptail zenity gdm gnome-themes-standard python-pygame
  sudo apt-get remove `sudo dpkg --get-selections | grep -v "deinstall" | grep x11 | sed s/install//`
  apt-get -y -qq autoremove
	
else
  echo "NOT removing graphical packages."
  echo ""
fi

# REMOVE Misc PACKAGES
read -p "Make this a minimalistic image? (y/n) " RESP
if [ "$RESP" = "y" ]; then
  echo "Removing packages this will take some time"
  # update system
  echo ""
  echo "###############PLEASE WAIT##################"
  apt-get -qq update 
  echo "update complete"
  # install requirements
  echo "Removing Packages"
  # remove all the python stuff
  rm -rf python_games
  apt-get -y remove x11-common midori lxde python3 python3-minimal
  
  rm -rf opt
  
  # remove all the development stuff
  apt-get remove `sudo dpkg --get-selections | grep "\-dev" | sed s/install//`
  
  apt-get remove `sudo dpkg --get-selections | grep -v "deinstall" | grep python | sed s/install//`
  apt-get remove lxde-common lxde-icon-theme omxplayer raspi-config
  
  # no need for sound remove audio stuff
  apt-get remove `sudo dpkg --get-selections | grep -v "deinstall" | grep sound | sed s/install//`
  
  # remove all gcc except for 4.7
  apt-get remove gcc-4.4-base:armhf gcc-4.5-base:armhf gcc-4.6-base:armhf
  
  apt-get remove ca-certificates libraspberrypi-doc xkb-data fonts-freefont-ttf locales manpages
  apt-get -y -qq autoremove
  apt-get clean
  
  # turn off swap and fill with zeros
  swapoff -a
  cd /var
  dd if=/dev/zero of=swap bs=1M count=100

  # delete the logs
  cd /var/log/
  rm `find . -type f`  
else
  echo "NOT removing graphical packages."
  echo ""
fi



# REDUCE SSH LOGIN DELAY
read -p "Apply fix to remove SSH login delay? (y/n) " RESP
if [ "$RESP" = "y" ]; then
  echo "Checking current configuration please wait"

  declare file="/etc/ssh/sshd_config"
  declare regex="\s+UseDNS\s+"
#  declare regex="\s+string\s+"

  declare file_content=$( cat "${file}" )
  if [[ " $file_content " =~ $regex ]] # please note the space before and after the file content
    then
        echo "Option UseDNS found in file /etc/ssh/sshd_config - nothing done"
    else
        echo UseDNS no >> /etc/ssh/sshd_config
        service ssh restart
        echo "Added option UseDNS to /etc/ssh/sshd_config and restarted SSH" 
  fi
	
  else
    echo "NOT applying SSH delay fix."
    echo ""
  fi
exit



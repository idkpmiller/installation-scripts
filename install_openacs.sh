#!/bin/bash
##############################
# by Paul Miller
# installation script for 
# LAMP (Apache or Nginx) with
# JBoss and OpenACS
# focus on Raspberry Pi
# Wheezy installations.  
##############################

IPv4_ADDRESS=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
FQDN=$(hostname -f)
WEBSVR="APACHE"
APACHEPKG="apache2"
PHPPKG="php5 php5-cli php5-mysql php-db php5-cgi"
MYSQLPKG="mysql-server mysql-client"
JAVAPKG="openjdk-7-jre"
UTILSPKG="chkconfig unzip"
OPENACSPKG="ant subversion"
bold=`tput bold`
normal=`tput sgr0`


#---------------------
# Functions
#---------------------

function func_chkRoot() {
echo "================================================================================"
echo "Checking for root Priviledges:"
echo "================================================================================"
echo ""
logger "$0 entered OS ENVIRONMENT CHECKS" 
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
#func_chkRoot
}

function func_chkInternet () {
/usr/bin/wget -q --tries=10 --timeout=5 "http://www.google.com" -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection working"
        /bin/rm /tmp/index.google
fi
#func_chkInternet
}

function func_updSystem() {
echo "================================================================================"
echo 'updating system, please Wait.'
echo "================================================================================"
echo ""
  apt-get -y -qq update
  
  #Check if user want to upgrade now.
  while true; do
    read -p "Would you like to upgrade the system now? (Y/N)" answer
    case $answer in
        [Yy] ) 
			echo "Upgrading please wait..";
			apt-get -y upgrade;
			apt-get -y autoremove;
			break;;
        [Nn] ) echo "NO, selected so continuing"; break;;
        * ) echo "Please answer Y or N.";;
    esac
done
#func_updSystem    
  }

function func_InstPackages(){
# call finction with list of packages to be installed as the argument"
# e.g. 
# MusicPKG="mp3 sox"
# func_InstPackages MusicPKG
##########################
PACKAGES="${!1}"

for pkg in $PACKAGES; do
  if apt-get -y -qq install $pkg; then
    echo "${bold}Successfully installed $pkg ${normal}"
  else
    echo "${bold}Error installing $pkg ${normal}"
	exit 1
  fi
done
echo "${bold}Package $1 installation complete. ${normal}"
echo "================================================================================"
echo ""
}
  
function func_InstApache() {
echo "================================================================================"
echo "Installing Apache:"

func_InstPackages APACHEPKG

#setup the servername
echo 'ServerName       $FQDN' > /etc/apache2/conf.d/servername.conf

chown www-data:www-data /var/www
chmod 775 /var/www
usermod -a -G www-data pi

a2enmod ssl 
a2ensite default-ssl

/etc/init.d/apache2 restart
echo "================================================================================"
echo ""
#func_InstApache
}

function func_InstMySQL() {
echo "================================================================================"
echo "Installing MySQL:"
echo "================================================================================"
func_InstPackages MYSQLPKG

#Set MySQL to start automatically
update-rc.d mysql remove
update-rc.d mysql defaults

/etc/init.d/mysql restart
#func_InstMySQL
}

function func_InstPhP() {
echo "================================================================================"
echo "Installing PhP:"
echo "================================================================================"
func_InstPackages PHPPKG

if [ $WEBSVR == "APACHE" ] 
then
  echo "Adding apache module"
  apt-get -y -qq install libapache2-mod-php5
  echo 'Include /etc/phpmyadmin/apache.conf' >> /etc/apache2/apache2.conf
  echo "================================================================================"
  echo "The next package to be installed is PHPMyAdmin as part of the installation"
  echo "a selection of the Web Server is Required. For this installation you should select:"
  echo "Apache"
  echo "================================================================================"
else
  :
fi

if [ $WEBSVR == "NGINX" ] 
then
  ln -s /usr/share/phpmyadmin/ /usr/share/nginx/www
  echo "================================================================================"
  echo "The next package to be installed is PHPMyAdmin as part of the installation"
  echo "a selection of the Web Server is Required. For this installation you should"
  echo "NOT select ANY Web Server."
  echo "================================================================================"
else
  :
fi

apt-get -y -qq install phpmyadmin

#create a basic php test page
echo '<?php phpinfo(); ?>' > /var/www/phpinfo.php

#func_InstPhP
}

function func_InstJava () {
cd /usr/src
# Install java
func_InstPackages JAVAPKG
func_InstPackages UTILSPKG
read -p "Error check point, press enter to continue or CTRL-C to exit" TEMP

#test java
/usr/lib/jvm/java-1.7.0-openjdk-armhf/bin/java -version
read -p "Check the version of Java is displayed, press enter to continue or CTRL-C to exit" TEMP

#make the path easier
cd /usr/lib/jvm


echo '  
export JAVA_HOME=/usr/lib/jvm/jdk-7-oracle-armhf  
export PATH=$JAVA_HOME/bin:$PATH  
' >> /etc/profile

source /etc/profile
}

function func_InstJBoss () {
cd /usr/src
wget "http://repo1.maven.org/maven2/org/jboss/as/jboss-as-dist/7.1.1.Final/jboss-as-dist-7.1.1.Final.tar.gz"
read -p "Error check point, press enter to continue or CTRL-C to exit" TEMP

# extract tar and position jboss
mkdir 
tar xzvf /usr/src/jboss-as-dist-7.1.1.Final.tar.gz -C /opt/jboss
cd /opt/jboss
mv jboss-as-7.1.1.Final/* ./
rmdir /opt/jboss/jboss-as-7.1.1.Final/
read -p "Error check point, press enter to continue or CTRL-C to exit" TEMP

echo '  
export JBOSS_HOME=/opt/jboss  
export PATH=$JBOSS_HOME/bin:$PATH
' >> ~/.bashrc

# add user and group
addgroup jboss  
useradd -g jboss jboss  
chown -R jboss:jboss /opt/jboss/
read -p "Error check point, press enter to continue or CTRL-C to exit" TEMP

# xml config file

echo '
<interfaces>
    <interface name="management">
        <inet-address value="0.0.0.0"/>
    </interface>
    <interface name="public">
        <inet-address value="0.0.0.0"/>
    </interface>
    <interface name="unsecure">
        <inet-address value="${jboss.bind.address.unsecure:127.0.0.1}"/>
    </interface>
</interfaces>

' > /opt/jboss/standalone/configuration/standalone-full.xml

# create init file
touch /etc/init.d/jbossas7  
chmod +x /etc/rc.d/init.d/jbossas7  
mkdir /etc/rc.d  
mkdir /etc/rc.d/init.d  
ln -s /etc/rc.d/init.d/jbossas7 /etc/init.d/jbossas7 

echo '
#!/bin/sh  
### BEGIN INIT INFO  
# Provides: jboss  
# Required-Start: $local_fs $remote_fs $network $syslog  
# Required-Stop: $local_fs $remote_fs $network $syslog  
# Default-Start: 2 3 4 5  
# Default-Stop: 0 1 6  
# Short-Description: Management of JBoss AS v7.x  
### END INIT INFO  
#Defining JBOSS_HOME  
JBOSS_HOME=/usr/jboss/  
case "$1"  
in  
start)  
echo "Starting JBoss AS7..."  
sudo -u jboss sh ${JBOSS_HOME}/bin/standalone.sh &  
;;  
stop)  
echo "Stopping JBoss AS7..."  
sudo sh ${JBOSS_HOME}/bin/jboss-cli.sh --connect command=:shutdown  
;;  
log)  
echo "Showing server.log..."  
tail -1000f ${JBOSS_HOME}/standalone/log/server.log  
;;  
*)  
echo "Usage: /etc/init.d/jboss {start|stop|log}"  
exit 1  
;; esac  
exit 0

' > /etc/init.d/jbossas7

# setup the start up configuration
cd /etc/init.d/  
chkconfig --add jbossas7 

#start jboss
/etc/init.d/jbossas7 start 
read -p "Error check point, press enter to continue or CTRL-C to exit" TEMP
}

function func_InstJConnector () {
cd /usr/src
#func_InstMySQL
func_InstMySQL
read -p "Error check point, press enter to continue or CTRL-C to exit" TEMP

#add the J connector to MySQL
cd /usr/src
wget "http://mirror.cogentco.com/pub/mysql/Connector-J/mysql-connector-java-5.1.28.tar.gz"
read -p "Error check point, press enter to continue or CTRL-C to exit" TEMP

#extract to jboss directory
tar -xzvf mysql-connector-java-5.1.28.tar.gz
cd mysql-connector-java-5.1.28
mv mysql-connector-java-5.1.28-bin.jar /usr/jboss/server/default/lib/
}

function func_InstOpenACS () {
#Install Apache Ant
func_InstPackages OPENACSPKG

# download OpenACS
cd ~
svn co https://svn.code.sf.net/p/openacs/code/trunk/acs/ openacs
sed 's/^jboss=.*$/jboss=\/opt\/jboss\/server\/default/' openacs/build.properties > openacs/.build.properties
mv openacs/.build.properties openacs/build.properties
mv openacs/acs-war/web/WEB-INF/web.prot.xml openacs/acs-war/web/WEB-INF/web.xml

ant -f b.xml
cp dist/acs.ear /opt/jboss/server/default/deploy/

# create firmware directory and owner 
mkdir /firmware
chown jboss /firmware

#
#Edit the web.xml file and set the right path to the firmware directory (org.openacs.fwbase context-param):
#nano acs-war/web/WEB-INF/web.xml
#[... cut ...]
#    <context-param>
#        <description>Path for firmware images</description>
#        <param-name>org.openacs.fwbase</param-name>
#        <param-value>/firmware/</param-value>
#    </context-param>
#[... cut ...]
#Run ant to build OpenACS
ant

#Copy dist/acs.ear to jboss/server/default/deploy:
cp dist/acs.ear /opt/jboss/server/default/deploy/

#Create and edit jboss/server/default/deploy/openacs-ds.xml, configuring data source
echo '
<?xml version="1.0" encoding="UTF-8"?>
<datasources>
    <local-tx-datasource>
        <jndi-name>ACS</jndi-name>
        <connection-url>jdbc:mysql://localhost/ACS</connection-url>
        <driver-class>com.mysql.jdbc.Driver</driver-class>
        <user-name>openacs</user-name>
        <password>openacs</password>
        <min-pool-size>5</min-pool-size>
        <max-pool-size>20</max-pool-size>
        <idle-timeout-minutes>5</idle-timeout-minutes>
    </local-tx-datasource>
</datasources>
' > /opt/jboss/server/default/deploy/openacs-ds.xml

#Create openacs-service.xml in jboss/server/default/deploy/jms
echo '
<?xml version="1.0" encoding="UTF-8"?>
<server>
    <mbean code="org.jboss.mq.server.jmx.Queue" name="jboss.mq.destination:service=Queue,name=acsQueue">
        <depends optional-attribute-name="DestinationManager">jboss.mq:service=DestinationManager</depends>
    </mbean>
</server>
' > /opt/jboss/server/default/deploy/jms/openacs-service.xml

#Create ACS database and openacs user on MySQL, as in openacs-ds.xml:
#mysql
#CREATE DATABASE ACS;
#GRANT ALL ON ACS.* TO openacs IDENTIFIED BY 'openacs';
#Create ACS tables:

echo "CREATE TABLE HardwareModelBean (
  id int(11) NOT NULL auto_increment,
  oui varchar(250) default NULL,
  hclass varchar(250) default NULL,
  DisplayName varchar(250) default NULL,
  manufacturer varchar(250) default NULL,
  PRIMARY KEY  (id)
);

CREATE TABLE HostsBean (
  id int(11) NOT NULL auto_increment,
  serialno varchar(250) default NULL,
  url varchar(250) default NULL,
  configname varchar(250) default NULL,
  currentsoftware varchar(250) default NULL,
  sfwupdtime timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  sfwupdres varchar(250) default NULL,
  cfgupdres varchar(250) default NULL,
  lastcontact timestamp NOT NULL default '0000-00-00 00:00:00',
  cfgupdtime timestamp NOT NULL default '0000-00-00 00:00:00',
  hardware varchar(250) default NULL,
  cfgversion varchar(250) default NULL,
  props longblob,
  hwid int(11) default NULL,
  username varchar(250) default NULL,
  password varchar(250) default NULL,
  authtype int(11) default NULL,
  customerid varchar(250) default NULL,
  conrequser varchar(250) default NULL,
  conreqpass varchar(250) default NULL,
  PRIMARY KEY  (id)
);

CREATE TABLE ConfigurationBean (
  name varchar(250) NOT NULL,
  hardware varchar(250) default NULL,
  config longblob,
  filename varchar(250) default NULL,
  version varchar(250) default NULL,
  PRIMARY KEY  (name)
);

CREATE TABLE ScriptBean (
  name varchar(250) NOT NULL,
  script longblob,
  description varchar(250) default NULL,
  PRIMARY KEY  (name)
);

CREATE TABLE SoftwareBean (
  hardware varchar(250) NOT NULL,
  version varchar(250) NOT NULL,
  minversion varchar(250) default NULL,
  url varchar(250) default NULL,
  size bigint(20) NOT NULL,
  filename varchar(250) default NULL,
  PRIMARY KEY  (hardware,version)
);" | mysql ACS;


#Run the server:
cd /opt/jboss/bin
./run.sh -b 0.0.0.0


#Browse the OpenACS web interface at http://YOUR_IP_ADDRESS:8080/openacs/index.jsf
#Some useful links:
#OpenACS Wiki: http://openacs.wiki.sourceforge.net/
#Getting JDK 1.5 and Tomcat 5.5 up and running in Debian Linux: http://nileshk.com/node/36
#JBoss on Debian quickstart: http://lorenzod8n.wordpress.com/2008/03/02/jboss-on-debian-quickstart/
#
#

}

function func_Dependencies(){
#Install Dependencies
clear

#func_Dependencies
}

#---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
func_chkInternet
func_updSystem

#---------------------
# INSTALL Aplications
#---------------------

func_Dependencies
func_InstJBoss
func_InstOpenACS
#func_InstApache

#func_InstPhP

#---------------------
# FINISHING OFF
#---------------------	
echo "================================================================================"
echo "${bold}The script is complete.${normal}"
echo "A webserver can be found by"
echo "pointing your web browser at:"
echo ""
echo "HTTP:			http://$IPv4_ADDRESS"
echo "PhP Test:		http://$IPv4_ADDRESS/phpinfo.php"
echo "Done!"
echo "================================================================================"

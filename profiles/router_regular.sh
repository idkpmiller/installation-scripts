#!/bin/bash

#************************************************#
#               Router-Regular                   #
#           written by Paul Miller               #
#                NOV, 2013                       #
#                                                #
#       Put the events you want to occur         #
#            when running asterisk               #
#************************************************#

if [ $DEBUG ]; then
	set -x
fi

DIALOG=${DIALOG-dialog}

#Check if root and advise if not
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1 

# log a message to /var/log/messages that we have entered the script.
logger Entered router_regular.sh script -t PROFILE



# Delete USB / Wifi default route
# route delete 0.0.0.0 192.168.1.1


# default gateway is via interface within the bridge
# route add -net 0.0.0.0 netmask 255.255.255.255 dev br0

# default gateway is via wifi
# route add -net 0.0.0.0 netmask 255.255.255.255 dev wlan0


# /24 routes when gateway is via interface on the bridge
route add -net 122.56.237.0 netmask 255.255.255.0 dev br0
route add -net 210.55.111.0 netmask 255.255.255.0 dev br0
route add -net 10.111.111.0 netmask 255.255.255.0 dev br0
route add -net 10.207.44.0 netmask 255.255.255.0 dev br0

# /25 routes when gateway is via interface on the bridge
route add -net 122.56.253.0 netmask 255.255.255.128 dev br0
route add -net 122.56.254.0 netmask 255.255.255.128 dev br0
route add -net 122.56.255.0 netmask 255.255.255.128 dev br0

# /27 routes when gateway is via interface on the bridge
route add -net 122.56.254.192 netmask 255.255.255.224 dev br0
route add -net 122.56.253.224 netmask 255.255.255.224 dev br0
route add -net 122.56.254.224 netmask 255.255.255.224 dev br0
route add -net 122.56.254.160 netmask 255.255.255.224 dev br0
route add -net 122.56.255.160 netmask 255.255.255.224 dev br0

# /29 routes when gateway is via interface on the bridge
route add -net 122.56.252.128 netmask 255.255.255.248 dev br0
route add -net 122.56.252.136 netmask 255.255.255.248 dev br0

# /32 routes when gateway is via interface on the bridge
route add -net 122.56.65.10 netmask 255.255.255.255 dev br0
route add -net 125.236.71.69 netmask 255.255.255.255 dev br0
route add -net 10.111.126.100 netmask 255.255.255.255 dev br0
route add -net 10.111.126.101 netmask 255.255.255.255 dev br0

# log a message to /var/log/messages that we have entered the script.
logger Exit router_regular.sh script -t PROFILE



#!/bin/bash

# This script updates the values in the database, and the last line applies the settings, causing
# freepbx to regenrate its config files for asterisk, nasty hack, but works


# Database credentials
dbuser=root
dbpassword=raspberry

# Location of the FreePBX Web interface
freepbx_base_url="http://127.0.0.1/admin"

# Login details for the FreePBX Web interface
ampuser="idkpmiller"
amppass="24662643"

########################START CODE ###################################

# define usage function
usage(){
	echo "Usage: $0 'Trunk Name'"
	echo "	Description:"
	echo "		Enables specified Trunk"
	echo "		Disables all other trunks."
	exit 1
}

# invoke  usage
# call usage() function if filename not supplied
[[ $# -eq 0 ]] && usage



# Pauls code, with some modifications to eliminate the need for the entire mysql command every time
mysql="mysql -D asterisk -u $dbuser -p$dbpassword -sNe"

count_total=$($mysql "SELECT COUNT(*) FROM trunks")
echo $count_total

count_ena=$($mysql "SELECT COUNT(*) FROM trunks WHERE disabled='on'")
echo $count_ena

if [ "$count_ena" -gt 0 ]
then
  result=$($mysql "SELECT name FROM trunks WHERE disabled='on'")
  echo $result
fi

echo

# Ryans Code

# All this does is return the id's of the disabled trunks
query="SELECT trunkid FROM trunks WHERE name='$1'"
echo $query
# Run the query
disabled_trunks=$($mysql "$query")
echo "-	Result: \"$disabled_trunks\""

# Check we got results
if [ ! -z "$disabled_trunks" ]; then
	# Replace all spaces with commas ready for the next set of queries

	sql_trunk_ids=$(echo $disabled_trunks | tr ' ' ',')

	# The statement above replaces all spaces with commas, as the NOT IN or IN syntax expects (1,2,3) etc
	# Set all the trunks that are disabled to enabled.
        query="UPDATE trunks SET disabled='off' WHERE trunkid IN ($sql_trunk_ids)"
	echo $query

	#Enable the Registration for the activatited trunk
	enReg="UPDATE sip SET flags=0 WHERE id IN ('tr-reg-$sql_trunk_ids')"
	echo $enReg


	# Run the query
	$mysql "$query"

	# Enable Trunk Registration
	$mysql "$enReg"

	# Disable all other trunks that were not updated above
	query="UPDATE trunks SET disabled='on' WHERE trunkid NOT IN ($sql_trunk_ids)"
	echo $query


	#Disable the Registration for the remaining trunks
	disReg="UPDATE sip SET flags=1 WHERE id NOT IN ('tr-reg-$sql_trunk_ids')"
	echo $disReg

	# Run the query
	$mysql "$query"

	# Disable Trunk Registration
	$mysql "$disReg"


fi

# Apply the changes by hitting the web URL
echo "Applying Changes"
curl -s --data "username=$ampuser&password=$amppass&handler=reload" $freepbx_base_url/config.php >/dev/null 2>&1


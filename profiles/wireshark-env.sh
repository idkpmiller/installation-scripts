#!/bin/bash

#************************************************#
#               wireshark-env                    #
#           written by Paul Miller               #
#                Feb, 2013                       #
#                                                #
#       Put the events you want to occur         #
#     when capture session is to take place.     #
#************************************************#

DIALOG=${DIALOG-dialog}

#Check if root and advise if not
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1 

# log a message to /var/log/messages that we have entered the script.
logger Entered wireshark-env.sh script -t PROFILE


f_AMPORTAL=`command -v amportal`

if [ -z $f_AMPORTAL ] 
then
   echo "AMPORTAL Not found"
fi

(echo "Shutting down unneeded network chatty services" ; sleep 2
echo "XXX" 
echo "Gracefully Stopping Asterisk, this may take some time" ;
echo "XXX";
$f_AMPORTAL stop >/dev/null 2>&1 ;
echo "10" ;
sleep 1 ;

echo "XXX" ; 
echo "Stopping MySQL" ;
echo "XXX";
/etc/init.d/mysql stop >/dev/null 2>&1 ;
echo "20" ;
sleep 1 ;

echo "XXX" ; 
echo "Stopping Avahi" ;
echo "XXX";
/etc/init.d/avahi-daemon stop >/dev/null 2>&1 ;
echo "40" ;
sleep 1 ;

echo "XXX" ; 
echo "Stopping Mail" ;
echo "XXX";
/etc/init.d/exim4 stop >/dev/null 2>&1 ;
echo "50" ;
sleep 1 ;

echo "XXX" ; 
echo "Stopping Web Server" ;
echo "XXX";
/etc/init.d/apache2 stop >/dev/null 2>&1 ;
echo "60" ;
sleep 1 ;

echo "XXX" ; 
echo "Stopping Samba" ;
echo "XXX";
/etc/init.d/samba stop >/dev/null 2>&1 ;
echo "70" ;
sleep 1 ;

echo "XXX" ; 
echo "Stopping RPCBind" ;
echo "XXX";
/etc/init.d/rpcbind stop >/dev/null 2>&1 ;
echo "80" ;
sleep 1 ;

echo "XXX" ; 
echo "Stopping NFS" ;
echo "XXX";
/etc/init.d/nfs stop >/dev/null 2>&1 ;
echo "90" ;
sleep 1 ;

echo "XXX" ; 
echo "Checking all services are down." ;
echo "XXX";
echo "99" ;
sleep 2) | \

$DIALOG --title "Profile Change Progress" --gauge "Profile Progress" 10 40 0

if [ "$?" = 255 ] ; then
	echo ""
	echo "Box closed !"
fi

# ==========================
MESSAGE="Profile change has been completed, Reduced network chatter sourced from host, ready for capturing"


$DIALOG --title "Update" --clear \
        --msgbox "$MESSAGE" 10 41

case $? in
  0)
    echo "OK";;
  255)
    echo "ESC pressed.";;
esac





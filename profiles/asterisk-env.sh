#!/bin/sh


file='/home/user9/work/conf.txt'
tempfile1=/tmp/dialog_1_$$
tempfile2=/tmp/dialog_2_$$
tempfile3=/tmp/dialog_3_$$

trap "rm -f $tempfile1 $tempfile2 $tempfile3" 0 1 2 5 15


_main () {
   dialog --title "Asterisk Environment" \
           --menu "Please choose an option:" 15 55 5 \
                   1 "VC Asterisk (NIL)" \
                   2 "VC Asterisk (PTC)" \
                   3 "VC Asterisk (PROD)" \
                   4 "VC Asterisk (TEST)" \
                   B "Back" 2> $tempfile3

   retv=$?
   choice=$(cat $tempfile3)
   [ $retv -eq 1 -o $retv -eq 255 ] && exit

   case $choice in
       1) ./asterisk.common.sh VC-NIL
           ;;
       2) ./asterisk.common.sh VC-PTC
           ;;
       3) ./asterisk.common.sh VC-PROD
           ;;
       4) ./asterisk.common.sh SIP-TEST-TRUNK
           ;;
       B) ../changeprofile.sh
           ;;
       3) exit ;;
   esac
}

_main


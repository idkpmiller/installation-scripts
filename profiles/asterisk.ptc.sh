#!/bin/bash

#************************************************#
#               Asterisk-PTC                     #
#           written by Paul Miller               #
#                SEP, 2013                       #
#                                                #
#       Put the events you want to occur         #
#     when running asterisk in the NIL.          #
#************************************************#

if [ $DEBUG ]; then
	set -x
fi

DIALOG=${DIALOG-dialog}

#Check if root and advise if not
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1 

# log a message to /var/log/messages that we have entered the script.
logger Entered asterisk-ptc.sh script -t PROFILE


f_AMPORTAL=`command -v amportal`

if [ -z $f_AMPORTAL ] 
then
   echo "AMPORTAL Not found"
   exit 2 
fi

if [ "$(pidof ntpd)" ] 
then
   echo 
else
   clear
   echo "NTP not running..."
   exit 3
fi

(echo "Checking services are running for asterisk." ; sleep 2
echo "XXX" ; 
echo "Starting MySQL" ;
echo "XXX";
/etc/init.d/mysql restart >/dev/null 2>&1 ;
echo "20" ;
sleep 1 ;

echo "XXX" ; 
echo "Starting Avahi" ;
echo "XXX";
/etc/init.d/avahi-daemon restart >/dev/null 2>&1 ;
echo "30" ;
sleep 1 ;

echo "XXX" ; 
echo "Starting Mail" ;
echo "XXX";
/etc/init.d/exim4 restart >/dev/null 2>&1 ;
echo "40" ;
sleep 1 ;

echo "XXX" ; 
echo "Starting Web Server" ;
echo "XXX";
/etc/init.d/apache2 restart >/dev/null 2>&1 ;
echo "50" ;
sleep 1 ;

echo "XXX" ; 
echo "Starting Samba" ;
echo "XXX";
/etc/init.d/samba restart >/dev/null 2>&1 ;
echo "60" ;
sleep 1 ;

echo "XXX" ; 
echo "Starting RPCBind" ;
echo "XXX";
/etc/init.d/rpcbind restart >/dev/null 2>&1 ;
echo "65" ;
sleep 1 ;

echo "XXX" ; 
echo "Starting NFS" ;
echo "XXX";
/etc/init.d/nfs restart >/dev/null 2>&1 ;
echo "70" ;
sleep 1 ;

echo "XXX" ; 
echo "Environment Trunk Change" ;
echo "XXX";
/bin/cp -a /root/profiles/extensions_custom_trunks-VC-PTC.conf /etc/asterisk/extensions_custom_trunks.conf >/dev/null 2>&1 ;
/bin/sh /root/profiles/activate_trunk.sh VC-PTC >/dev/null 2>&1 ;
echo "80" ;
sleep 1 ;

echo "XXX" 
echo "Starting Asterisk, this may take some time" ;
echo "XXX";
$f_AMPORTAL restart >/dev/null 2>&1 ;
echo "90" ;
sleep 1 ;


echo "XXX" ; 
echo "Checking all services are Up." ;
echo "XXX";


if [ "$(pidof asterisk)" ] 
then
  echo "92" ;
  sleep 1
else
  exit 92
fi

if [ "$(pidof mysqld)" ] 
then
  echo "94" ;
  sleep 1
else
  exit 94
fi

if [ "$(pidof apache2)" ] 
then
  echo "96" ;
  sleep 1
else
  exit 96
fi

echo "99" ;
sleep 2) | $DIALOG --title "Profile Change Progress" --gauge "Profile Progress" 10 40 0

if [ "$?" = 255 ] ; then
	echo ""
	echo "Box closed !"
fi

# ==========================
MESSAGE="Profile change has been completed, Asterisk PBX is now running..."


$DIALOG --title "Update" --clear \
        --msgbox "$MESSAGE" 10 41

case $? in
  0)
    echo "OK";;
  255)
    echo "ESC pressed.";;
esac






echo "run asterisk.ptc.sh"



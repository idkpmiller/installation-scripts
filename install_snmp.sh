#!/bin/bash
#
# SNMP install
#by Paul Miller
#Revisions:
#1.0 - Initial
#1.1 corrected for use on ubuntu 8.04 LTS
#1.2 corrected final user instructions
#1.3 Added support for yum package manager
#1.4 Changes to accommodate systems that do not have IPv6 support

#CHANGE DEFAULT script variables here before running

def_READ_ONLY_SECRET="public"
def_READ_WRITE_SECRET="private"
def_IPv4_SUBNET="192.168.0.0/16"
def_IPv6_SUBNET="2001:470:812c::/64" 
def_SYS_LOC="Wellington, New Zealand"
def_SYS_CONT_NAME="Paul Miller"
def_SYS_CONT_EMAIL="idkpmiller@sip2serve.com"
def_TRAP_COMMUNITY="idknet"
def_TRAP_RECEIVER="nms.sip2serve.com" # IP or FQDN of a SNMP TRAP reciever server


#====END of User Varables==================

#check current user is root
(( `id -u` )) && echo "Must be ran as root, try prefixxing with sudo." && exit 1

#check if IPv6 enabled system
$ [ -f /proc/net/if_inet6 ] && IPv6=1 || IPv6=0

#### check package manager
type yum >/dev/null 2>&1 || {  PKGMGR="apt"; }
type apt-get >/dev/null 2>&1 || {  PKGMGR="yum"; }

#clear the screen
clear

# Get Input from User
echo "Capture User Options:"
echo "====================="
echo "Please answer the following questions."
echo "Hitting return will continue with the default option"
echo
echo

read -p "System Location [$def_SYS_LOC]: " -e t1
if [ -n "$t1" ]; then def_SYS_LOC="$t1";fi

read -p "System Contact Name [$def_SYS_CONT_NAME]: " -e t1
if [ -n "$t1" ]; then def_SYS_CONT_NAME="$t1";fi

read -p "System Contact Email [$def_SYS_CONT_EMAIL]: " -e t1
if [ -n "$t1" ]; then def_SYS_CONT_EMAIL="$t1";fi

read -p "Trap Community String [$def_TRAP_COMMUNITY]: " -e t1
if [ -n "$t1" ]; then def_TRAP_COMMUNITY="$t1";fi

read -p "NMS or Trap Reciever Address (IP or FQDN) [$def_TRAP_RECEIVER]: " -e t1
if [ -n "$t1" ]; then def_TRAP_RECEIVER="$t1";fi

read -p "IPv4 subnets that can access SNMP on System [$def_IPv4_SUBNET]: " -e t1
if [ -n "$t1" ]; then def_IPv4_SUBNET="$t1";fi

if [ IPv6 ]
then
  read -p "IPv6 subnets that can access SNMP on System [$def_IPv6_SUBNET]: " -e t1
  if [ -n "$t1" ]; then def_IPv6_SUBNET="$t1";fi
fi

read -p "Read Only Community String [$def_READ_ONLY_SECRET]: " -e t1
if [ -n "$t1" ]; then def_READ_ONLY_SECRET="$t1";fi

read -p "Read Write Community String [$def_READ_WRITE_SECRET]: " -e t1
if [ -n "$t1" ]; then def_READ_WRITE_SECRET="$t1";fi


if [ IPv6 ]
then
cat <<EOF
=====================
The Answers provided:
=====================
System Location:       $def_SYS_LOC
System Contact:        $def_SYS_CONT_NAME
System Contact Email:  $def_SYS_CONT_EMAIL
RO Community String:   $def_READ_ONLY_SECRET
RW Community String    $def_READ_WRITE_SECRET
Trap Community String: $def_TRAP_COMMUNITY
Trap Reciever Address: $def_TRAP_RECEIVER
IPv4 Subnet ACL:       $def_IPv4_SUBNET
IPv6 Subnet ACL:       $def_IPv6_SUBNET

EOF
else
cat <<EOF
=====================
The Answers provided:
=====================
System Location:       $def_SYS_LOC
System Contact:        $def_SYS_CONT_NAME
System Contact Email:  $def_SYS_CONT_EMAIL
RO Community String:   $def_READ_ONLY_SECRET
RW Community String    $def_READ_WRITE_SECRET
Trap Community String: $def_TRAP_COMMUNITY
Trap Reciever Address: $def_TRAP_RECEIVER
IPv4 Subnet ACL:       $def_IPv4_SUBNET

EOF
fi
read -p "Press Y to continue with Install." -n 1
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    echo ""
    echo "Goodbye!"
    exit 1
fi

# STEP 1 Update system
echo ""
echo "###############################################"
echo "update system"
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  apt-get -qq update 
else
  # RHEL code
  yum update
fi  

# STEP 2 Install requirements
echo "install requirements"
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  apt-get -y -qq install snmp
  apt-get -y -qq install snmp-mibs-downloader
  apt-get -y -qq install snmpd
  apt-get -y -qq install libsnmp-base
else
  # RHEL code
  yum install net-snmp net-snmp-utils
fi 




# STEP 3 Configure SNMP
echo "Configure SNMP"
echo "###############"


cat <<EOF > /etc/snmp/snmp.conf
#
# As the snmp packages come without MIB files due to license reasons, loading
# of MIBs is disabled by default. If you added the MIBs you can reenable
# loaging them by commenting out the following line.
#mibs :
EOF

echo "/etc/snmp/snmp.conf  - DONE."
#==========================================

if [ IPv6 ]
then
cat <<EOF > /etc/snmp/snmpd.conf
############### Generic config information ###############
agentAddress udp:161,udp6:161
sysLocation    "$def_SYS_LOC"
sysServices    76
sysContact     "$def_SYS_CONT_NAME <$def_SYS_CONT_EMAIL>"
rocommunity $def_READ_ONLY_SECRET  $def_IPv4_SUBNET
rocommunity6 $def_READ_ONLY_SECRET  $def_IPv6_SUBNET
rwcommunity $def_READ_WRITE_SECRET $def_IPv4_SUBNET
rwcommunity6 $def_READ_WRITE_SECRET $def_IPv6_SUBNET
trapcommunity  $def_TRAP_COMMUNITY
trapsink  $def_TRAP_RECEIVER  
trap2sink  $def_TRAP_RECEIVER  
informsink  $def_TRAP_RECEIVER
authtrapenable  1

############### Common config directives ###############
disk / 15%
disk /var 15%
disk /usr 15%
disk /tmp 15%
swap 16000

proc  miniserv.pl 1 1

load  80 50 10

#linkUpDownNotifications yes
#defaultMonitors yes
master agentx
############### Node specific config directives ###############

EOF
else
cat <<EOF > /etc/snmp/snmpd.conf
############### Generic config information ###############
agentAddress udp:161
sysLocation    "$def_SYS_LOC"
sysServices    76
sysContact     "$def_SYS_CONT_NAME <$def_SYS_CONT_EMAIL>"
rocommunity $def_READ_ONLY_SECRET  $def_IPv4_SUBNET
rwcommunity $def_READ_WRITE_SECRET $def_IPv4_SUBNET
trapcommunity  $def_TRAP_COMMUNITY
trapsink  $def_TRAP_RECEIVER  
trap2sink  $def_TRAP_RECEIVER  
informsink  $def_TRAP_RECEIVER
authtrapenable  1

############### Common config directives ###############
disk / 15%
disk /var 15%
disk /usr 15%
disk /tmp 15%
swap 16000

proc  miniserv.pl 1 1

load  80 50 10

#linkUpDownNotifications yes
#defaultMonitors yes
master agentx
############### Node specific config directives ###############

EOF
fi

echo "/etc/snmp/snmpd.conf  - DONE."
#==========================================


cat <<EOF > /etc/snmp/snmptrapd.conf
#
# PLEASE: read the snmptrapd.conf(5) manual page as well!
#
TRAPDRUN=yes
EOF

echo "/etc/snmp/snmptrapd.conf  - DONE."
#==========================================


# STEP 4 Configure for daemon on startup
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  cat <<EOF > /etc/default/snmpd
SNMPDRUN=yes
SNMPDOPTS='-Lsd -Lf /dev/null -u snmp -g snmp -I -smux -p /var/run/snmpd.pid'
TRAPDRUN=no
TRAPDOPTS='-Lsd -p /var/run/snmptrapd.pid'
SNMPDCOMPAT=yes
EOF
echo "/etc/default/snmpd  - DONE."
#==========================================
else
  # RHEL code
  chkconfig snmpd on
fi

# STEP 6 configure access to SNMP daemon
if [ "$PKGMGR" = "apt" ]; then
  # Debian code
  echo ""
else
  # RHEL code
  echo "selinux will be disabled permanently after next reboot"
  sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config
  echo "disable selinux temporary (without reboot)"
  setenforce 0
  iptables -I INPUT 4 -m state --state NEW -m udp -p udp --dport 161 -j ACCEPT
  echo "Save IPv4 access rule"
  service iptables save
  echo "Now activate new IPv4 rule."
  service iptables restart
fi

  

# STEP 5 Start SNMP Daemon
echo '###############'
echo 'Starting SNMP  '
echo '###############'

/etc/init.d/snmpd restart
sleep 5

# STEP 6 Validate daemon started ok
if [ "$(pidof snmpd)" ] 
   then
       clear
	cat <<EOF
Installation and Configuration of snmp
was successful.

INSTRUCTIONS
============
you can test snmp by using the following commands
for IPv4 systems:
snmpwalk -c public -v 2c <IPv4-ADDRESS> sysname

for IPv6 enabled systems:
snmpwalk -c public -v 2c udp6:<IPv6-ADDRESS> sysname
EOF

else
      echo "snmpd FAILED! to start"
      echo "Not sure what went wrong."
fi

exit

Y   SPA-config-tool.sh D:\WORKFO~1\PROJEC~3\CPE\SPA-CO~1.SH    %   D:\WORKFO~1\PROJEC~3\CPE\SPA-CO~1.SH �  #!/bin/bash

#####################################################
# Used to replace known Cisco SPA xml elements
# from a BroadSoft CPE kit or from Cisco SPC default
# configuration files with those stored in a knowledge
#  base file. In this way any new Cisco SPA devices
# can benefit from previous devices.
# 
# --------------------------------------------------
#
# idea by Paul Miller 2013
# script developed by Ryan Davies 2013
# Revision History
# ----------------
# 0.1 Ryan Davies 30/03/2013 Initial release
# 0.2 Ryan Davies 10/04/2013 Added missing quotes
#                            Modified to cope with DOS/MAC input files
# 0.3 Ryan Davies 03/05/2013 Fixed unintended stripping of single and double quotes
#                            Added Conversion to UTF-8
# 
#################################################### 


function usage {
	echo
	echo "USAGE: $0 INFILE KNOWLEDGEFILE"
	exit
}

if [ -z $1 ]; then
	echo "ERROR: You must specify an input file."
	usage
fi

if [ -z $2 ]; then
	echo "ERROR: I need a knowledgebase file to work with."
	usage
fi


infile=$1
kbfile=$2

echo "Processing $infile with knowledge file $kbfile"

# This is hard coded to take the filename of the infile, and append .out on it.
outfile="$infile.out"

# Load the knowledgebase from file and strip any whitespace or tabs from the start and end of each line
knowledgebase=$(tr -d '\r' < $kbfile | sed "s/^[ \\t]*//" | sed "s/[ \\t]*$//")

# Make sure no existing file is present
if [ -f $outfile ]; then
	rm -rf $outfile
fi

# Copy the infile to the outfile, we never work on the original!
tr -d '\r' < $infile > /tmp/$outfile

# Strip any whitespace or tabs from the start of each line in the outfile
sed -i "s/^[ \\t]*//" /tmp/$outfile

# Loop through each line of the knowledgebase
IFS=$'\n'
for line in $knowledgebase; do

	# Take everything up to the first space as our match (What we are looking for in the infile)
	match=$( echo $line | awk -F " " '{print $1}' | sed 's/^[\s|\t]*|[\s|\t]*$//g' | sed 's/"/\\"/g' | sed 's/\//\\\//g' )


	if [ "${match:1:1}" == "!" ] || [ "$match" == "" ]; then
		continue
	fi 


	# Make the line we are replacing REGEX friendly (Escaping " and / )
	line=$( echo -n $line | sed 's/^[\s|\t]*|[\s|\t]*$//g' | sed 's/"/\\"/g' | sed 's/\//\\\//g' )

	# Do the actual repace in the outfile directly
	sed -i "s/^$match.*$/$line <!-- Replaced from knowledgebase file -->/" /tmp/$outfile

	if [ $? -ne 0 ]; then
		echo "SED encountered an error processing line:"
		echo -e "\t$line"
		echo -e "\t\tCommand: sed -i \"s/^$match.*\$/$line <!-- Replaced from knowledgebase file -->/\" /tmp/$outfile"
	fi

done

# Convert the file to UTF-8
iconv -f US-ASCII -t UTF-8 -o $outfile /tmp/$outfile

# Remove Temp File
if [ -f /tmp/$outfile ]; then
	rm -rf /tmp/$outfile
fi

replaced=$(grep "Replaced from knowledgebase file"
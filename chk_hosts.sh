#!/bin/bash

#---------------------
# Functions
#---------------------

function func_chkRoot {
logger "$0 Entered OS ENVIRONMENT CHECKS" 
  (( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
# END func_chkRoot
}

function func_usage {
echo "Usage: $0 [IP Address Port] [-f file]"
echo "OPTIONS:"
echo "  -h     This help file"
echo "  -f     provide a file which lists IP Address and Port combinations for testing."
# END func_usage
}

function file_chk {
printf "%s\t\t%s\t%s\n" "IP@" "Port" "Status"
echo "================================="
RETURN=0
while read ip port
do
  	printf "%s\t" $ip $port
	nc -z -w 3 $ip $port
	if test $? -eq 0
	then
		printf "%s\n" "[PASS]"
	else
		printf "%s\n" "[FAIL]"
              RETURN=1
	fi
done < "${INPUT}"
exit $RETURN
# END file_chk
}

function ip_chk {
printf "%s\t\t%s\t%s\n" "IP@" "Port" "Status"
echo "================================="
printf "%s\t" $SUT
nc -z -w 3 $SUT
if test $? -eq 0
then
	printf "%s\n" "[PASS]"
       exit 0
else
	printf "%s\n" "[FAIL]"
       exit 1
fi
# END ip_chk
}

func_chkRoot
echo "" 
OPTIND=1 # Reset is necessary if getopts was used previously in the script.  It is a good idea to make this local in a function.
while getopts "hf:" opt; do
    case "$opt" in
        h | -help)  func_usage
            exit 0
            ;;
        f)  INPUT=$OPTARG
            file_chk
            ;;
        '?')func_usage >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Shift off the options and optional --.
#echo "File: $INPUT, Leftovers: $@"
SUT=$@
if [ ! -z "$SUT" ]
then
  ip_chk
fi

exit





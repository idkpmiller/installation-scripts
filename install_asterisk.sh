#!/bin/bash
##############################
# by Paul Miller
# installation script for 
# Asterisk 11 with
# a focus on Raspberry Pi
# Wheezy installations.  
##############################

ASTPKG="curl libnewt-dev sqlite3 libsqlite3-dev pkg-config libtool sox libncurses5-dev libssl-dev mpg123 libxml2-dev libgnutls26 bison flex flite"
COMPILEPKG="build-essential automake autoconf git subversion"
APACHEPKG="apache2"
PHPPKG="php5 php5-curl php5-cli php5-mysql php-pear php-db php5-gd"
MYSQLPKG="libmysqlclient15-dev mysql-server mysql-client"
#KERNELPKG="linux-headers-`uname -r`"
KERNELPKG="linux-headers-3.10-3-all" # RPI raspbian 
bold=`tput bold`
normal=`tput sgr0`
####
ASTERISKDBPW=amp109


#---------------------
# Functions
#---------------------

function func_chkRoot() {
echo "================================================================================"
echo "Checking for root Priviledges:"

logger "$0 entered OS ENVIRONMENT CHECKS" 
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
echo "================================================================================"
echo ""
}

function func_chkInternet () {
/usr/bin/wget -q --tries=10 --timeout=5 "http://www.google.com" -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection working"
        /bin/rm /tmp/index.google
fi
echo "================================================================================"
echo ""
}


function func_InstAsterisk-GVoice() {
# Install Dependencies for Google Voice
# Install iksemel
cd /usr/src
wget "https://iksemel.googlecode.com/files/iksemel-1.4.tar.gz"
tar xf iksemel-1.4.tar.gz
cd iksemel-1.4
./configure
make
make install
}

function func_InstAsterisk-DAHDI() {
# Compile and install DAHDI.
cd /usr/src
wget "http://downloads.asterisk.org/pub/telephony/dahdi-linux-complete/dahdi-linux-complete-current.tar.gz"
tar xvfz dahdi-linux-complete-current.tar.gz
cd dahdi-linux-complete-2.6.1+2.6.1
make all
make install
make config
}

function func_InstAsterisk-LIBPRI() {
# Compile and install LIBPRI
cd /usr/src
wget "http://downloads.asterisk.org/pub/telephony/libpri/libpri-1.4-current.tar.gz"
tar xvfz libpri-1.4-current.tar.gz
cd libpri-1.4.14
make
make install
}

function func_InstAsterisk-CORE() {
# Compile and install Asterisk
cd /usr/src
wget "http://downloads.asterisk.org/pub/telephony/asterisk/asterisk-11-current.tar.gz"
tar xvfz asterisk-11-current.tar.gz
cd asterisk-11*

./configure
contrib/scripts/get_mp3_source.sh
make menuselect
make
make install
make config
}

function func_InstAsterisk-LOGS() {
#Setup asterisk log rotation
touch /etc/logrotate.d/asterisk
echo '

/var/log/asterisk/*log {
   missingok
   rotate 5
   weekly
   create 0640 asterisk asterisk
   postrotate
       /usr/sbin/asterisk -rx 'logger reload' > /dev/null 2> /dev/null
   endscript
}

/var/log/asterisk/full {
   missingok
   rotate 5
   daily
   create 0640 asterisk asterisk
   postrotate
       /usr/sbin/asterisk -rx 'logger reload' > /dev/null 2> /dev/null
   endscript
}

/var/log/asterisk/messages {
   missingok
   rotate 5
   daily
   create 0640 asterisk asterisk
   postrotate
       /usr/sbin/asterisk -rx 'logger reload' > /dev/null 2> /dev/null
   endscript
}

/var/log/asterisk/cdr-csv/*csv {
  missingok
  rotate 5
  monthly
  create 0640 asterisk asterisk
}

'  > /etc/logrotate.d/asterisk

chown -R asterisk:asterisk /var/log/asterisk/ 
}

function func_InstAsterisk-SOUNDS_GSM() {
# Install Asterisk-Extra-Sounds
cd /var/lib/asterisk/sounds
wget "http://downloads.asterisk.org/pub/telephony/sounds/asterisk-extra-sounds-en-gsm-current.tar.gz"
tar xvfz asterisk-extra-sounds-en-gsm-current.tar.gz
rm asterisk-extra-sounds-en-gsm-current.tar.gz
}

function func_InstAsterisk-TFTP() {
# allow asterisk to use tftp server directory for device management if present
if [ -d /tftpboot ]; then
 chown -R asterisk:asterisk /tftpboot
 echo "set tftpboot directory owner to asterisk"
fi
}

function func_InstAsterisk() {
# call the separate parts as wanted.
func_InstAsterisk-GVoice
#func_InstAsterisk-DAHDI
#func_InstAsterisk-LIBPRI
func_InstAsterisk-CORE
func_InstAsterisk-SOUNDS_GSM
func_InstAsterisk-LOGS
func_InstAsterisk-TFTP

# create the Asterisk user and set ownership permissions
adduser asterisk --disabled-password --no-create-home --gecos "Asterisk User"
chown asterisk. /var/run/asterisk
chown -R asterisk. /etc/asterisk
chown -R asterisk. /var/{lib,log,spool}/asterisk
chown -R asterisk. /usr/lib/asterisk
chown -R asterisk. /var/www/

#chown -R asterisk:asterisk /etc/asterisk/ /var/lib/asterisk/ /var/run/asterisk /var/spool/asterisk
echo "${bold}Asterisk user and set ownership permissions complete.${normal}"

}

function func_InstFreePBX-CORE () {
# Install and Configure FreePBX
# Download and extract FreePBX
export VER_FREEPBX=2.11
cd /usr/src
svn co "http://www.freepbx.org/v2/svn/freepbx/branches/${VER_FREEPBX}" freepbx
cd freepbx
}

function func_InstFreePBX-WWW () {
#TODO choose Web Server to deploy if none found.
#for now just assume Apache
func_InstFreePBX-APACHE
}

function func_InstFreePBX-APACHE () {
# modifications to Apache
sed -i 's/\(^upload_max_filesize = \).*/\120M/' /etc/php5/apache2/php.ini
cp /etc/apache2/apache2.conf /etc/apache2/apache2.conf_orig
sed -i 's/^\(User\|Group\).*/\1 asterisk/' /etc/apache2/apache2.conf
service apache2 restart

}

function func_InstFreePBX-DB () {
#TODO choose database to deploy if none found.
#for now just assume MySQL
func_InstFreePBX--MySQL
}

function func_InstFreePBX-MySQL () {
# Configure Asterisk database in MYSQL.
read -p "To configure the database for Asterisk we require the root passwrd provided earlier for the MySQL installation" SQLPASSWD
export ASTERISK_DB_PW=$ASTERISKDBPW 
mysqladmin -u root -p$SQLPASSWD create asterisk 
mysqladmin -u root -p$SQLPASSWD create asteriskcdrdb 
mysql -u root -p$SQLPASSWD asterisk < SQL/newinstall.sql 
mysql -u root -p$SQLPASSWD asteriskcdrdb < SQL/cdr_mysql_table.sql

# Set permissions on MYSQL database
mysql -u root -p$SQLPASSWD -e "GRANT ALL PRIVILEGES ON asterisk.* TO asteriskuser@localhost IDENTIFIED BY '${ASTERISK_DB_PW}';"
mysql -u root -p$SQLPASSWD -e "GRANT ALL PRIVILEGES ON asteriskcdrdb.* TO asteriskuser@localhost IDENTIFIED BY '${ASTERISK_DB_PW}';"
mysql -u root -p$SQLPASSWD -e "flush privileges;"
Restart Asterisk and install FreePBX.
./start_asterisk start
./install_amp --webroot /var/www/freepbx
amportal a ma installall
amportal a reload
}

function func_InstFreePBX () {
func_InstFreePBX-CORE
func_InstFreePBX-WWW
func_InstFreePBX-DB

#** If �. /install_amp� fails to run correctly, in terminal type:
./install_amp --username=asteriskuser --password=$ASTERISK_DB_PW --webroot /var/www/freepbx

# one last mod and start FreePBX.
ln -s /var/lib/asterisk/moh /var/lib/asterisk/mohmp3
amportal start 
echo "================================================================================"
echo ""
}

function func_updSystem() {
echo 'updating system, please Wait.'
echo ""
  apt-get -y -qq update
  
  #Check if user want to upgrade now.
  while true; do
    read -p "Would you like to upgrade the system now? (Y/N)" answer
    case $answer in
        [Yy] ) 
			echo "Upgrading ${bold}please wait..${normal}";
			apt-get -y upgrade;
			apt-get -y autoremove;
			break;;
        [Nn] ) echo "NO, selected so continuing"; break;;
        * ) echo "Please answer Y or N.";;
    esac
done
#func_updSystem
echo "================================================================================"
echo ""    
}

function func_InstPackages(){
# call finction with list of packages to be installed as the argument"
# e.g. 
# MusicPKG="mp3 sox"
# func_InstPackages MusicPKG
##########################
PACKAGES="${!1}"

for pkg in $PACKAGES; do
  if apt-get -y -qq install $pkg; then
    echo "${bold}Successfully installed $pkg ${normal}"
  else
    echo "${bold}Error installing $pkg ${normal}"
	exit 1
  fi
done
echo "${bold}Package $1 installation complete. ${normal}"
echo "================================================================================"
echo ""
}

function func_InstApache() {
# Generic Apache Installation
echo "================================================================================"
echo "Installing Apache:"

func_InstPackages APACHEPKG

#setup the servername
echo 'ServerName       $FQDN' > /etc/apache2/conf.d/servername.conf

chown www-data:www-data /var/www
chmod 775 /var/www
usermod -a -G www-data pi

a2enmod ssl 
a2ensite default-ssl

/etc/init.d/apache2 restart
echo "================================================================================"
echo ""
#func_InstApache
}

function func_Dependencies(){
# install packages
func_InstPackages ASTPKG
func_InstPackages MYSQLPKG
func_InstPackages COMPILEPKG
func_InstPackages PHPPKG
func_InstPackages KERNELPKG


#Install Dependencies
# Install PearDB
pear install db

#func_Dependencies
echo "================================================================================"
echo ""
}

#---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
#func_chkRoot
#func_chkInternet
#func_updSystem

#---------------------
# INSTALL APPLICATIONS
#---------------------

#func_Dependencies
#func_InstApache
#func_InstAsterisk
func_InstFreePBX

exit	

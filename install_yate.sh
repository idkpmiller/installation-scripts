#!/bin/bash
##############################
# Version 0.1 - 
#   Initial release 09/01/2014
#
# by Paul Miller
##############################
# DESCRIPTION:
# installation script for 
# YATE (Yet Another Telephony
# Engine).
#
# Script is focused on Raspberry Pi
# Wheezy installations. But may
# function on other Debian/Ubuntu
# Installs. 
##############################

IPv4_ADDRESS=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')
FQDN=$(hostname -f)
DIALOG="$(which whiptail dialog)"

bold=`tput bold`
normal=`tput sgr0`

YATEPKG="build-essential subversion sox autoconf cvs libssl-dev php5-cli"

#---------------------
# Functions
#---------------------
function pTitle () {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as arguament to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot() {
# Supports: text, whiptail, dialog
# Requires:
# DIALOG="$(which whiptail dialog)" # variable
##############################################

logger "$0 Entered OS ENVIRONMENT CHECKS" 

if [ $DIALOG ]
then
  (( `id -u` )) && $DIALOG --msgbox --title "Checking for root Priviledges:" "Must be ran as root, try prefixing with sudo." 8 44 && exit 1
else
pTitle "Checking for root Priviledges:"
  (( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
fi

#func_chkRoot
}

function func_chkInternet () {
pTitle "Checking Internet Connectivity."
/usr/bin/wget -q --tries=10 --timeout=5 "http://www.google.com" -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection working"
        /bin/rm /tmp/index.google
fi
#func_chkInternet
}

function func_updSystem() {
pTitle 'Updating system, please Wait.'
  apt-get -y -qq update
  
  if [ $DIALOG ]; then
    #Check if user want to upgrade now.
    whiptail --yesno --title "Upgrade" "Would you like to upgrade the system now?" 10 50
    if [ $? -ne 0 ]; then
      echo "NO, selected so continuing"
    else
	  apt-get -y upgrade;
	  apt-get -y autoremove;
    fi
  else  
    while true; do
      read -p "Would you like to upgrade the system now? (Y/N)" answer
      case $answer in
          [Yy] ) 
	  		echo "Upgrading please wait..";
	  		apt-get -y upgrade;
	  		apt-get -y autoremove;
	  		break;;
          [Nn] ) echo "NO, selected so continuing"; break;;
          * ) echo "Please answer Y or N.";;
      esac
    done
  fi
  
#func_updSystem    
  }

function func_InstPackages(){
#Usuage:
# call finction with list of packages to be installed as the argument"
# e.g. 
# MusicPKG="mp3 sox"
# func_InstPackages MusicPKG
##########################
PACKAGES="${!1}"

for pkg in $PACKAGES; do
  if apt-get -y -qq install $pkg; then
	echo "${bold}Successfully installed $pkg ${normal}"
  else
    echo "${bold}Error installing $pkg ${normal}"
	exit 1
  fi
done

pTitle "Package $1 installation complete."
}

function func_InstYATE() {
logger "$0 Entered func_InstYATE"
pTitle "Installing YATE:"

# Install the dependencies
func_InstPackages YATEPKG

#Download Yate source
cd /usr/src
svn checkout "http://voip.null.ro/svn/yate/trunk" yate
cd yate
./autogen.sh

./configure

pTitle "Compiling, this will take a while..."
sleep 2
 
make -m=/usr/lib/yate/ -c=/etc/yate/ -e=/usr/share/yate/ -u=~/.yate/ -x=/opt/yate/ install-noapi
if [ $? ]; then echo "Compiling failed"; exit $?; fi

# ./configure
# du -sh ../yate
# make install-noapi

# To run Yate outside of the home directory, we need to do the following:
echo '/usr/local/lib' >> /etc/ld.so.conf
ldconfig

# At this point, Yate should be compiled and must be ready to run.
# 
# 9) Modify the settings per this post (including the 'Updates' mentioned there). One minor change is the default area code needs to be changed per your choice in regexroute.conf for following line (highlighted in red)
# ^\([1-9][0-9][0-9][0-9][0-9][0-9][0-9]\)$=jingle/513\1@voice.google.com;line=GoogleVoice;ojingle_version=0;ojingle_flags=noping;redirectcount=5;checkcalled=false;dtmfmethod=rfc2833

# 10) Now, run the yate from command line to make sure that no 'errors' are thrown (vvv flag is for verbose and CDoa gives a colorful output):
# yate -vvv -CDoa
# If everything looks okay, press ctrl+C to stop the program
# 11) If you have reached this far, the next step would be to run yate as daemon (so that it starts everytime Rpi is rebooted) - Follow the steps mentioned here ('Prepare the Init script' section)
# 11) Download Yate client (I got the windows version) and try connecting to the server from the same subnet as Rpi and that should be it!
# I was able to make calls from the client and receive calls made to my google voice on it as well.
# Next steps: Install CallerID Superfecta and make it work with  Yate on Rpi and install Yate on OpenWRT (need to install OpenWRT on my Netgear WNDR3700). Then configure the SP1 on my obi100 to point to Yate SIP gateway on OpenWRT router.
# 
# Update 31 May 2013: Detailed info on yate (download src using svn build and run) here


# END func_InstYATE
}

##---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
func_chkInternet
func_updSystem

#---------------------
# INSTALL Aplications
#---------------------
func_InstYATE

#---------------------
# FINISHING OFF
#---------------------	
pTitle "Done!"
exit

#!/bin/bash -e

#************************************************#
#                   MkCubie                      #
#           written by Paul Miller               #
#                  Feb, 2013                     #
#                                                #
#    Takes a rootfs RPi image and makes it       #
#      compatible for adding to berryboot        #
#************************************************#

if [ $DEBUG ]; then
	set -x
fi

SOURCE="$(find /opt/images/mkcubie/ -name **.img | awk 'NR==1{print;quit}')"
PREFIX="cubie-"
SOURCE_FILE="$(basename $SOURCE)"
DEST_FILE=$PREFIX$SOURCE_FILE
MAPPER="$(kpartx -l $SOURCE | awk NR==2'{print $1}')"

if [ -z $SOURCE_FILE ]
then
   echo "No img file to convert found"
   exit 1
fi

echo "The source is called " $SOURCE 
echo "The source file is called " $SOURCE_FILE
echo "The destn file is called " $DEST_FILE
echo "The mapper is " $MAPPER

#Check if root and advise if not
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1 

# log a message to /var/log/messages that we have entered the script.
logger Entered mkcubie.sh script -t CUBIE

cd /opt/images/mkcubie/ || { echo "command failed"; exit 1; } 

echo "current directory"
pwd

echo "Set-Up: partition mapping defined"
kpartx -av $SOURCE_FILE || { echo "command failed"; exit 1; } 

echo "Set-Up: Creating temporary mount point"
mkdir /mnt/tmp || { echo "command failed"; exit 1; } 

echo "Set-Up: mounted partition $MAPPER as image"
mount /dev/mapper/$MAPPER /mnt/tmp || { echo "command failed"; exit 1; } 

mksquashfs /mnt/tmp $DEST_FILE -noappend -e lib/modules || { echo "command failed"; exit 1; }

echo "Clean-Up: unmounted image"
umount /mnt/tmp  || { echo "command failed"; exit 1; }


echo "Clean-Up: Removed temporary mount point"
rm -rf /mnt/tmp  || { echo "command failed"; exit 1; }

echo "Clean-Up: deleted partition mapping"
kpartx -d $SOURCE_FILE  || { echo "command failed"; exit 1; }


echo "conversion completed successfully!"


#!/bin/bash

##############################
# by Paul Miller
# installation script for 
# Homer captagent with a focus on Raspberry Pi
# Wheezy installations.
# Limitations:
# ------------
# hard-coded - compiled for TLS support 
#
##############################
# Version:
# --------
# 0.1 initial version
#
##############################

bold=`tput bold`
normal=`tput sgr0`

SELF=$(basename $0)
SCRIPT_NAME="$0"
ARGS="$@"
VERSION="0.1"

APPPKG="openssl build-essential libssl-dev libexpat1-dev libpcap-dev libtool automake "

#---------------------
# Functions
#---------------------
function pTitle () {
#Requires:
#  bold=`tput bold`
#  normal=`tput sgr0`
# Title is passed as argument to the function.
TITLE=$1
  echo "================================================================================"
  echo ${bold}$TITLE${normal}
  echo "================================================================================"
}

function func_chkRoot() {
echo "================================================================================"
echo "Checking for root Privileges:"
echo "================================================================================"
logger "$0 v${VERSION}, entered OS ENVIRONMENT CHECKS" 
(( `id -u` )) && echo "Must be ran as root, try prefixing with sudo." && exit 1
}

function func_chkInternet () {
/usr/bin/wget -q --tries=10 --timeout=5 http://www.google.com -O /tmp/index.google &> /dev/null
if [ ! -s /tmp/index.google ];then
        echo "No Internet connection. Exiting."
        /bin/rm /tmp/index.google
        exit 1
else
        echo "Internet connection is functioning"
        /bin/rm /tmp/index.google
fi
}

function func_updSystem() {
echo 'updating system, please Wait.'
echo ""
  apt-get -y -qq update
  
  #Check if user want to upgrade now.
  while true; do
    read -p "Would you like to upgrade the system now? (Y/N)" answer
    case $answer in
        [Yy] ) 
			echo "Upgrading ${bold}please wait..${normal}";
			apt-get -y upgrade;
			apt-get -y autoremove;
			break;;
        [Nn] ) echo "NO, selected so continuing"; break;;
        * ) echo "Please answer Y or N.";;
    esac
done
#func_updSystem
echo "================================================================================"
echo ""    
}

function func_InstPackages(){
# call finction with list of packages to be installed as the argument"
# e.g. 
# MusicPKG="mp3 sox"
# func_InstPackages MusicPKG
##########################
PACKAGES="${!1}"

for pkg in $PACKAGES; do
  if apt-get -y -qq install $pkg; then
    echo "${bold}Successfully installed $pkg ${normal}"
  else
    echo "${bold}Error installing $pkg ${normal}"
	exit 1
  fi
done
echo "${bold}Package $1 installation complete. ${normal}"
echo "================================================================================"
echo ""
}

function func_Inst-captagent () {

# Install pre-requisites
func_InstPackages APPPKG

# Download
cd /usr/src
git clone https://code.google.com/p/captagent/ captagent

# Compile
cd /usr/src/captagent/captagent
if [ $? -ne 0 ]; then exit 2; fi

./build.sh
if [ $? -ne 0 ]; then exit 2; fi

./configure --enable-ssl --enable-compression
if [ $? -ne 0 ]; then exit 2; fi

make
if [ $? -ne 0 ]; then exit 2; fi

make install
if [ $? -ne 0 ]; then exit 2; fi

captagent -h
if [ $? -ne 0 ]; then exit 2; fi

echo ""
echo "next edit the config file which can normally be found at:"
echo "/usr/local/etc/captagent/captagent.xml"
echo "================================================================================"

} 


#---------------------
# OS ENVIRONMENT CHECKS
#---------------------
clear
func_chkRoot
func_chkInternet
func_updSystem

#---------------------
# APPLICATION INSTALL
#---------------------
func_Inst-captagent



#!/bin/bash

##############################
# by Paul Miller
# initialisation script for 
# iptables.  
##############################

function func_flushIPtablesRules () {
iptables -F
iptables -t nat -F
iptables -t mangle -F
}

function func_flushIPtablesChains () {
iptables -X
iptables -t nat -X
iptables -t mangle -X
}

function func_resetIPtables () {
service iptables save
service iptables restart
}

function func_InitialiseIPtables () {
func_flushIPtablesRules
func_flushIPtablesChains
func_resetIPtables
}

function func_VoIPIPtables () {
#firewall script for VoIP
#To add a range of IP Addresses - use the following syntax
#iptables -A INPUT -p tcp --destination-port 22 -m iprange --src-range 192.168.1.100-192.168.1.200 -j ACCEPT 
#Or single IP Address - for VoIP
#iptables -A INPUT -p udp -s 10.10.10.10 --dport 5060 -j ACCEPT
echo '

#!/bin/bash
#Goes in /etc/init.d/firewall
#sudo /etc/init.d/firewall start 
#sudo /etc/init.d/firewall stop 
#sudo /etc/init.d/firewall restart 
#sudo /etc/init.d/firewall status
#To make it run, sudo update-rc.d firewall defaults



RETVAL=0

# To start the firewall
start() {
  echo -n "Iptables rules creation: "
  /etc/firewall.sh
  RETVAL=0
}

# To stop the firewall
stop() {
  echo -n "Removing all iptables rules: "
  /etc/flush_iptables.sh
  RETVAL=0
}

case $1 in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    start
    ;;
  status)
    /sbin/iptables -L
    /sbin/iptables -t nat -L
    RETVAL=0
    ;;
  *)
    echo "Usage: firewall {start|stop|restart|status}"
    RETVAL=1
esac

exit
' > /etc/init.d/firewall

echo '

#!/bin/bash
#Starts the default IP tables for A2Billing / FreePBX, edit this script to change behaviour
#File location /etc/firewall.sh


iptables -F
iptables -X


iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -i lo -p all -j ACCEPT  
iptables -A INPUT -p udp -m udp --dport 69 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 4445 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 9000 -j ACCEPT
iptables -A INPUT -p tcp -m tcp --dport 10000 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 4520 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 4569 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 5060 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 10000:20000 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 4000:4999 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 123 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 69 -j ACCEPT
iptables -A INPUT -p udp -m udp --dport 53 -j ACCEPT
iptables -A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT
iptables -A INPUT -p all -s localhost  -i eth0 -j DROP	
iptables -A INPUT -j REJECT
iptables -A FORWARD -j REJECT

iptables-save

# End message
echo " [End iptables rules setting]"

' > /etc/firewall.sh

echo '

#!/bin/sh
#Flush iptable rules, and open everything
#File location - /etc/flush_iptables.bash


#
# Set the default policy
#
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT

#
# Set the default policy for the NAT table
#
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
iptables -t nat -P OUTPUT ACCEPT

#
# Delete all rules
#
iptables -F
iptables -t nat -F

#
# Delete all chains
#

iptables -X
iptables -t nat -X

# End message
echo " [End of flush]"

' > /etc/flush_iptables.sh

chmod +x /etc/flush_iptables.sh
chmod +x /etc/firewall.sh
chmod +x /etc/init.d/firewall
update-rc.d firewall defaults

/etc/init.d/firewall restart

#funciptables
}

func_InitialiseIPtables

######
# Last check on fresh raspbian
######
#pi@chat ~ $ ./init_iptables.sh
#ERROR: could not insert 'ip_tables': Operation not permitted
#iptables v1.4.14: can't initialize iptables table `filter': Table does not exist (do you need to insmod?)
#Perhaps iptables or your kernel needs to be upgraded.
#ERROR: could not insert 'ip_tables': Operation not permitted
#iptables v1.4.14: can't initialize iptables table `nat': Table does not exist (do you need to insmod?)
#Perhaps iptables or your kernel needs to be upgraded.
#ERROR: could not insert 'ip_tables': Operation not permitted
#iptables v1.4.14: can't initialize iptables table `mangle': Table does not exist (do you need to insmod?)
#Perhaps iptables or your kernel needs to be upgraded.
#ERROR: could not insert 'ip_tables': Operation not permitted
#iptables v1.4.14: can't initialize iptables table `filter': Table does not exist (do you need to insmod?)
#Perhaps iptables or your kernel needs to be upgraded.
#ERROR: could not insert 'ip_tables': Operation not permitted
#iptables v1.4.14: can't initialize iptables table `nat': Table does not exist (do you need to insmod?)
#Perhaps iptables or your kernel needs to be upgraded.
#ERROR: could not insert 'ip_tables': Operation not permitted
#iptables v1.4.14: can't initialize iptables table `mangle': Table does not exist (do you need to insmod?)
#Perhaps iptables or your kernel needs to be upgraded.
#iptables: unrecognized service
#iptables: unrecognized service
#pi@chat ~ $

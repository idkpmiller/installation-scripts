#!/bin/bash
#
# Create New VM Qemu
#by Paul Miller
#Revisions:
# 0.1 - 20/05/2013 Initial Release
# 0.2 - Add hook point between selecting MAIN and running function
#
###################################################

###################################################
#set default values
###################################################

# Defaults
NAME=WebRTC
RAM=512
CDROM_IMG="/media/data2/Diskless/Operating systems/Linux distros/Centos/6.4 64bit/CentOS-6.4-x86_64-bin-DVD1.iso"
HDPATH="/opt/images/"
HOST_IP=192.168.0.7
NETWORK="bridge:vmlan0"
OS_TYPE="linux"
VCPU=1
AUDIO="default"
SIZE=20
DISTRO="rhel6"

# environmentals
#SRV_CPU='cat /proc/cpuinfo | grep "physical id" | sort | uniq | wc -l'
#SRV_CORESperCPU='cat /proc/cpuinfo | grep "cpu cores" | wc -l'
#SRV_TOTAL_CORES=$((SRV_CPU * SRV_CORESperCPU))
SRV_TOTAL_RAM="grep MemTotal /proc/meminfo"
SRV_TOTAL_SWAP="grep SwapTotal /proc/meminfo"

function update_Vars  {
# Defaults
HDDISK=$HDPATH$NAME.qcow2
VIRT_CMD="virt-install --network="${NETWORK}" --name="${NAME}" --disk "${HDDISK}",size="${SIZE}" --ram $RAM -c "$CDROM_IMG" --vcpus=$VCPU --check-cpu --accelerate --cpuset auto --os-type $OS_TYPE --os-variant $DISTRO --hvm --soundhw $AUDIO --location=$location1 --graphics vnc,port='-1',listen=192.168.0.7,keymap="local""
}

update_Vars

# Usage: showInfo <function> <message>
function showInfo {
update_Vars
  whiptail --msgbox " \
Current status
-----------------------------------------------------
NAME: $NAME, RAM: $RAM MB, VCPU: $VCPU, HD Size: $SIZE GB, AUDIO: $AUDIO
OS   $OS_TYPE $DISTRO, NETWORK: $NETWORK, VNC IP: $HOST_IP
CDROM_IMG $CDROM_IMG
HDDISK    $HDDISK

The command below will be run if you select create.
 ================================================
$VIRT_CMD
" 24 80 1
}

do_Name() {
NAME=$(whiptail --inputbox "What is the name of ther VM you wish to create?" 8 80 $NAME --title "The name mist be unique to this server" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $NAME
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"
}

do_RAM() {
RAM=$(whiptail --inputbox "Enter amount of RAM in MB?" 8 80 $RAM --title "1024 equals 1GB RAM" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $RAM
    
#	if ! [[ "$RAM" =~ ^[0-9]+$ ]] ; then
#    exec >&2; echo "error: Not a number"; do_RAM
	
#	if ! [[ "$RAM" < 64 ]] ; then
#    exec >&2; echo "error: value too small"; do_RAM

#	if ! [[ "$RAM" > $SRV_TOTAL_RAM ]] ; then
#    exec >&2; echo "error: value too large"; do_RAM

else
    echo "User selected Cancel."
fi

echo "Exit status was $exitstatus"
}

do_CPU() {
VCPU=$(whiptail --inputbox "How many CPU's would you like to allocate?" 8 80 $VCPU --title "You have 4 Phycical CPU/Cores" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $VCPU
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"
}

do_CDROM() {
CDROM_IMG=$(whiptail --inputbox "Enter full path and filename including extention of the CD or DVD image file" 8 80 "$CDROM_IMG" --title "The default path is currently $CDROM_IMG" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $CDROM_IMG
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"
}

do_Hard_Drive() {
update_Vars
while true; do
HD=$(whiptail --menu "Create VM Hard drive" 20 80 12 --cancel-button Cancel --ok-button Back \
    "Disk" "Configure the Disk path and filename" \
    "Size" "Set the HD size" \
    3>&1 1>&2 2>&3)
	RET=$?
	  if [ $RET -eq 1 ]; then
    do_MAIN
  elif [ $RET -eq 0 ]; then
    "do_$HD" || whiptail --msgbox "There was an error running do_$HD" 20 60 1
  else
    exit 1
  fi
 done
}

do_Disk() {
update_Vars
HDDISK=$(whiptail --inputbox "Enter full path and filename including extention of the hard drive image file" 8 80 $HDDISK --title "The default path is currently $HDPATH" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $HDDISK
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"

}

do_Size() {
SIZE=$(whiptail --inputbox "Enter amount of Hard Drive space in GB?" 8 80 $SIZE --title "1024 equals 1TB Disk Space" 3>&1 1>&2 2>&3)
 
exitstatus=$?
if [ $exitstatus = 0 ]; then
    echo "User selected Ok and entered " $SIZE
else
    echo "User selected Cancel."
fi
 
echo "(Exit status was $exitstatus)"

}

do_OS_Distro() {
OS_TYPE=$(whiptail --title "OS Type" --radiolist \
"Choose the OS Distro to Install" 20 80 30 \
"win7" " Microsoft Windows 7" ON \
"vista" " Microsoft Windows Vista" OFF \
"winxp64"              " Microsoft Windows XP (x86_64)" OFF \
"winxp"                " Microsoft Windows XP" OFF \
"win2k"                " Microsoft Windows 2000" OFF \
"win2k8"               " Microsoft Windows Server 2008" OFF \
"win2k3"               " Microsoft Windows Server 2003" OFF \
"openbsd4"             " OpenBSD 4.x" OFF \
"freebsd8"             " FreeBSD 8.x" OFF \
"freebsd7"             " FreeBSD 7.x" OFF \
"freebsd6"             " FreeBSD 6.x" OFF \
"solaris9"             " Sun Solaris 9" OFF \
"solaris10"            " Sun Solaris 10" OFF \
"opensolaris"          " Sun OpenSolaris" OFF \
"netware6"             " Novell Netware 6" OFF \
"netware5"             " Novell Netware 5" OFF \
"netware4"             " Novell Netware 4" OFF \
"msdos"                " MS-DOS" OFF \
"generic"              " Generic" OFF \
"debiansqueeze"        " Debian Squeeze" OFF \
"debianlenny"          " Debian Lenny" OFF \
"debianetch"           " Debian Etch" OFF \
"fedora16"             " Fedora 16" OFF \
"fedora15"             " Fedora 15" OFF \
"fedora14"             " Fedora 14" OFF \
"fedora13"             " Fedora 13" OFF \
"fedora12"             " Fedora 12" OFF \
"fedora11"             " Fedora 11" OFF \
"fedora10"             " Fedora 10" OFF \
"fedora9"              " Fedora 9" OFF \
"fedora8"              " Fedora 8" OFF \
"fedora7"              " Fedora 7" OFF \
"fedora6"              " Fedora Core 6" OFF \
"fedora5"              " Fedora Core 5" OFF \
"mes5.1"               " Mandriva Enterprise Server 5.1 and later" OFF \
"mes5"                 " Mandriva Enterprise Server 5.0" OFF \
"mandriva2010"         " Mandriva Linux 2010 and later" OFF \
"mandriva2009"         " Mandriva Linux 2009 and earlier" OFF \
"rhel7"                " Red Hat Enterprise Linux 7" OFF \
"rhel6"                " Red Hat Enterprise Linux 6" OFF \
"rhel5.4"              " Red Hat Enterprise Linux 5.4 or later" OFF \
"rhel5"                " Red Hat Enterprise Linux 5" OFF \
"rhel4"                " Red Hat Enterprise Linux 4" OFF \
"rhel3"                " Red Hat Enterprise Linux 3" OFF \
"rhel2.1"              " Red Hat Enterprise Linux 2.1" OFF \
"sles11"               " Suse Linux Enterprise Server 11" OFF \
"sles10"               " Suse Linux Enterprise Server" OFF \
"ubuntuoneiric"        " Ubuntu 11.10 (Oneiric Ocelot)" OFF \
"ubuntunatty"          " Ubuntu 11.04 (Natty Narwhal)" OFF \
"ubuntumaverick"       " Ubuntu 10.10 (Maverick Meerkat)" OFF \
"ubuntulucid"          " Ubuntu 10.04 (Lucid Lynx)" OFF \
"ubuntukarmic"         " Ubuntu 9.10 (Karmic Koala)" OFF \
"ubuntujaunty"         " Ubuntu 9.04 (Jaunty Jackalope)" OFF \
"ubuntuintrepid"       " Ubuntu 8.10 (Intrepid Ibex)" OFF \
"ubuntuhardy"          " Ubuntu 8.04 LTS (Hardy Heron)" OFF \
"virtio26"             " Generic 2.6.25 or later kernel with virtio" OFF \
"generic26"            " Generic 2.6.x kernel" OFF \
"generic24"            " Generic 2.4.x kernel" OFF \
3>&1 1>&2 2>&3)
}

do_audio() {
AUDIO=$(whiptail --title "Audio Hardware" --radiolist \
"Choose the Audio Hardware to Emulate" 20 80 6 \
"default" "if the hypervisor supports it, otherwise it will be ES1370" OFF \
"ich6" "Intel Input Output Controller Hub 6" OFF \
"ac97" "Intel Audio Labs 1997, Audio Codec 97" ON \
"es1370" "Ensoniq AudioPCI ES1370" OFF \
"sb16" "Creative Sound Blaster 16 bit Compatible" OFF \
"pcspk" "PC Speaker" OFF \
3>&1 1>&2 2>&3)
}

do_Create() {
update_Vars
if [ -f "$CDROM_IMG" ];
then
   echo "File $CDROM_IMG exists."
else
   echo "The CDROM image specified; $CDROM_IMG does not exist."
   exit 1
fi

 # whiptail --msgbox " \
 # Command below will be run to create your new VM.
 # ================================================
 # " 20 70 1
clear
  echo "Creating VM........"
virt-install --network="${NETWORK}" --name="${NAME}" --disk "${HDDISK}",size="${SIZE}" --ram "${RAM}" -c "${CDROM_IMG}" --vcpus=$VCPU --check-cpu --accelerate --cpuset auto --os-type $OS_TYPE --os-variant $DISTRO --hvm --soundhw "${AUDIO}" --location="$location1" --graphics vnc,port='-1',listen=192.168.0.7,keymap="local"
RET=$?
exit $RET
 }

do_finish() {
echo "Finished was pressed."

  exit 0
}

do_OS_type() {
OS_TYPE=$(whiptail --title "OS Type" --radiolist \
"Choose the OS Type to Install" 20 80 3 \
"linux" "Covers many different linux distros" OFF \
"windows" "Covers the Microsoft Windows OS family" ON \
3>&1 1>&2 2>&3)
}

do_MAIN() {
 while true; do
  FUNC=$(whiptail --menu "Create-VM" 20 80 12 --cancel-button Finish --ok-button Select \
    "showInfo" "Information about this tool" \
    "Name" "Provide a name for the new VM" \
    "RAM" "Set the amiunt of RAM" \
    "CPU" "Set the number of vCPUs for the VM" \
    "CDROM" "Provide the CDROM image path and filename" \
    "Hard_Drive" "Set the size abd filename for the Hard Drive" \
    "OS_type" "Set the OS type to be installed" \
    "OS_Distro" "Set the OS Distro to be installed" \
    "audio" "Set the Audio Hw to emulate" \
    "Create" "The final check and then creation" \
    3>&1 1>&2 2>&3)
  RET=$?
  if [ $RET -eq 1 ]; then
    do_finish
  elif [ $RET -eq 0 ]; then
  #  "do_$FUNC" || whiptail --msgbox "There was an error running do_$FUNC" 20 60 1
  case $FUNC in
        "showInfo")
            echo "you chose choice "${FUNC}""
			"${FUNC}"
            ;;
        "Name")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
        "RAM")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
		"CPU")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
		"CDROM")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "Hard_Drive")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "OS_type")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "OS_Distro")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "audio")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
	    "Create")
            echo "you chose choice "${FUNC}""
			"do_$FUNC"
            ;;
        *) echo invalid option;;
    esac
  else
    exit 1
  fi
 done
}


#check current user privileges
(( `id -u` )) && echo "This script MUST be ran with root privileges, try prefixing with sudo. i.e sudo $0" && exit 1

# check if Bridged Networking is configured
show_bridge=`brctl show | awk 'NR==2 {print $1}'`
if [ $? -ne 0 ] ; then
	echo "Bridged Networking is not configured, please do so to get an IP similar to your host."
	exit 255
fi 

  
do_MAIN


#!/bin/bash +x
# script creates a whitelist from a file in the same directory
# as the script consisting of CIDR addresses the file is called
# whitelist.txt
# although this sets up the whitlist chain it does not decide which
# rules send you to the chain that is left to the administrator.

# If script is not ran as root then exit with error message
(( `id -u` )) && echo "This script Must be ran as root, try prefixing with sudo." && exit 1

# create new chain called ASTERISK_IP
iptables -N ASTERISK_IP

if [ -f whitelist.txt ]; then
   iptables -F
   BLOCKDB="whitelist.txt"
   IPS=$(grep -Ev "^#" $BLOCKDB)
   for i in $IPS
   do
      iptables -A ASTERISK_IP -s $i -j ACCEPT
   done
else
   echo "Unable to locate file in the same directory as script called whitelist.txt"
   exit 1
fi

# set the default drop all packets that are not matched by previous rules
iptables -A ASTERISK_IP -j DROP

# commit the changes to file
iptables-save > /etc/iptables.whitelist.rules

# if ending here set positive exit code
exit 0









